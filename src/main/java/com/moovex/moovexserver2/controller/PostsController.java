package com.moovex.moovexserver2.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.moovex.moovexserver2.model.PageDTO;
import com.moovex.moovexserver2.model.post.Comment;
import com.moovex.moovexserver2.model.post.Post;
import com.moovex.moovexserver2.service.PostService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@Slf4j
@RestController
public class PostsController {

    @Autowired
    private PostService postService;

    private final ObjectMapper jsonMapper = new ObjectMapper();

    @PostMapping("v1/createFeed")
    public Post createFeed(
            @RequestHeader("user-token") String userToken,
            @RequestParam("feed") String feed,
            @RequestParam(value = "feedImg", required = false) String feedImgUrl,
            @RequestParam("creatorId") UUID creatorId
    ) throws JsonProcessingException {
        log.info("PostsController -> v1/createFeed (API REQUEST): userToken->{}, feed->{}, feedImgUrl->{}, creatorId->{} ", userToken, feed, feedImgUrl, creatorId);
        Post result = postService.createPost(feed, feedImgUrl, creatorId);
        log.info("PostsController -> v1/createFeed (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
        return result;
    }

    @GetMapping("v1/getAllFeeds")
    public PageDTO<Post> getAllFeeds(
            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "pageSize", required = false, defaultValue = "10") int pageSize
    ) throws JsonProcessingException {
        log.info("findAllPosts -> v1/getAllFeeds (API REQUEST): page->{}, pageSize->{}", page, pageSize);
        PageDTO<Post> result = postService.getAllPosts(page, pageSize);
        log.info("findAllPosts -> v1/getAllFeeds (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
        return result;
    }

    @PostMapping("v1/createComment")
    public Comment addComment(
            @RequestHeader("user-token") String userToken,
            @RequestParam("comment") String comment,
            @RequestParam("feedId") UUID feedId,
            @RequestParam("commentCreatorId") UUID creatorId
    ) throws JsonProcessingException {
        log.info("PostsController -> v1/createComment (API REQUEST): userToken->{}, comment->{}, feedId->{}, creatorId->{}", userToken, comment, feedId, creatorId);
        Comment result = postService.addComment(comment, creatorId, feedId);
        log.info("findAllPosts -> v1/createComment (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
        return result;
    }

    @GetMapping("v1/getFeedComments")
    public PageDTO<Comment> getFeedComments(
            @RequestParam("feedId") UUID feedId,
            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "pageSize", required = false, defaultValue = "10") int pageSize
    ) throws JsonProcessingException{
        log.info("findAllPosts -> v1/getAllFeeds (API REQUEST): feedId->{} page->{}, pageSize->{}",feedId, page, pageSize);
        PageDTO<Comment> result = postService.getFeedComments(feedId,page, pageSize);
        log.info("findAllPosts -> v1/getAllFeeds (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
        return result;
    }

    @GetMapping("v1/findPostById")
    public Post findPostById(@RequestParam("postId") UUID postId) {
        return postService.findCommentById(postId);
    }
}
