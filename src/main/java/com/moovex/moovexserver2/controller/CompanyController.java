package com.moovex.moovexserver2.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.moovex.moovexserver2.entity.company.CompanyType;
import com.moovex.moovexserver2.entity.company.ContractStatus;
import com.moovex.moovexserver2.entity.user.UserType;
import com.moovex.moovexserver2.entity.user.VerificationState;
import com.moovex.moovexserver2.model.company.CompanyFull;
import com.moovex.moovexserver2.model.company.Company;
import com.moovex.moovexserver2.model.user.User;
import com.moovex.moovexserver2.model.user.UserPreview;
import com.moovex.moovexserver2.model.vechile.TractorHead;
import com.moovex.moovexserver2.model.vechile.Trailer;
import com.moovex.moovexserver2.model.vechile.Truck;
import com.moovex.moovexserver2.service.CompanyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Set;
import java.util.UUID;

import static com.moovex.moovexserver2.util.CollectionUtil.isNotNullNotEmpty;

@Slf4j
@RestController
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    private final ObjectMapper jsonMapper = new ObjectMapper();

    @PostMapping("v1/createCompany")
    public CompanyFull createCompany(
            @RequestHeader("user-token") String userToken,
            @RequestParam(value = "tenantId", required = false) UUID tenantId,
            @RequestParam(value = "isPrivateTenant", required = false, defaultValue = "true") Boolean isPrivate,
            @RequestBody CompanyFull companyFull
    ) throws JsonProcessingException {
        log.info("CompanyController -> v1/createCompany (API REQUEST): userToken->{}, tenantId->{}, isPrivate->{}, company->{}", userToken, tenantId, isPrivate, jsonMapper.writeValueAsString(companyFull));
        CompanyFull result = companyService.createCompany(companyFull, isPrivate, tenantId);
        log.info("CompanyController -> v1/createCompany (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
        return result;
    }

    @PutMapping("v1/updateCompany")
    public CompanyFull updateCompany(
            @RequestHeader("user-token") String userToken,
            @RequestBody CompanyFull companyFull
    ) throws JsonProcessingException {
        log.info("CompanyController -> v1/updateCompany (API REQUEST): userToken->{}, company->{}", userToken, jsonMapper.writeValueAsString(companyFull));
        CompanyFull result = companyService.updateCompany(companyFull);
        log.info("CompanyController -> v1/updateCompany (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
        return result;
    }

    @PostMapping("v1/createCompanyEmployee")
    public User createCompanyEmployee(
            @RequestHeader("user-token") String userToken,
            @RequestParam("companyId") UUID companyId,
            @RequestBody User employee
    ) throws JsonProcessingException {
        log.info("CompanyController -> v1/createCompanyEmployee (API REQUEST):  companyId->{}, employee->{}", companyId,jsonMapper.writeValueAsString(employee));
        User result = companyService.createCompanyEmployee(companyId, employee);
        log.info("CompanyController -> v1/createCompanyEmployee (API RESPONSE): responseObject->{}",jsonMapper.writeValueAsString(result));
        return result;
    }

    @GetMapping("v1/findCompanyById")
    public CompanyFull findCompanyById(@RequestParam("companyId") UUID companyId) throws JsonProcessingException {
        log.info("CompanyController -> v1/findCompanyById (API REQUEST): companyId->{}", companyId);
        CompanyFull result = companyService.findCompanyById(companyId);
        log.info("CompanyController -> v1/findCompanyById (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
        return result;
    }

    @GetMapping("v1/isCompanyPhoneNumberExist")
    public boolean isCompanyPhoneNumberExist(
            @RequestParam(value = "tenantId", required = false) UUID tenantId,
            @RequestParam(value = "phoneNumber") String phoneNumber
    ) {
        log.info("CompanyController -> v1/isCompanyPhoneNumberExist (API REQUEST): tenantId->{}, phoneNumber->{}", tenantId, phoneNumber);
        boolean result = companyService.isCompanyPhoneNumberExist(tenantId, phoneNumber);
        log.info("CompanyController -> v1/isCompanyPhoneNumberExist (API RESPONSE): responseObject->{}", result);
        return result;
    }

    @GetMapping("v1/findAllCompanies")
    public Set<CompanyFull> findAllCompanies(
            @RequestParam(value = "tenantId") UUID tenantId,
            @RequestParam(value = "companyType", required = false) CompanyType companyType,
            @RequestParam(value = "companyVerificationStatus", required = false) VerificationState verificationState,
            @RequestParam(value = "contractStatus", required = false) ContractStatus contractStatus
    ) throws JsonProcessingException {
        log.info("CompanyController -> v1/findAllCompanies (API REQUEST): tenantId->{}, companyType->{}, verificationState->{}, contractStatus->{}", tenantId, companyType, verificationState, contractStatus);
        Set<CompanyFull> result = companyService.findAllCompanies(tenantId, companyType, verificationState, contractStatus);
        if (isNotNullNotEmpty(result)) {
            log.info("CompanyController -> v1/findAllCompanies (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
            return result;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No companies found");
        }
    }

    @GetMapping("v1/findTransportersPreviews")
    public Set<Company> findTransportersPreviews(@RequestParam(value = "tenantId") UUID tenantId) throws JsonProcessingException {
        log.info("CompanyController -> v1/findTransportersPreviews (API REQUEST): tenantId->{}", tenantId);
        Set<Company> result = companyService.findTransportersPreviews(tenantId);
        if (isNotNullNotEmpty(result)) {
            log.info("CompanyController -> v1/findTransportersPreviews (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
            return result;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No companies found");
        }
    }


    @GetMapping("v1/findAllCompaniesPreview")
    public Set<Company> findAllCompaniesPreview(
            @RequestParam(value = "tenantId", required = false) UUID tenantId,
            @RequestParam(value = "companyType", required = false) CompanyType companyType
    ) throws JsonProcessingException {
        log.info("CompanyController -> v1/findAllCompaniesPreview (API REQUEST): tenantId->{}, companyType->{}", tenantId, companyType);
        Set<Company> result = companyService.findAllCompaniesPreview(tenantId, companyType);
        if (isNotNullNotEmpty(result)) {
            log.info("CompanyController -> v1/findAllCompaniesPreview (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
            return result;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No companies found");
        }
    }

    @GetMapping("v1/findCompanyEmployeesPreview")
    public Set<UserPreview> findCompanyEmployeesPreview(
            @RequestParam(value = "companyId") UUID companyId,
            @RequestParam(value = "userType", required = false) UserType... userType
    ) throws JsonProcessingException {
        log.info("CompanyController -> v1/findCompanyEmployeesPreview (API REQUEST): companyId->{}, userTypes->{}", companyId, userType);
        Set<UserPreview> result = companyService.findCompanyEmployeePreview(companyId, userType);
        if (isNotNullNotEmpty(result)) {
            log.info("CompanyController -> v1/findCompanyEmployeesPreview (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
            return result;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No employees found for requested company id:" + companyId + " userType:" + userType);
        }
    }

    @PutMapping("v1/addEmployeeToCompany")
    public CompanyFull addEmployeeToCompany(
            @RequestHeader("user-token") String userToken,
            @RequestParam("userId") UUID userId,
            @RequestParam("companyId") UUID companyId,
            @RequestParam("tenantUserRole") UserType tenantUserRole
    ) throws JsonProcessingException {
        log.info("CompanyController -> v1/addEmployeeToCompany (API REQUEST): userToken->{}, userId->{}, companyId->{}, tenantUserRole->{}", userToken, userId, companyId, tenantUserRole);
        CompanyFull result = companyService.addEmployeeToCompany(userId, companyId, tenantUserRole);
        log.info("CompanyController -> v1/addEmployeeToCompany (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
        return result;
    }

    //------------------------- Vehicles ----------------------------------------

    @PostMapping("v1/createCompanyTruck")
    public Truck createCompanyTruck(
            @RequestParam(value = "companyId") UUID companyId,
            @RequestBody Truck truck
    ) throws JsonProcessingException {
        log.info("CompanyController -> v1/createCompanyTruck (API REQUEST): companyId->{}, truck->{}", companyId, jsonMapper.writeValueAsString(truck));
        Truck result = companyService.createTruck(companyId, truck);
        log.info("CompanyController -> v1/createCompanyTruck (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
        return result;
    }

    @PostMapping("v1/createCompanyTractor")
    public TractorHead createCompanyTractor(
            @RequestParam(value = "companyId") UUID companyId,
            @RequestBody TractorHead tractor
    ) throws JsonProcessingException {
        log.info("CompanyController -> v1/createCompanyTractor (API REQUEST): companyId->{}, tractor->{}", companyId, jsonMapper.writeValueAsString(tractor));
        TractorHead result = companyService.createTractor(companyId,tractor);
        log.info("CompanyController -> v1/createCompanyTractor (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
        return result;
    }

    @PostMapping("v1/createCompanyTrailer")
    public Trailer createCompanyTrailer(
            @RequestParam(value = "companyId") UUID companyId,
            @RequestParam Trailer trailer
    ) throws JsonProcessingException {
        log.info("CompanyController -> v1/createCompanyTrailer (API REQUEST): companyId->{}, trailer->{}", companyId, jsonMapper.writeValueAsString(trailer));
        Trailer result = companyService.createTrailer(companyId, trailer);
        log.info("CompanyController -> v1/createCompanyTrailer (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
        return result;
    }

    @GetMapping("v1/findCompanyTucks")
    public Set<Truck> findCompanyTucks(@RequestParam(value = "companyId") UUID companyId) throws JsonProcessingException {
        log.info("CompanyController -> v1/findCompanyTrucks (API REQUEST): companyId->{}", companyId);
        Set<Truck> result = companyService.findCompanyTrucks(companyId);
        if (isNotNullNotEmpty(result)) {
            log.info("CompanyController -> v1/findCompanyTrucks (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
            return result;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No trucks found for requested company id:" + companyId);
        }
    }

    @GetMapping("v1/findCompanyTrailers")
    public Set<Trailer> findCompanyTrailers(@RequestParam(value = "companyId") UUID companyId) throws JsonProcessingException {
        log.info("CompanyController -> v1/findCompanyTrailers (API REQUEST): companyId->{}", companyId);
        Set<Trailer> result = companyService.findCompanyTrailers(companyId);
        if (isNotNullNotEmpty(result)) {
            log.info("CompanyController -> v1/findCompanyTrailers (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
            return result;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No trailers found for requested company id:" + companyId);
        }
    }

    @GetMapping("v1/findCompanyTractors")
    public Set<TractorHead> findCompanyTractors(@RequestParam(value = "companyId") UUID companyId) throws JsonProcessingException {
        log.info("CompanyController -> v1/findCompanyTractors (API REQUEST): companyId->{}", companyId);
        Set<TractorHead> result = companyService.findCompanyTractors(companyId);
        if (isNotNullNotEmpty(result)) {
            log.info("CompanyController -> v1/findCompanyTractors (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
            return result;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No tractors found for requested company id:" + companyId);
        }
    }

}
