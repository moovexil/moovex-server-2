package com.moovex.moovexserver2.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.moovex.moovexserver2.entity.events.EventType;
import com.moovex.moovexserver2.service.EventService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@Slf4j
@RestController
public class EventController {

    private final ObjectMapper jsonMapper = new ObjectMapper();

    @Autowired
    private EventService eventService;

    @PostMapping("v1/createEvent")
    public void createEvent(
            @RequestParam("trackingObjectId") UUID trackingObjectId,
            @RequestParam("type") EventType type,
            @RequestParam("timestamp") Long timestamp
    ) {
        log.info("EventController -> v1/createEvent (API createEvent): trackingObjectId->{}, type->{}, timestamp->{}", trackingObjectId,type,timestamp);
        eventService.createEvent(trackingObjectId,type,timestamp);
    }
}
