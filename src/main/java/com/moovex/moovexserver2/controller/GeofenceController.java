package com.moovex.moovexserver2.controller;

import com.moovex.moovexserver2.model.geofence.GeofenceRadius;
import com.moovex.moovexserver2.model.geofence.geofence_event.GeofenceEventType;
import com.moovex.moovexserver2.service.GeofenceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@Slf4j
@RestController
public class GeofenceController {

    @Autowired
    GeofenceService geofenceService;

    @PostMapping("v1/registerGeofence")
    public void createPlace(
            @RequestParam("placeId") UUID placeId,
            @RequestParam("trackingId") UUID trackingId,
            @RequestParam(value = "dwellDelay", required = false) Long dwellDelay,
            @RequestParam("radius") GeofenceRadius radius
    ) {
        log.info("GeofenceController -> v1/registerGeofence (API createEvent): placeId->{}, trackingId->{}, radius->{}, dwellDelay->{}", placeId, trackingId,radius.toString(),dwellDelay);
        geofenceService.registerGeofence(placeId, trackingId,radius.getRadius(),dwellDelay );
    }


    @PutMapping("v1/unregisterGeofence")
    public void unregisterGeofence(@RequestParam("geofenceId") UUID geofenceId) {
        log.info("GeofenceController -> v1/unregisterGeofence (API createEvent): geofenceId->{}", geofenceId);
        geofenceService.unregisterGeofence(geofenceId);
    }


    @PutMapping("v1/changeGeofenceRegistrationStatus")
    public void updateGeofenceRegistration(
            @RequestParam("registered") boolean isRegistered,
            @RequestParam("id") UUID geofenceId
    ) {
        log.info("GeofenceController -> v1/changeGeofenceRegistrationStatus (API createEvent): geofenceId->{} , registered->{}", geofenceId, isRegistered);
        geofenceService.updateGeofenceRegistrationState(geofenceId, isRegistered);
    }

    @PostMapping("v1/createGeofenceEvent")
    public void createGeofenceEvent(
            @RequestParam("eventType") GeofenceEventType eventType,
            @RequestParam("timestamp") long timestamp,
            @RequestParam(value = "id", required = false) UUID geofenceId,
            @RequestParam(value = "rationale", required = false) String rationale,
            @RequestParam(value = "latitude", required = false) Double latitude,
            @RequestParam(value = "longitude", required = false) Double longitude
    ) {
        log.info("GeofenceController -> v1/unregisterGeofence (API createEvent): geofenceId->{}, type->{}, rationale->{}, lat->{}, long->{}, timestamp-{}", geofenceId, eventType, rationale, latitude, longitude, timestamp);
        geofenceService.createGeofenceEvent(eventType, timestamp, geofenceId, rationale, latitude, longitude);
    }

}
