package com.moovex.moovexserver2.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.moovex.moovexserver2.entity.company.CompanyType;
import com.moovex.moovexserver2.entity.company.ContractStatus;
import com.moovex.moovexserver2.entity.user.VerificationState;
import com.moovex.moovexserver2.model.company.Company;
import com.moovex.moovexserver2.service.CompanyServiceV2;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@Slf4j
@RestController
public class CompanyControllerV2 {

    private final ObjectMapper jsonMapper = new ObjectMapper();

    @Autowired
    CompanyServiceV2 companyServiceV2;

    @SneakyThrows
    @PostMapping("v1/createPartnerCompany")
    public Company createPartnerCompany(
            @RequestParam("companyTenantId") UUID companyTenantId,
            @RequestParam("name") String name,
            @RequestParam("companyType") CompanyType companyType,
            @RequestParam("verificationState") VerificationState verificationState,
            @RequestParam("contractStatus") ContractStatus contractStatus,
            @RequestParam("cbtAvailable") Boolean cbtAvailable,
            @RequestParam("gitAvailable") Boolean gitAvailable,
            @RequestParam(value = "aboutCompany", required = false) String aboutCompany,
            @RequestParam(value = "specialty", required = false) String specialty
    ) {
        log.info("CompanyControllerV2 -> v1/createPartnerCompany (API REQUEST): companyTenantId->{},name->{},companyType->{},verificationState->{},contractStatus->{},cbt-{},git->{},about->{},specialty->{}", companyTenantId, name, companyType, verificationState, contractStatus, cbtAvailable, gitAvailable, aboutCompany, specialty);
        return companyServiceV2.createPartnerCompany(companyTenantId, name, companyType, verificationState,
                contractStatus, cbtAvailable, gitAvailable, aboutCompany, specialty);
    }

    @SneakyThrows
    @PostMapping("v1/createUserCompany")
    public Company createUserCompany(
            @RequestParam("companyAdminId") UUID companyAdminId,
            @RequestParam("name") String name,
            @RequestParam("companyType") CompanyType companyType,
            @RequestParam("verificationState") VerificationState verificationState,
            @RequestParam("contractStatus") ContractStatus contractStatus,
            @RequestParam("cbtAvailable") boolean cbtAvailable,
            @RequestParam("gitAvailable") boolean gitAvailable,
            @RequestParam(value = "aboutCompany", required = false) String aboutCompany,
            @RequestParam(value = "specialty", required = false) String specialty
    ) {
        log.info("CompanyControllerV2 -> v1/createUserCompany (API REQUEST): companyAdminId->{},name->{},companyType->{},verificationState->{},contractStatus->{},cbt-{},git->{},about->{},specialty->{}", companyAdminId, name, companyType, verificationState, contractStatus, cbtAvailable, gitAvailable, aboutCompany, specialty);

        return companyServiceV2.createUserCompany(companyAdminId, name, companyType, verificationState,
                contractStatus, cbtAvailable, gitAvailable, aboutCompany, specialty);
    }

    @SneakyThrows
    @PutMapping("v1/updatePartnerCompany")
    public Company updateCompany(
            @RequestParam("companyId") UUID companyId,
            @RequestParam(value = "tagId", required = false) UUID tagId,
            @RequestParam("name") String name,
            @RequestParam("companyType") CompanyType companyType,
            @RequestParam("verificationState") VerificationState verificationState,
            @RequestParam("contractStatus") ContractStatus contractStatus,
            @RequestParam("cbtAvailable") boolean cbtAvailable,
            @RequestParam("gitAvailable") boolean gitAvailable,
            @RequestParam(value = "aboutCompany", required = false) String aboutCompany,
            @RequestParam(value = "specialty", required = false) String specialty
    ) {
        log.info("CompanyControllerV2 -> v1/updatePartnerCompany (API REQUEST): companyId->{},tagId->{},name->{},companyType->{},verificationState->{},contractStatus->{},cbt-{},git->{},about->{},specialty->{}", companyId, tagId, name, companyType, verificationState, contractStatus, cbtAvailable, gitAvailable, aboutCompany, specialty);

        return companyServiceV2.updateCompany(companyId, tagId, name, companyType,
                verificationState, contractStatus, cbtAvailable, gitAvailable, aboutCompany, specialty);
    }

}
