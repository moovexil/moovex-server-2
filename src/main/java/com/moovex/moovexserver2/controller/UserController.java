package com.moovex.moovexserver2.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.moovex.moovexserver2.model.PageDTO;
import com.moovex.moovexserver2.model.filters.UserFiler;
import com.moovex.moovexserver2.model.user.User;
import com.moovex.moovexserver2.service.UserService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Set;
import java.util.UUID;

import static com.moovex.moovexserver2.util.CollectionUtil.isNotNullNotEmpty;

@Slf4j
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    private final ObjectMapper jsonMapper = new ObjectMapper();

    @Deprecated
    @PutMapping("v1/updateUserFcm")
    public void updateUserFcmToken(
            @RequestHeader("user-token") String userToken,
            @RequestParam("userId") UUID userId,
            @RequestParam("fcmToken") String fcm
    ) throws  JsonProcessingException{
        log.info("UserController -> v1/updateUserFcm (API REQUEST): userToken->{}, userId->{}, fcmToken->{}", userToken, userId,fcm);
        userService.updateFcmToken(userId,fcm);
    }

    @ApiOperation(value = "Creates user in the global or private tenant if the  @FIELD->tenant is specified")
    @PostMapping("v1/createUser")
    public User createUserV2(
            @RequestHeader("user-token") String userToken,
            @RequestBody User user
    ) throws JsonProcessingException {
        log.info("UserController -> v1/createUser (API REQUEST): userToken->{}, requestObject->{}", userToken, jsonMapper.writeValueAsString(user));
        User result = userService.createUser(user);
        log.info("UserController -> v1/createUser (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
        return result;
    }

    @ApiOperation(value = "Creates user in the global or private tenant if the  @FIELD->tenant is specified")
    @PostMapping("v2/createUser")
    public User createUserV2(
            @RequestHeader(value = "user-token", required = false, defaultValue = "unknown") String userToken,
            @RequestParam("name") String name,
            @RequestParam("phoneNumber") String phoneNumber,
            @RequestParam("city") String city,
            @RequestParam("country") String country,
            @RequestParam(value = "specialty", required = false) String specialty
    ) throws JsonProcessingException {
        log.info("UserController -> v2/createUser (API REQUEST): userToken->{}, name->{}, phoneNumber->{}, city->{}, country->{}, specialty->{}", userToken,name,phoneNumber,city,country,specialty);
        User result = userService.createUserByParameters(name, phoneNumber, city, country, specialty);
        log.info("UserController -> v2/createUser (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
        return result;
    }

    @PutMapping("v1/updateUser")
    public User updateUser(
            @RequestHeader("user-token") String userToken,
            @RequestBody User user
    ) throws JsonProcessingException {
        log.info("UserController -> v1/updateUser (API REQUEST): userToken->{}, requestObject->{}", userToken, jsonMapper.writeValueAsString(user));
        User result = userService.updateUser(user);
        log.info("UserController -> v1/updateUser (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
        return result;
    }

    @GetMapping("v1/findAllUsers")
    public Set<User> findAllUsers(
            @RequestParam(value = "tenantId", required = false) UUID tenantId
    ) throws JsonProcessingException {
        log.info("UserController -> v1/findAllUsers(API REQUEST): tenantId->{}", tenantId);
        Set<User> result = userService.findAllUsers(tenantId);
        if (isNotNullNotEmpty(result)) {
            log.info("UserController -> v1/findAllUsers(API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
            return result;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No users found");
        }
    }

    @GetMapping("v1/findUserByPhoneNumber")
    public User findUserByPhoneNumber(
            @RequestParam(value = "tenantId", required = false) UUID tenantId,
            @RequestParam(name = "phoneNumber") String phoneNumber
    ) throws JsonProcessingException {
        log.info("UserController -> v1/findUserByPhoneNumber (API REQUEST): tenantId->{}, phoneNumber->{}", tenantId, phoneNumber);
        User result = userService.findUserByPhoneNumber(phoneNumber, tenantId);
        if (result != null) {
            log.info("UserController -> v1/findUserByPhoneNumber (API RESPONSE): responseObject ->{}", jsonMapper.writeValueAsString(result));
            return result;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No users found by phone number: " + phoneNumber);
        }
    }

    @GetMapping("v1/isUserPhoneNumberExist")
    public boolean isUserPhoneNumberExist(
            @RequestParam(value = "tenantId", required = false) UUID tenantId,
            @RequestParam(value = "phoneNumber") String phoneNumber
    ) {
        log.info("UserController -> v1/isUserPhoneNumberExist (API REQUEST): tenantId->{}, phoneNumber->{}", tenantId, phoneNumber);
        boolean result = userService.isUserPhoneNumberExist(phoneNumber, tenantId);
        log.info("UserController -> v1/isUserPhoneNumberExist (API RESPONSE): responseObject->{}", result);
        return result;
    }

    @GetMapping("v1/approveUserByPhoneNumber")
    public User approveUserByPhoneNumber(@RequestParam(name = "phoneNumber") String phoneNumber) throws JsonProcessingException {
        log.info("UserController -> v1/approveUserByPhoneNumber (API REQUEST): phoneNumber->{}", phoneNumber);
        User result = userService.findUserAsPerPhoneNumberAndWorkerTenant(phoneNumber);
        if (result != null) {
            log.info("UserController -> v1/approveUserByPhoneNumber (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
            return result;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No users found by phone number: " + phoneNumber);
        }
    }

    @PostMapping("v1/findUserByFilters")
    public PageDTO<User> findUserByFilters(
            @RequestBody UserFiler filter,
            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "pageSize", required = false, defaultValue = "10") int pageSize
    ) {
        log.info("UserController -> v1/findUserByFilters (API REQUEST): page->{}, pageSize->{}", page, pageSize);
        PageDTO<User> result = userService.findUsersByFilters(filter, page, pageSize);
        log.info("UserController -> v1/findUserByFilters (API RESPONSE): responseObject->{}",result);
        return result;
    }

}
