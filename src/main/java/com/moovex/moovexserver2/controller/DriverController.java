package com.moovex.moovexserver2.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.moovex.moovexserver2.model.order.Order;
import com.moovex.moovexserver2.service.DriverService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.UUID;

@Slf4j
@RestController
public class DriverController {

    @Autowired
    private DriverService driverService;

    private final ObjectMapper jsonMapper = new ObjectMapper();

    @GetMapping("v1/getDriverCurrentTask")
    public Order getDriverCurrentTask(@RequestParam("driverTagId") UUID driverTagId) {
        log.info("DriverController -> v1/getDriverCurrentTask (API REQUEST): driverTagId->{}", driverTagId);
        Order result = driverService.currentTask(driverTagId);
        if (result == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No task found for driver with tag:" + driverTagId);
        } else {
            log.info("DriverController -> v1/getDriverCurrentTask (API RESPONSE): responseObject->{}", result);
            return result;
        }
    }
}
