package com.moovex.moovexserver2.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.moovex.moovexserver2.model.dashboard.CompanyDashboard;
import com.moovex.moovexserver2.model.dashboard.order_dashboard.AdminOrderDashboard;
import com.moovex.moovexserver2.service.DashboardService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@Slf4j
@RestController
public class DashboardController {

    @Autowired
    private DashboardService dashboardService;

    private final ObjectMapper jsonMapper = new ObjectMapper();

    @GetMapping("v1/generateAdminOrderDashboard")
    public AdminOrderDashboard generateAdminOrderDashboard(
            @RequestParam(value = "tenantId") UUID tenantId,
            @RequestParam(value = "companyTagId") UUID companyTagId
    ) throws JsonProcessingException {
        log.info("DashboardController -> v1/generateAdminOrderDashboard (API REQUEST): tenantId->{}, companyTagId->{}", tenantId,companyTagId);
        AdminOrderDashboard result = dashboardService.generateAdminOrderDashboard(tenantId,companyTagId);
        log.info("DashboardController -> v1/generateAdminOrderDashboard (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
        return result;
    }

    @GetMapping("v1/generateCompanyDashboard")
    public CompanyDashboard generateCompanyDashboard (
            @RequestParam(value = "tenantId") UUID tenantId,
            @RequestParam(value = "tenantOwnerCompanyId") UUID tenantOwnerCompanyId
    ) throws JsonProcessingException{
        log.info("DashboardController -> v1/generateCompanyDashboard (API REQUEST): tenantId->{}, tenantOwnerCompanyId->{}", tenantId,tenantOwnerCompanyId);
        CompanyDashboard result = dashboardService.generateCompanyDashboard(tenantId, tenantOwnerCompanyId);
        log.info("DashboardController -> v1/generateCompanyDashboard (API RESPONSE): responseObject->{}",result);
        return result;
    }


}
