package com.moovex.moovexserver2.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.moovex.moovexserver2.entity.events.EventType;
import com.moovex.moovexserver2.entity.tracking.TrackingObjectType;
import com.moovex.moovexserver2.entity.tracking.TrackingStatus;
import com.moovex.moovexserver2.model.address.LocationStatistics;
import com.moovex.moovexserver2.model.traking.Tracking;
import com.moovex.moovexserver2.service.TrackingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Slf4j
@RestController
public class TrackingController {

    @Autowired
    private TrackingService trackingService;

    private final ObjectMapper jsonMapper = new ObjectMapper();

    @GetMapping("v1/getTrackingKeys")
    public List<Tracking> getTrackingKeys(
            @RequestParam("tenantId") UUID tenant,
            @RequestParam("userId") UUID userId,
            @RequestParam(value = "trackingKeyType", required = false) TrackingObjectType trackingObjectType
    ) throws JsonProcessingException {
        log.info("TrackingController -> v1/getTrackingKeys (API REQUEST): tenantId->{}, trackingObjectType->{}, userId->{}", tenant, trackingObjectType, userId);
        List<Tracking> result = trackingService.getTrackingKeys(tenant, trackingObjectType, userId);
        log.info("TrackingController -> v1/getTrackingKeys (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
        return result;
    }

    @PutMapping("v1/changeTrackingServiceStatus")
    public Tracking changeTrackingServiceStatus(
            @RequestParam("trackingKeyId") UUID trackingId
    ) throws JsonProcessingException {
        log.info("TrackingController -> v1/startLocationTracking (API REQUEST): trackingKeyId->{}", trackingId);
        Tracking result = trackingService.changeTrackingStatus(trackingId);
        log.info("TrackingController -> v1/startLocationTracking (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
        return result;
    }

    @GetMapping("v2/getGeoTrackingDetails")
    public String getLocationTrackingDetailsV2(
            @RequestParam("trackingKeyId") UUID trackingKeyId,
            @RequestParam("dateFromUTC") Long from,
            @RequestParam("dateToUTC") Long to,
            @RequestParam(value = "step", defaultValue = "0.1", required = false) double step //location points distance in kilometers
    ) {
        log.info("TrackingController -> v2/getLocationTrackingDetailsV2 (API REQUEST): trackingKeyId->{}, dateFrom->{}, dateTo->{}, ste0->{}", trackingKeyId, from, to, step);
        String result = trackingService.getTrackingDetails(trackingKeyId, from, to, step);
        log.info("TrackingController -> v2/getLocationTrackingDetailsV2 (API RESPONSE): responseObject->{}", result);
        return result;
    }

    @GetMapping("v1/getGeoTrackingEventsDetails")
    public String getGeoTrackingEventsDetails(
            @RequestParam("trackingKeyId") UUID trackingKeyId,
            @RequestParam("dateFromUTC") Long from,
            @RequestParam("dateToUTC") Long to
    ) {
        log.info("TrackingController -> v1/getGeoTrackingEventsDetails (API REQUEST): trackingKeyId->{}, dateFrom->{}, dateTo->{}",trackingKeyId, from, to);
        String result = trackingService.getTrackingEventDetails(trackingKeyId, from, to);
        log.info("TrackingController -> v1/getGeoTrackingEventsDetails (API RESPONSE): responseObject->{}", result);
        return result;
    }

    @PostMapping("v1/updateUserPosition")
    public void updateUserPosition(
            @RequestHeader(value = "user-token", defaultValue = "unknown") String userToken,
            @RequestParam("userId") UUID userId,
            @RequestBody List<LocationStatistics> locationReport
    ) throws JsonProcessingException {
        log.info("TrackingController -> v1/updateUserPosition (API REQUEST): userId->{}, locations->{}", userId, jsonMapper.writeValueAsString(locationReport));
        trackingService.saveTrackingInformation(userId, locationReport);
    }

    @PutMapping("v1/updateTrackingStatus")
    public void updateTrackingStatus(
            @RequestParam("objectId") UUID objectId,
            @RequestParam("status") TrackingStatus status
    ) {
        log.info("TrackingController -> v1/updateTrackingStatus (API REQUEST): objectId->{}, status->{}", objectId, status);
        trackingService.updateTrackingInformation(objectId, status);
    }
}
