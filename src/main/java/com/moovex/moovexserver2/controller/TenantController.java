package com.moovex.moovexserver2.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.moovex.moovexserver2.model.tenant.Tenant;
import com.moovex.moovexserver2.service.TenantService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import springfox.documentation.spring.web.json.Json;

import java.util.Set;

import static com.moovex.moovexserver2.util.CollectionUtil.isNotNullNotEmpty;

@Slf4j
@RestController
public class TenantController {

    @Autowired
    private TenantService tenantService;

    private final ObjectMapper jsonMapper = new ObjectMapper();

    @PostMapping("v1/createTenant")
    public Tenant createTenant(
            @RequestHeader("user-token") String userToken,
            @RequestParam("tenantName") String name
    ) throws JsonProcessingException {
        log.info("TenantController -> v1/createTenant (API REQUEST): userToken->{}, tenantName->{}", userToken,name);
        Tenant result = tenantService.createTenant(name.toLowerCase());
        log.info("TenantController -> v1/createTenant (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
        return result;
    }

    @GetMapping("v1/findAllTenants")
    public Set<Tenant> findAllTenants() throws JsonProcessingException {
        Set<Tenant> result = tenantService.findAllTenants();
        if (isNotNullNotEmpty(result)) {
            log.info("TenantController -> v1/findAllTenants (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
            return result;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No tenants found in the DB");
        }
    }

}
