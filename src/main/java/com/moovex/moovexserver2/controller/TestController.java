package com.moovex.moovexserver2.controller;

import com.moovex.moovexserver2.service_test.FeedTestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class TestController {

    @Autowired
    private FeedTestService feedTestService;

    @PostMapping("v1/generateFeeds")
    public void generateFeeds(@RequestParam(value = "numberOfFeeds", defaultValue = "30") int amount) {
        feedTestService.createFeedsWithoutComments(amount);
    }

    @PostMapping("v1/generateComments")
    public void generateComments(
            @RequestParam(value = "numberOfComments", defaultValue = "30") int amount,
            @RequestParam("feedId") UUID feedId
    ) {
        feedTestService.createCommentsForPost(feedId,amount);
    }

}
