package com.moovex.moovexserver2.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.moovex.moovexserver2.entity.tag.TagType;
import com.moovex.moovexserver2.model.tag.Tag;
import com.moovex.moovexserver2.service.TagGeneratorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Set;
import java.util.UUID;

import static com.moovex.moovexserver2.util.CollectionUtil.isNotNullNotEmpty;

@Slf4j
@RestController
public class TagController {

    @Autowired
    private TagGeneratorService tagService;

    private final ObjectMapper jsonMapper = new ObjectMapper();

    @GetMapping("v1/findTagsByNameLike")
    public Set<Tag> findTagsByNameLike(
            @RequestParam("tagNameLike") String tagNameLike,
            @RequestParam("tagType") TagType tagType
    ) throws JsonProcessingException {
        log.info("TagController -> v1/findTagsByNameLike (API REQUEST): tagNameLike->{}, tagType->{}", tagNameLike, tagType);
        Set<Tag> result = tagService.findTagsByName(tagNameLike, tagType);
        if (isNotNullNotEmpty(result)){
            log.info("TagController -> v1/findTagsByNameLike (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
            return result;
        }else {
            throw  new ResponseStatusException(HttpStatus.NOT_FOUND, "No tags found for tagName: " + tagNameLike + " and tagType:" + tagType);
        }
    }

    @PutMapping("v1/setUserTag")
    public void setUserTag(
            @RequestHeader("user-token") String userToken,
            @RequestParam("userId") UUID userId,
            @RequestParam("tagId") UUID tagId
    ) {
        log.info("TagController -> v1/setUserTag (API REQUEST): userToken->{}, userId->{}, tagId->{}", userToken, userId, tagId);
        tagService.addTagToUser(userId, tagId);
    }

    @PutMapping("v1/setCompanyTag")
    public void setCompanyTag(
            @RequestHeader("user-token") String userToken,
            @RequestParam("companyId") UUID companyId,
            @RequestParam("tagId") UUID tagId
    ) {
        log.info("TagController -> v1/setCompanyTag (API REQUEST): userToken->{}, companyId->{}, tagId->{}", userToken, companyId, tagId);
        tagService.addTagToCompany(companyId, tagId);
    }

}
