package com.moovex.moovexserver2.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.moovex.moovexserver2.entity.order.OrderShipmentStatus;
import com.moovex.moovexserver2.entity.order.OrderStatus;
import com.moovex.moovexserver2.model.PageDTO;
import com.moovex.moovexserver2.model.address.LocationStatistics;
import com.moovex.moovexserver2.model.address.LocationReport;
import com.moovex.moovexserver2.model.order.Order;
import com.moovex.moovexserver2.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.UUID;

import static com.moovex.moovexserver2.util.CollectionUtil.isNotNullNotEmpty;

@Slf4j
@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    private final ObjectMapper jsonMapper = new ObjectMapper();

    @PostMapping("v1/createOrder")
    public Order createOrder(
            @RequestHeader("user-token") String userToken,
            @RequestParam("companyId") UUID companyId,
            @RequestBody Order order
    ) throws JsonProcessingException {
        log.info("OrderController -> v1/createOrder (API REQUEST): userToken->{}, companyId->{}, order->{}", userToken, companyId, jsonMapper.writeValueAsString(order));
        Order result = orderService.createOrder(order, companyId);
        log.info("OrderController -> v1/createOrder (API RESPONSE): resultObject->{}", jsonMapper.writeValueAsString(order));
        return result;
    }

    @PutMapping("v1/updateOrder")
    public Order updateUser(
            @RequestHeader("user-token") String userToken,
            @RequestBody Order order
    ) throws JsonProcessingException {
        log.info("OrderController -> v1/updateOrder (API REQUEST): userToken->{}, order->{}", userToken, jsonMapper.writeValueAsString(order));
        Order result = orderService.updateOrder(order);
        log.info("OrderController -> v1/updateOrder (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(order));
        return result;
    }

    @GetMapping("v1/getPublicOrders")
    public PageDTO<Order> getPublicOrders(
            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "pageSize", required = false, defaultValue = "10") int pageSize
    ) throws JsonProcessingException {
        log.info("OrderController -> v1/getPublicOrders (API REQUEST): page->{}, pageSize->{}", page, pageSize);
        PageDTO<Order> result = orderService.getPublicOrders(page, pageSize);
        log.info("OrderController -> v1/getPublicOrders (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
        return result;
    }

    @PutMapping("v1/leftContactInfo")
    public void leftContactInfo(
            @RequestParam("orderId") UUID orderId,
            @RequestParam("userId") UUID userId
    ) {
        log.info("OrderController -> v1/leftContactInfo (API REQUEST): orderId->{}, userId->{}", orderId, userId);
        orderService.leftContactInfo(orderId, userId);
    }

    @GetMapping("v1/getOrderByOrderStatus")
    public PageDTO<Order> getOrderByOrderStatus(
            @RequestParam(value = "tenantId") UUID tenantId,
            @RequestParam(value = "companyTagId") UUID companyTagId,
            @RequestParam(value = "orderStatus") OrderStatus orderStatus,
            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "pageSize", required = false, defaultValue = "10") int pageSize
    ) throws JsonProcessingException {
        log.info("OrderController -> v1/getOrderByOrderStatus (API REQUEST): tenantId->{}, companyId->{}, page->{}, pageSize->{}, orderStatus->{}", tenantId, companyTagId, page, pageSize, orderStatus);
        PageDTO<Order> result = orderService.findOrdersByOrderStatus(orderStatus, tenantId, companyTagId, page, pageSize);
        log.info("OrderController -> v1/getOrderByOrderStatus (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
        return result;
    }

    @GetMapping("v1/getOrderByShipmentStatus")
    public PageDTO<Order> getOrderByOrderStatus(
            @RequestParam(value = "tenantId") UUID tenantId,
            @RequestParam(value = "companyTagId") UUID companyTagId,
            @RequestParam(value = "shipmentStatus") OrderShipmentStatus shipmentStatus,
            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "pageSize", required = false, defaultValue = "10") int pageSize
    ) throws JsonProcessingException {
        log.info("OrderController -> v1/getOrderByShipmentStatus (API REQUEST): tenantId->{}, companyId->{}, page->{}, pageSize->{}, shipmentStatus->{}", tenantId, companyTagId, page, pageSize, shipmentStatus);
        PageDTO<Order> result = orderService.findOrdersByShipmentStatus(shipmentStatus, tenantId, companyTagId, page, pageSize);
        log.info("OrderController -> v1/getOrderByShipmentStatus (API RESPONSE): responseObject->{}", jsonMapper.writeValueAsString(result));
        return result;
    }

    @PutMapping("v1/setOrderCargoLocation")
    public void setOrderCargoLocation(
            @RequestHeader("user-token") String userToken,
            @RequestParam("orderId") UUID orderId,
            @RequestBody LocationReport locationReport
    ) throws JsonProcessingException {
        log.info("OrderController -> v1/setOrderCargoLocation (API REQUEST): userToken->{}, orderId->{}, locationReport->{}", userToken, orderId, jsonMapper.writeValueAsString(locationReport));

        if (isNotNullNotEmpty(locationReport.getLocations())) {
            for (LocationStatistics location : locationReport.getLocations()) {
                if (location != null) {
                    orderService.setOrderCargoLocation(orderId, location);
                }
            }
        }
    }

    @GetMapping("v1/getOrderCargoRoute")
    public List<LocationStatistics> getOrderCargoRoute(@RequestParam("orderId") UUID orderId) {
        log.info("OrderController -> v1/getOrderCargoRoute (API REQUEST): orderId->{}", orderId);
        List<LocationStatistics> result = orderService.getOrderCargoRoute(orderId);
        if (isNotNullNotEmpty(result)) {
            log.info("OrderController -> v1/getOrderCargoRoute (API RESPONSE): responseObject->{}", result);
            return result;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No location information found for this orderId:" + orderId);
        }
    }

}
