package com.moovex.moovexserver2.service;

import com.moovex.moovexserver2.entity.company.CompanyEntity;
import com.moovex.moovexserver2.entity.user.UserEntity;
import com.moovex.moovexserver2.entity.user.UserType;
import com.moovex.moovexserver2.model.user.User;
import com.moovex.moovexserver2.repository.CompanyRepository;
import com.moovex.moovexserver2.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.*;

@Transactional
@Service
public class CompanyEmployeeServiceImpl implements CompanyEmployeeService {

    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;

    @Override
    public void addUserLikeEmployee(@NonNull UUID userId, @Nullable UserType newUserRole, @NonNull UUID companyId) {
        User employee = userService.copyAndDisableOldUser(userId, newUserRole, companyId);
        addEmployeeToCompany(employee.getId(),companyId);
    }

    private void addEmployeeToCompany(@NonNull UUID employeeId, @NonNull UUID companyId){
        CompanyEntity company = findCompany(companyId);
        UserEntity employee = findUser(employeeId);
        Set<UserEntity> employees = company.getEmployees();

        if (employees == null) {
            employees = new HashSet<>();
        }

        employees.add(employee);

        companyRepository.save(company);
    }


    private CompanyEntity findCompany(@NonNull UUID id) {
        Optional<CompanyEntity> o = companyRepository.findCompanyById(id);
        if (o.isPresent()) {
            return o.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Company with this id: " + id + " wan't found in the DB");
        }
    }

    private UserEntity findUser(@NonNull UUID id) {
        Optional<UserEntity> o = userRepository.findUserById(id);
        if (o.isPresent()) {
            return o.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User with this id: " + id + " wan't found in the DB");
        }
    }
}
