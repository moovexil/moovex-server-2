package com.moovex.moovexserver2.service;

import com.moovex.moovexserver2.entity.tracking.TrackingObjectType;
import com.moovex.moovexserver2.entity.tracking.TrackingStatus;
import com.moovex.moovexserver2.model.address.LocationStatistics;
import com.moovex.moovexserver2.model.traking.Tracking;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.List;
import java.util.UUID;

public interface TrackingService {

    Tracking changeTrackingStatus(@NonNull UUID trackingId);

    @NonNull
    List<Tracking> getTrackingKeys(@NonNull UUID tenantId, @Nullable TrackingObjectType trackingObjectType, @NonNull UUID userId);

    @NonNull
    String getTrackingDetails(
            @NonNull UUID trackingId,
            @NonNull Long from,
            @NonNull Long to,
            double step
    );

    @NonNull
    String getTrackingEventDetails(
            @NonNull UUID trackingId,
            @NonNull Long from,
            @NonNull Long to
    );

    void saveTrackingInformation(@NonNull UUID userId, @NonNull List<LocationStatistics> locationReport);

    void updateTrackingInformation(@NonNull UUID objectId, @NonNull TrackingStatus newStatus);

}
