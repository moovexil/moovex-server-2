package com.moovex.moovexserver2.service;

import com.moovex.moovexserver2.entity.events.EventEntity;
import com.moovex.moovexserver2.entity.events.EventType;
import com.moovex.moovexserver2.entity.tracking.TrackingEntity;
import com.moovex.moovexserver2.repository.EventRepository;
import com.moovex.moovexserver2.repository.TrackingRepository;
import com.moovex.moovexserver2.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
public class EventServiceImpl implements EventService {

    @Autowired
    EventRepository eventRepository;

    @Autowired
    TrackingRepository trackingRepository;

    @Override
    public void createEvent(@NonNull UUID trackingObjectId, @NonNull EventType type, @NonNull Long eventTimestamp) {
        TrackingEntity trackingEntity = findTrackingByTrackingObjectId(trackingObjectId);

        EventEntity newEvent = new EventEntity();
        newEvent.setTrackingKey(trackingEntity);
        newEvent.setType(type);
        newEvent.setEventTimestamp(eventTimestamp);

        eventRepository.save(newEvent);
    }


    private TrackingEntity findTrackingByTrackingObjectId(@NonNull UUID userId) {
        Optional<TrackingEntity> tracking = trackingRepository.findTrackingByTrackingObjectId(userId);
        if (tracking.isPresent()) {
            return tracking.get();
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Tracking by this object id not found :" + userId);
        }
    }

}
