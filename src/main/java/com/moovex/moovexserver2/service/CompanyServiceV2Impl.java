package com.moovex.moovexserver2.service;

import com.moovex.moovexserver2.entity.company.CompanyEntity;
import com.moovex.moovexserver2.entity.company.CompanyType;
import com.moovex.moovexserver2.entity.company.ContractStatus;
import com.moovex.moovexserver2.entity.tenant.TenantEntity;
import com.moovex.moovexserver2.entity.user.UserType;
import com.moovex.moovexserver2.entity.user.VerificationState;
import com.moovex.moovexserver2.mapper.*;
import com.moovex.moovexserver2.model.company.Company;
import com.moovex.moovexserver2.repository.CompanyRepository;
import com.moovex.moovexserver2.repository.TenantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

@Transactional
@Service
public class CompanyServiceV2Impl implements CompanyServiceV2 {

    @Autowired
    TagGeneratorServiceV2 tagGeneratorService;

    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    CompanyEmployeeService companyEmployeeService;

    @Autowired
    TenantRepository tenantRepository;

    @Autowired
    CompanyMapper companyMapper;


    @Override
    public Company createPartnerCompany(
            @NonNull UUID companyTenantId,
            @NonNull String name,
            @NonNull CompanyType companyType,
            @NonNull VerificationState verificationState,
            @NonNull ContractStatus contractStatus,
            boolean cbtAvailable,
            boolean gitAvailable,
            @Nullable String aboutCompany,
            @Nullable String specialty
    ) {
        CompanyEntity company = new CompanyEntity();
        company.setName(name);
        company.setCompanyType(companyType);
        company.setVerificationState(verificationState);
        company.setContractStatus(contractStatus);
        company.setCbtAvailable(cbtAvailable);
        company.setGitAvailable(gitAvailable);
        company.setSpecialty(specialty);
        company.setAbout(aboutCompany);
        company.setTenant(findTenant(companyTenantId));

        CompanyEntity newCompany = companyRepository.save(company);
        return companyMapper.toCompany(newCompany);
    }

    @Override
    public Company createUserCompany(
            @NonNull UUID companyAdminId,
            @NonNull String name,
            @NonNull CompanyType companyType,
            @NonNull VerificationState verificationState,
            @NonNull ContractStatus contractStatus,
            boolean cbtAvailable,
            boolean gitAvailable,
            @Nullable String aboutCompany,
            @Nullable String specialty
    ) {
        CompanyEntity company = new CompanyEntity();
        company.setName(name);
        company.setCompanyType(companyType);
        company.setVerificationState(verificationState);
        company.setContractStatus(contractStatus);
        company.setCbtAvailable(cbtAvailable);
        company.setGitAvailable(gitAvailable);
        company.setSpecialty(specialty);
        company.setAbout(aboutCompany);
        company.setTenant(createCompanyTenant(name));
        //create new user company
        CompanyEntity newCompany = companyRepository.save(company);
        //create and add tag to the company
        tagGeneratorService.createAndAddCompanyTag(newCompany.getId());
        //add administrator user to the company
        companyEmployeeService.addUserLikeEmployee(companyAdminId, UserType.ADMIN, newCompany.getId());

        return companyMapper.toCompany(findCompany(newCompany.getId()));
    }

    @Override
    public Company updateCompany(
            @NonNull UUID companyId,
            UUID tagId, @NonNull String name,
            @NonNull CompanyType companyType,
            @NonNull VerificationState verificationState,
            @NonNull ContractStatus contractStatus,
            boolean cbtAvailable,
            boolean gitAvailable,
            @Nullable String aboutCompany,
            @Nullable String specialty
    ) {
       CompanyEntity company = findCompany(companyId);
        company.setName(name);
        company.setCompanyType(companyType);
        company.setVerificationState(verificationState);
        company.setContractStatus(contractStatus);
        company.setCbtAvailable(cbtAvailable);
        company.setGitAvailable(gitAvailable);
        company.setSpecialty(specialty);
        company.setAbout(aboutCompany);

        companyRepository.save(company);

        if (tagId != null){
            tagGeneratorService.addTagToCompany(companyId,tagId);
        }

        return companyMapper.toCompany(findCompany(companyId));
    }

    @NonNull
    private TenantEntity createCompanyTenant(@NonNull String companyName) {
        String tenantName = companyName.toLowerCase().replaceAll("\\s", "") + "_" + UUID.randomUUID();
        TenantEntity newTenant = new TenantEntity(null, tenantName);
        return tenantRepository.save(newTenant);
    }


    private CompanyEntity findCompany(@NonNull UUID id) {
        Optional<CompanyEntity> o = companyRepository.findCompanyById(id);
        if (o.isPresent()) {
            return o.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Company with this id: " + id + " wan't found in the DB");
        }
    }

    private TenantEntity findTenant(@NonNull UUID id) {
        Optional<TenantEntity> tenant = tenantRepository.findTenantById(id);
        if (tenant.isPresent()) {
            return tenant.get();
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "This tenant does not exist:" + id);
        }
    }

}
