package com.moovex.moovexserver2.service;

import com.moovex.moovexserver2.entity.events.EventType;
import org.springframework.lang.NonNull;

import java.util.UUID;

public interface EventService {

    void createEvent(@NonNull UUID trackingObjectId, @NonNull EventType type, @NonNull Long eventTimestamp);


}
