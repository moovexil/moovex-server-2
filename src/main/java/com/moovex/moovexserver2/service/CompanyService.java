package com.moovex.moovexserver2.service;

import com.moovex.moovexserver2.entity.company.CompanyType;
import com.moovex.moovexserver2.entity.company.ContractStatus;
import com.moovex.moovexserver2.entity.user.UserType;
import com.moovex.moovexserver2.entity.user.VerificationState;
import com.moovex.moovexserver2.model.user.User;
import com.moovex.moovexserver2.model.vechile.TractorHead;
import com.moovex.moovexserver2.model.vechile.Trailer;
import com.moovex.moovexserver2.model.vechile.Truck;
import com.moovex.moovexserver2.model.company.CompanyFull;
import com.moovex.moovexserver2.model.company.Company;
import com.moovex.moovexserver2.model.user.UserPreview;
import com.sun.istack.NotNull;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.Set;
import java.util.UUID;

public interface CompanyService {

    @NonNull
    CompanyFull findCompanyById(@NonNull UUID id);

    @NonNull
    CompanyFull createCompany(@NonNull CompanyFull companyFull, boolean isPrivate, @Nullable UUID tenantId);

    @NonNull
    CompanyFull updateCompany(@NonNull CompanyFull companyFull);

    boolean isCompanyPhoneNumberExist(@Nullable UUID tenantId, @NonNull String phoneNumber);

    @Nullable
    Set<CompanyFull> findAllCompanies(
            @NotNull UUID tenant,
            @Nullable CompanyType companyType,
            @Nullable VerificationState verificationState,
            @Nullable ContractStatus contractStatus
    );

    @Nullable
    Set<Company> findTransportersPreviews(@NotNull UUID tenantId);

    @Nullable
    Set<Company> findAllCompaniesPreview(@Nullable UUID tenant, @Nullable CompanyType companyType);

    @Nullable
    Set<UserPreview> findCompanyEmployeePreview(@NonNull UUID companyId, @Nullable UserType... userType);

    @NonNull
    CompanyFull addEmployeeToCompany(UUID userId, UUID companyId, UserType tenantUserRole);

    @NonNull
    User createCompanyEmployee (@NonNull UUID companyId,@NonNull User employee);

    //-------------------------------- Vehicles -----------------------------------------

    @NonNull
    Truck createTruck (@NonNull UUID companyId,@NonNull Truck truck);

    @NonNull
    Trailer createTrailer (@NonNull UUID companyId,@NonNull Trailer trailer);

    @NonNull
    TractorHead createTractor (@NonNull UUID companyId,@NonNull TractorHead tractor);

    @Nullable
    Set<Trailer> findCompanyTrailers(@NonNull UUID companyId);

    @Nullable
    Set<Truck> findCompanyTrucks(@NonNull UUID companyId);

    @Nullable
    Set<TractorHead> findCompanyTractors (@NonNull UUID companyId);

}
