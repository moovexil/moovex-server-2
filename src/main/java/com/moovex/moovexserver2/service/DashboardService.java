package com.moovex.moovexserver2.service;

import com.moovex.moovexserver2.model.dashboard.CompanyDashboard;
import com.moovex.moovexserver2.model.dashboard.order_dashboard.AdminOrderDashboard;
import com.sun.istack.NotNull;
import org.springframework.lang.NonNull;

import java.util.UUID;

public interface DashboardService {

    @NonNull
    AdminOrderDashboard generateAdminOrderDashboard(@NotNull UUID tenantId, @NotNull UUID companyTagId);

    @NonNull
    CompanyDashboard generateCompanyDashboard(@NonNull UUID tenantId, @NonNull UUID tenantOwnerCompanyId);

}
