package com.moovex.moovexserver2.service;

import com.moovex.moovexserver2.model.tenant.Tenant;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.Set;

public interface TenantService {

    @NonNull
    Tenant createTenant(@NonNull String name);

    boolean isExist(@NonNull String tenantName);

    @Nullable
    Set<Tenant> findAllTenants();

}
