package com.moovex.moovexserver2.service;

import com.moovex.moovexserver2.entity.user.UserType;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.UUID;

public interface CompanyEmployeeService {

    void addUserLikeEmployee(@NonNull UUID userId, @Nullable UserType newUserRole, @NonNull UUID companyId);

}
