package com.moovex.moovexserver2.service;

import com.moovex.moovexserver2.entity.tag.TagType;
import com.moovex.moovexserver2.model.tag.Tag;
import org.springframework.lang.NonNull;

import java.util.Set;
import java.util.UUID;

public interface TagGeneratorService {

    @NonNull
    Tag createCompanyTag(@NonNull UUID companyId);

    @NonNull
    Tag createUserTag(@NonNull UUID userId);

    Set<Tag> findTagsByName(@NonNull String nameQuery, @NonNull TagType tagType);

    void addTagToCompany(@NonNull UUID companyId, @NonNull UUID tagId);

    void addTagToUser(@NonNull UUID userId, @NonNull UUID tagId);
}
