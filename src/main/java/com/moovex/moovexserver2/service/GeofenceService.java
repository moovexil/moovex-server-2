package com.moovex.moovexserver2.service;

import com.moovex.moovexserver2.model.geofence.geofence_event.GeofenceEventType;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.UUID;

public interface GeofenceService {

    void registerGeofence(@NonNull UUID placeId, @NonNull UUID trackingId, float radius,@Nullable Long dwellDelay);

    void unregisterGeofence(@NonNull UUID geofenceId);

    void updateGeofenceRegistrationState(@NonNull UUID geofenceId, boolean registered);

    void createGeofenceEvent(
            @NonNull GeofenceEventType eventType,
            long timestamp,
            @Nullable UUID geofenceId,
            @Nullable String rationale,
            @Nullable Double latitude,
            @Nullable Double longitude
    );
}
