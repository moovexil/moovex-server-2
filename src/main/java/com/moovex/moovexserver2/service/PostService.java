package com.moovex.moovexserver2.service;

import com.moovex.moovexserver2.model.PageDTO;
import com.moovex.moovexserver2.model.post.Comment;
import com.moovex.moovexserver2.model.post.Post;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.UUID;

public interface PostService {

    @NonNull
    Post createPost(@NonNull String feed, @Nullable String imgUrl, @NonNull UUID creatorId);

    @NonNull
    PageDTO<Post> getAllPosts(int page, int size);

    PageDTO<Comment> getFeedComments(@NonNull UUID feedId, int page, int size);

    @NonNull
    Comment addComment(@NonNull String text, @NonNull UUID creatorId, @NonNull UUID feedId);

    @Nullable
    Post findCommentById(@NonNull UUID postId);


}
