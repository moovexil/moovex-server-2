package com.moovex.moovexserver2.service;

import com.moovex.moovexserver2.entity.place.PlaceEntity;
import com.moovex.moovexserver2.entity.tracking.TrackingEntity;
import com.moovex.moovexserver2.model.geofence.GeofenceEntity;
import com.moovex.moovexserver2.model.geofence.StartingState;
import com.moovex.moovexserver2.model.geofence.geofence_event.GeofenceEventEntity;
import com.moovex.moovexserver2.model.geofence.geofence_event.GeofenceEventType;
import com.moovex.moovexserver2.repository.GeofenceEventRepository;
import com.moovex.moovexserver2.repository.GeofenceRepository;
import com.moovex.moovexserver2.repository.PlaceRepository;
import com.moovex.moovexserver2.repository.TrackingRepository;
import org.locationtech.jts.geom.Point;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.*;

import static com.moovex.moovexserver2.service.MessengerServiceImpl.*;
import static com.moovex.moovexserver2.service.PlaceServiceImpl.latLongToPoint;
import static com.moovex.moovexserver2.service.PlaceServiceImpl.pointToGeoJson;

@Service
@Transactional
public class GeofenceServiceImpl implements GeofenceService {

    @Autowired
    GeofenceRepository geofenceRepository;

    @Autowired
    PlaceRepository placeRepository;

    @Autowired
    TrackingRepository trackingRepository;

    @Autowired
    MessengerService messengerService;

    @Autowired
    GeofenceEventRepository geofenceEventRepository;

    @Override
    public void registerGeofence(@NonNull UUID placeId, @NonNull UUID trackingId, float radius, Long dwellDelay) {
        PlaceEntity place = findPlaceById(placeId);
        TrackingEntity tracking = findTrackingById(trackingId);
        String objectFcm = findTrackingObjectFcm(trackingId);
        GeofenceEntity geofence = createGeofence(place, tracking, radius,dwellDelay,null,null);
        Map<String, String> properties = createGeofenceMessageProperties(true, geofence);
        messengerService.sendMessage(objectFcm, properties);
    }

    @Override
    public void unregisterGeofence(@NonNull UUID geofenceId) {
        GeofenceEntity geofence = findGeofenceById(geofenceId);
        geofence.setStartingState(StartingState.UNREGISTERED);

        String objectFcm = findTrackingObjectFcm(geofence.getTracking().getId());
        Map<String, String> properties = createGeofenceMessageProperties(false, geofence);
        messengerService.sendMessage(objectFcm, properties);
    }

    private Map<String, String> createGeofenceMessageProperties(boolean register, @NonNull GeofenceEntity geofence) {
        Map<String, String> properties = new HashMap<>();
        properties.put(PROPERTY_GEOFENCE_ID, geofence.getId().toString());

        if (register) {
            properties.put(PROPERTY_EVENT_TYPE, MessageEvent.EVENT_REGISTER_GEOFENCE.name());
            properties.put(PROPERTY_GEOFENCE_NAME, geofence.getLandmarkName());
            properties.put(PROPERTY_GEOFENCE_DESC, geofence.getLandmarkDescription());
            properties.put(PROPERTY_GEOFENCE_LOCATION, pointToGeoJson(geofence.getLocation()));
            properties.put(PROPERTY_GEOFENCE_RADIUS,String.valueOf(geofence.getRadius()));

            if (geofence.getDwellDelay() != null){
                properties.put(PROPERTY_GEOFENCE_DWELL_DELAY,String.valueOf(geofence.getDwellDelay()));
            }

        } else {
            properties.put(PROPERTY_EVENT_TYPE, MessageEvent.EVENT_UNREGISTER_GEOFENCE.name());
        }

        return properties;
    }

    private GeofenceEntity createGeofence(
            @NonNull PlaceEntity place,
            @NonNull TrackingEntity tracking,
            float radius,
            @Nullable Long dwellDelay,
            @Nullable GeofenceEntity parentGeofence,
            @Nullable List<GeofenceEntity> childGeofences
    ) {
        GeofenceEntity geofence = new GeofenceEntity();
        geofence.setLandmarkName(place.getName());
        geofence.setLandmarkDescription(place.getDescription());
        geofence.setStartingState(StartingState.NOT_REGISTERED);
        geofence.setLocation(place.getLocation());
        geofence.setTracking(tracking);
        geofence.setParent(parentGeofence);
        geofence.setChild(childGeofences);
        geofence.setRadius(radius);
        geofence.setDwellDelay(dwellDelay);
        return geofenceRepository.save(geofence);
    }

    @Override
    public void updateGeofenceRegistrationState(@NonNull UUID geofenceId, boolean registered) {
        GeofenceEntity geofence = findGeofenceById(geofenceId);
        if (registered) {
            geofence.setStartingState(StartingState.REGISTERED);
        } else {
            geofence.setStartingState(StartingState.UNREGISTERED);
        }
    }

    @Override
    public void createGeofenceEvent(
            @NonNull GeofenceEventType eventType,
            long timestamp,
            @Nullable UUID geofenceId,
            @Nullable String rationale,
            @Nullable Double latitude,
            @Nullable Double longitude
    ) {

        GeofenceEventEntity event = new GeofenceEventEntity();
        event.setEventType(eventType);
        event.setRationale(rationale);
        event.setTimestamp(timestamp);

        if (geofenceId != null){
            GeofenceEntity geofence = findGeofenceById(geofenceId);
            event.setHostGeofence(geofence);
        }

        if (latitude != null && longitude != null){
          Point point = latLongToPoint(latitude,longitude);
          event.setLocation(point);
        }

        GeofenceEventEntity resultEvent = geofenceEventRepository.save(event);

        //TEMPORARY SOLUTION in error type event we don't have geofence information
        if (resultEvent.getEventType() != GeofenceEventType.ERROR){
            sendGeofenceEventToManagers(resultEvent);
        }
    }

    private void sendGeofenceEventToManagers(GeofenceEventEntity event) {
        UUID trackingId = event.getHostGeofence().getTracking().getId();
        String trackingObjectName = findTrackingObjectName(trackingId);

        UUID trackingObjectId = event.getHostGeofence().getTracking().getTrackingObjectId();
        String trackingObjectCompanyId = trackingRepository.findTrackingObjectCompany(trackingObjectId);
        List<String> trackingObjectManagersFcms = trackingRepository.findManagersFcmTokens(UUID.fromString(trackingObjectCompanyId));

        Map<String, String> properties = new HashMap<>();
        properties.put(PROPERTY_EVENT_TYPE, MessageEvent.EVENT_GEOFENCE_TRANSITION.name());
        properties.put(PROPERTY_GEOFENCE_NAME, event.getHostGeofence().getLandmarkName());
        properties.put(PROPERTY_GEOFENCE_DESC, event.getHostGeofence().getLandmarkDescription());
        properties.put(PROPERTY_GEOFENCE_TRANSITION,event.getEventType().name());
        properties.put(PROPERTY_TRACKING_OBJECT_DISPLAY_NAME,trackingObjectName);

        messengerService.sendMessages(trackingObjectManagersFcms,properties);
    }

    @NonNull
    private PlaceEntity findPlaceById(@NonNull UUID placeId) {
        Optional<PlaceEntity> place = placeRepository.findPlaceById(placeId);
        if (place.isPresent()) {
            return place.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Place with this id: " + placeId + " wasn't found in the DB");
        }
    }

    @NonNull
    private TrackingEntity findTrackingById(@NonNull UUID trackingId) {
        Optional<TrackingEntity> tracking = trackingRepository.findTrackingById(trackingId);
        if (tracking.isPresent()) {
            return tracking.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Tracking with this id: " + tracking + " wasn't found in the DB");
        }
    }

    @NonNull
    private GeofenceEntity findGeofenceById(@NonNull UUID geofenceId) {
        Optional<GeofenceEntity> geofence = geofenceRepository.findGeofenceById(geofenceId);
        if (geofence.isPresent()) {
            return geofence.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Geofence with this id: " + geofenceId + " wasn't found in the DB");
        }
    }

    private String findTrackingObjectFcm(@NonNull UUID trackingId) {
        Optional<String> fcm = trackingRepository.findUserTypeTrackingObjectFcm(trackingId);
        if (fcm.isPresent()) {
            return fcm.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "FCM for this trackingId: " + trackingId + " wasn't found in the DB");
        }
    }

    private String findTrackingObjectName(@NonNull UUID trackingId) {
        Optional<String> fcm = trackingRepository.findUserTypeTrackingObjectName(trackingId);
        if (fcm.isPresent()) {
            return fcm.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Display name for this trackingId: " + trackingId + " wasn't found in the DB");
        }
    }

}
