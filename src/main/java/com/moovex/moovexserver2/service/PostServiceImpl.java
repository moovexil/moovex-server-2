package com.moovex.moovexserver2.service;

import com.moovex.moovexserver2.entity.post.CommentEntity;
import com.moovex.moovexserver2.entity.post.PostEntity;
import com.moovex.moovexserver2.entity.user.UserEntity;
import com.moovex.moovexserver2.mapper.CommentMapper;
import com.moovex.moovexserver2.mapper.PostMapper;
import com.moovex.moovexserver2.model.PageDTO;
import com.moovex.moovexserver2.model.post.Comment;
import com.moovex.moovexserver2.model.post.Post;
import com.moovex.moovexserver2.repository.CommentRepository;
import com.moovex.moovexserver2.repository.PostRepository;
import com.moovex.moovexserver2.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.moovex.moovexserver2.util.CollectionUtil.findFirstOrNull;
import static com.moovex.moovexserver2.util.CriteriaUtil.*;


@Service
@Transactional
public class PostServiceImpl implements PostService {

    @Autowired
    PostMapper postMapper;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PostRepository postRepository;

    @Autowired
    CommentRepository commentRepository;

    @Autowired
    CommentMapper commentMapper;

    @Override
    public Post createPost(String feed, String imgUrl, UUID creatorId) {
        UserEntity creatorEntity = findUser(creatorId);
        PostEntity entity = new PostEntity();
        entity.setCreator(creatorEntity);
        entity.setText(feed);
        entity.setImgUrl(imgUrl);
        entity.setComments(new ArrayList<>());

        PostEntity result = postRepository.save(entity);
        return postMapper.toDomain(result);
    }


    @Override
    public PageDTO<Post> getAllPosts(int page, int size) {
        Pageable pageable = getSimplePageable(page, size, sortByCreationDesc());
        Page<PostEntity> postPage = postRepository.findAll(pageable);
        return postMapper.toDomainPage(postPage);
    }

    @NonNull
    @Override
    public Comment addComment(@NonNull String text, @NonNull UUID creatorId, @NonNull UUID feedId) {
        PostEntity post = findPost(feedId);
        List<CommentEntity> comments = post.getComments();

        CommentEntity newComment = createCommentEntity(text,creatorId, feedId);
        comments.add(newComment);

        PostEntity updatedPost = postRepository.save(post);
        CommentEntity newCommentResult = findFirstOrNull(updatedPost.getComments(),comment -> comment.getId() == newComment.getId());

        if (newCommentResult != null){
            return commentMapper.toDomain(newCommentResult);
        }else {
            throw  new ResponseStatusException(HttpStatus.NOT_FOUND, "Can't find created comment inside the post, Post: " + updatedPost + " CommentId: " + newComment.getId());
        }
    }

    @Override
    public PageDTO<Comment> getFeedComments(UUID feedId, int page, int size) {
        Pageable pageable = getSimplePageable(page, size, sortByCreationDesc());
        Page<CommentEntity> commentPage = commentRepository.findAll(pageable);
        return commentMapper.toDomainPage(commentPage);
    }

    @Override
    public Post findCommentById(UUID postId) {
        PostEntity result = findPost(postId);
        return postMapper.toDomain(result);
    }

    //------------------------- Helpers ---------------------------------

    private CommentEntity createCommentEntity(String text, UUID creatorId, UUID feedId) {
        UserEntity creatorUser = findUser(creatorId);
        PostEntity feed = findPost(feedId);
        CommentEntity commentEntity = new CommentEntity();
        commentEntity.setText(text);
        commentEntity.setCreator(creatorUser);
        commentEntity.setFeed(feed);
        return commentRepository.save(commentEntity);
    }

    private UserEntity findUser(@NonNull UUID id) {
        Optional<UserEntity> o = userRepository.findUserById(id);
        if (o.isPresent()) {
            return o.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User with this id: " + id + " wan't found in the DB");
        }
    }

    private PostEntity findPost(@NonNull UUID id) {
        Optional<PostEntity> o = postRepository.findPostById(id);
        if (o.isPresent()) {
            return o.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Post with this id: " + id + " wan't found in the DB");
        }
    }
}
