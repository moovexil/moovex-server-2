package com.moovex.moovexserver2.service;


import com.moovex.moovexserver2.entity.address.AddressEntity;
import com.moovex.moovexserver2.entity.address.AddressEntity_;
import com.moovex.moovexserver2.entity.phone_number.PhoneNumberEntity;
import com.moovex.moovexserver2.entity.phone_number.PhoneNumberEntity_;
import com.moovex.moovexserver2.entity.tag.TagEntity;
import com.moovex.moovexserver2.entity.tenant.TenantEntity;
import com.moovex.moovexserver2.entity.user.UserEntity;
import com.moovex.moovexserver2.entity.user.UserEntity_;
import com.moovex.moovexserver2.entity.user.UserType;
import com.moovex.moovexserver2.entity.user.VerificationState;
import com.moovex.moovexserver2.mapper.UserMapper;
import com.moovex.moovexserver2.model.PageDTO;
import com.moovex.moovexserver2.model.filters.AddressFilters;
import com.moovex.moovexserver2.model.filters.UserFiler;
import com.moovex.moovexserver2.model.tag.Tag;
import com.moovex.moovexserver2.model.tenant.Tenant;
import com.moovex.moovexserver2.model.user.User;
import com.moovex.moovexserver2.repository.TagRepository;
import com.moovex.moovexserver2.repository.TenantRepository;
import com.moovex.moovexserver2.repository.UserRepository;
import com.moovex.moovexserver2.util.CopyUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.SetJoin;
import javax.validation.constraints.NotNull;
import java.util.*;

import static com.moovex.moovexserver2.util.CollectionUtil.isNotNullNotEmpty;
import static com.moovex.moovexserver2.util.CriteriaUtil.getSimplePageable;
import static com.moovex.moovexserver2.util.CriteriaUtil.sortByLastModifyDesc;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    UserMapper userMapper;
    @Autowired
    TenantRepository tenantRepository;
    @Autowired
    TagGeneratorService tagGeneratorService;
    @Autowired
    TagRepository tagRepository;

    @NonNull
    @Override
    public User createUser(@NonNull User user) {
        TenantEntity tenant = verifyTenantOrGlobalIfNull(user.getTenant());
        UserEntity entity = userMapper.toEntity(user);
        entity.setTenant(tenant);
        User result =  insertUserIntoDB(entity);
        return setUserTag(result.getId());
    }

    @NonNull
    @Override
    public User createUserByParameters(@NonNull String name, @NonNull String phoneNumber, @NonNull String city, @NonNull String country, @Nullable String specialty) {
        UserEntity userRequest = new UserEntity();
        userRequest.setContactName(name);
        userRequest.setTenant(getGlobalTenant());
        userRequest.setUserType(UserType.USER);
        userRequest.setUserStatus(VerificationState.APPROVED);

        Set<PhoneNumberEntity> phoneNumbers = new HashSet<>();
        PhoneNumberEntity phoneNumberEntity = new PhoneNumberEntity();
        phoneNumberEntity.setPhoneNumber(phoneNumber);
        phoneNumbers.add(phoneNumberEntity);
        userRequest.setPhoneNumbers(phoneNumbers);

        Set<AddressEntity> addresses = new HashSet<>();
        AddressEntity addressEntity = new AddressEntity();
        addressEntity.setCity(city);
        addressEntity.setCountry(country);
        addresses.add(addressEntity);
        userRequest.setAddresses(addresses);

        UserEntity newUser = userRepository.save(userRequest);
        return setUserTag(newUser.getId());
    }

    private User setUserTag(@NonNull UUID userId) {
        UserEntity user = findUser(userId);

        Tag userTag = tagGeneratorService.createUserTag(userId);
        TagEntity tagEntity = findTag(userTag.getTagId());

        user.setTag(tagEntity);
        return insertUserIntoDB(user);
    }

    @NonNull
    @Override
    public User updateUser(@NonNull User user) {
        UserEntity oldEntity = findUser(user.getId());
        UserEntity newEntity = userMapper.toEntity(user);
        UserEntity updatedEntity = userMapper.updateEntity(newEntity, oldEntity);
        UserEntity result = userRepository.save(updatedEntity);

        //if old user doesn't have a tag but new user has it we should update tag also
        //This case happens when we will add a tag to the fake user under the parent tenant
        if (user.getTag() != null) {
            tagGeneratorService.addTagToUser(oldEntity.getId(), user.getTag().getTagId());
        }

        return userMapper.toDomain(result);
    }

    private User insertUserIntoDB(@NonNull UserEntity entity) {
        UserEntity result = userRepository.save(entity);
        return userMapper.toDomain(result);
    }

    @NonNull
    @Override
    public User disableUser(@NonNull UUID id, UUID workerTenantId) {
        UserEntity user = findUser(id);
        user.setUserStatus(VerificationState.DISABLED);

        if (workerTenantId != null) {
            user.setWorkerTenant(findTenant(workerTenantId));
        }

        UserEntity result = userRepository.save(user);
        return userMapper.toDomain(result);
    }

    @NonNull
    @Override
    public User copyAndDisableOldUser(@NonNull UUID oldUserId, @Nullable UserType newUserRole, @NotNull UUID workerTenantId) {
        UserEntity oldUser = findUser(oldUserId);
        UserEntity copy = CopyUtil.duplicateDefaults(oldUser);
        copy.setTenant(findTenant(workerTenantId));
        copy.setTag(oldUser.getTag());
        copy.setUserStatus(VerificationState.APPROVED);

        if (newUserRole != null) {
            copy.setUserType(newUserRole);
        }

        //disable original user and set him worker tenant
        disableUser(oldUserId, workerTenantId);
        //insert the user copy into the DB
        return insertUserIntoDB(copy);
    }

    @Override
    public Set<User> findAllUsers(UUID tenantId) {
        Iterable<UserEntity> entities;
        if (tenantId != null) {
            entities = userRepository.findAll(byTenant(findTenant(tenantId)));
        } else {
            entities = userRepository.findAll();
        }

        Collection<User> result = userMapper.toDomainCollection(entities);

        if (isNotNullNotEmpty(result)) {
            return new HashSet<>(result);
        } else {
            return null;
        }
    }

    @Override
    public User findUserByPhoneNumber(@NonNull String phoneNumber, UUID tenantId) {
        Optional<UserEntity> user;
        if (tenantId != null) {
            user = userRepository.findOne(
                    byPhoneNumber(phoneNumber)
                            .and(byTenant(findTenant(tenantId)))
                            .and(byUserStatusNot(VerificationState.DISABLED))
            );
        } else {
            user = userRepository.findOne(
                    byPhoneNumber(phoneNumber)
                            .and(byTenant(getGlobalTenant()))
                            .and(byUserStatusNot(VerificationState.DISABLED))
            );
        }

        return user.map(userMapper::toDomain).orElse(null);
    }

    @Override
    public User findUserAsPerPhoneNumberAndWorkerTenant(@NonNull String phoneNumber) {
        Optional<UserEntity> entity = userRepository.findOne(
                byPhoneNumber(phoneNumber).and(byTenant(getGlobalTenant()))
        );

        if (entity.isPresent()) {
            UserEntity user = entity.get();
            //if the user disabled and worker tenant is not null it means that this use works in another tenant
            // and we need fetch that instance of a user
            if (user.getUserStatus() == VerificationState.DISABLED && user.getWorkerTenant() != null) {
                Optional<UserEntity> result = userRepository.findOne(
                        byPhoneNumber(phoneNumber)
                                .and(byTenant(user.getWorkerTenant()))
                                .and(byUserStatusNot(VerificationState.DISABLED))
                );

                return result.map(userEntity -> userMapper.toDomain(userEntity)).orElse(null);
            }
            //if user disabled and without worker tenant we need to skip it
            else if (user.getUserStatus() == VerificationState.DISABLED) {
                return null;
            }
            //return user instance from the global tenant
            else {
                return userMapper.toDomain(user);
            }
        } else {
            return null;
        }
    }

    @Override
    public User findUserById(@NonNull UUID id) {
        Optional<UserEntity> o = userRepository.findUserById(id);
        return o.map(userMapper::toDomain).orElse(null);
    }

    @Override
    public boolean isUserPhoneNumberExist(@NonNull String phoneNumber, UUID tenantId) {
        Specification<UserEntity> spec;
        if (tenantId != null) {
            spec = byPhoneNumber(phoneNumber).and(byTenant(findTenant(tenantId)));
        } else {
            spec = byPhoneNumber(phoneNumber).and(byTenant(getGlobalTenant()));
        }

        Optional<UserEntity> entity = userRepository.findOne(spec);
        return entity.isPresent();
    }

    @Override
    public PageDTO<User> findUsersByFilters(
            @NonNull UserFiler filter,
            int page,
            int size
    ) {
        Specification<UserEntity> specification = userFilter(filter);
        Pageable pageable = getSimplePageable(page, size, sortByLastModifyDesc());
        Page<UserEntity> result = userRepository.findAll(specification, pageable);
        if (result.isEmpty()) {
            return null;
        } else {
            return userMapper.toDomainPage(result);
        }
    }

    @Override
    public void updateFcmToken(@NonNull UUID userId, @NonNull String token) {
        UserEntity user = findUser(userId);
        user.setFcmToken(token);
        userRepository.save(user);
    }

    //------------------------ Helper methods -------------------------------------------------

    private TenantEntity verifyTenantOrGlobalIfNull(@Nullable Tenant requestedTenant) {
        if (requestedTenant == null) {
            return getGlobalTenant();
        } else {
            return findTenant(requestedTenant.getId());
        }
    }

    private TenantEntity getGlobalTenant() {
        Optional<TenantEntity> globalTenant = tenantRepository.findTenantByTenantName("global");
        if (globalTenant.isPresent()) {
            return globalTenant.get();
        } else {
            throw new IllegalArgumentException("Can't find global tenant");
        }
    }

    private TenantEntity findTenant(@NonNull UUID id) {
        Optional<TenantEntity> tenant = tenantRepository.findTenantById(id);
        if (tenant.isPresent()) {
            return tenant.get();
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "This tenant does not exist:" + id);
        }
    }

    private UserEntity findUser(@NonNull UUID userId) {
        Optional<UserEntity> user = userRepository.findUserById(userId);
        if (user.isPresent()) {
            return user.get();
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "This user was not found in the DB " + userId);
        }
    }

    private TagEntity findTag(@NonNull UUID id) {
        Optional<TagEntity> o = tagRepository.findTagById(id);
        if (o.isPresent()) {
            return o.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Tag with this id: " + id + " wan't found in the DB");
        }
    }

    //------------------ Specifications --------------------------------------------------

    private Specification<UserEntity> byTenant(@NonNull TenantEntity tenant) {
        return (Specification<UserEntity>) (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(UserEntity_.tenant), tenant);
    }

    private Specification<UserEntity> byPhoneNumber(@NonNull String phoneNumber) {
        return (Specification<UserEntity>) (root, criteriaQuery, criteriaBuilder) -> {

            SetJoin<UserEntity, PhoneNumberEntity> phoneNumbers = root.join(UserEntity_.phoneNumbers);
            return criteriaBuilder.equal(phoneNumbers.get(PhoneNumberEntity_.phoneNumber), phoneNumber);
        };
    }

    private Specification<UserEntity> byUserType(@NonNull UserType userType) {
        return (Specification<UserEntity>) (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(UserEntity_.userType), userType);
    }

    private Specification<UserEntity> byUserStatusNot(@NonNull VerificationState userStatus) {
        return (Specification<UserEntity>) (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.notEqual(root.get(UserEntity_.userStatus), userStatus);
    }

    private Specification<UserEntity> userFilter(@NonNull UserFiler userFiler) {
        return (Specification<UserEntity>) (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (userFiler.getTenant() != null) {
                predicates.add(
                        criteriaBuilder.equal(root.get(UserEntity_.tenant), userFiler.getTenant())
                );
            }

            if (userFiler.getContactName() != null) {
                predicates.add(
                        criteriaBuilder.equal(
                                criteriaBuilder.lower(root.get(UserEntity_.contactName)), userFiler.getContactName().toLowerCase()
                        )
                );
            }

            if (userFiler.getUserType() != null) {
                predicates.add(
                        criteriaBuilder.equal(root.get(UserEntity_.userType), userFiler.getUserType())
                );
            }

            if (userFiler.getUserStatus() != null) {
                predicates.add(
                        criteriaBuilder.equal(root.get(UserEntity_.userStatus), userFiler.getUserStatus())
                );
            }

            AddressFilters addressFilters = userFiler.getAddressFilters();
            if (addressFilters != null) {
                SetJoin<UserEntity, AddressEntity> addresses = root.join(UserEntity_.addresses);

                if (addressFilters.getCountry() != null) {
                    predicates.add(criteriaBuilder.equal(
                            criteriaBuilder.lower(addresses.get(AddressEntity_.country)), addressFilters.getCountry().toLowerCase()
                    ));
                }

                if (addressFilters.getRegion() != null) {
                    predicates.add(criteriaBuilder.equal(
                            criteriaBuilder.lower(addresses.get(AddressEntity_.region)), addressFilters.getRegion().toLowerCase()
                    ));
                }

                if (addressFilters.getCity() != null) {
                    predicates.add(criteriaBuilder.equal(
                            criteriaBuilder.lower(addresses.get(AddressEntity_.city)), addressFilters.getCity().toLowerCase()
                    ));
                }
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }


}
