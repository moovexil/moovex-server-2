package com.moovex.moovexserver2.service;

import com.moovex.moovexserver2.entity.address.AddressEntity;
import com.moovex.moovexserver2.entity.address.LocationStatisticsEntity;
import com.moovex.moovexserver2.entity.company.CompanyEntity;
import com.moovex.moovexserver2.entity.order.*;
import com.moovex.moovexserver2.entity.phone_number.PhoneNumberEntity;
import com.moovex.moovexserver2.entity.tag.TagEntity;
import com.moovex.moovexserver2.entity.tag.TagEntity_;
import com.moovex.moovexserver2.entity.tenant.TenantEntity;
import com.moovex.moovexserver2.entity.tenant.TenantEntity_;
import com.moovex.moovexserver2.entity.user.UserEntity;
import com.moovex.moovexserver2.entity.vehicle.TractorHeadEntity;
import com.moovex.moovexserver2.entity.vehicle.TrailerEntity;
import com.moovex.moovexserver2.entity.vehicle.TruckEntity;
import com.moovex.moovexserver2.mapper.*;
import com.moovex.moovexserver2.model.PageDTO;
import com.moovex.moovexserver2.model.address.LocationStatistics;
import com.moovex.moovexserver2.model.order.Order;
import com.moovex.moovexserver2.repository.*;
import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.SetJoin;
import java.util.*;

import static com.moovex.moovexserver2.util.CollectionUtil.isNotNullNotEmpty;
import static com.moovex.moovexserver2.util.CopyUtil.copyAddresses;
import static com.moovex.moovexserver2.util.CopyUtil.copyPhoneNumber;
import static com.moovex.moovexserver2.util.CriteriaUtil.*;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private AddressMapper addressMapper;
    @Autowired
    private TagMapper tagMapper;
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TruckRepository truckRepository;
    @Autowired
    private TrailerRepository trailerRepository;
    @Autowired
    private TractorRepository tractorRepository;
    @Autowired
    private TenantMapper tenantMapper;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private TenantRepository tenantRepository;
    @Autowired
    private LatLongMapper latLongMapper;
    @Autowired
    private TagRepository tagRepository;
    @Autowired
    private ContactMapper contactMapper;


    @Override
    public Order createOrder(Order order, UUID companyId) {
        OrderEntity entity = createOrderEntity(order);
        OrderEntity savedOrder = orderRepository.save(entity);
        OrderEntity withUpdatedTags = updateTags(savedOrder.getId(), companyId);
        return orderMapper.toDomain(withUpdatedTags);
    }

    @Override
    public Order updateOrder(Order order) {
        OrderEntity oldEntity = findOrder(order.getId());
        OrderEntity newEntity = createOrderEntity(order);
        OrderEntity updatedOldEntity = updateOrder(oldEntity, newEntity);
        OrderEntity updatingResult = orderRepository.save(updatedOldEntity);
        OrderEntity withUpdatedTags = updateTags(updatingResult.getId(),null);
        return orderMapper.toDomain(withUpdatedTags);
    }

    private OrderEntity createOrderEntity(Order domain) {
        OrderEntity result = new OrderEntity();

        result.setPublicity(domain.isPublicity());
        result.setOrderStatus(domain.getOrderStatus());
        result.setShipmentStatus(domain.getShipmentStatus());
        result.setNotes(domain.getNotes());
        result.setPickupDate(domain.getPickupTime());
        result.setDeliveryDate(domain.getDeliveryTime());
        result.setRequestedVehicleType(domain.getRequestedVehicleType());
        result.setGoodsDescription(domain.getGoodsDescription());
        result.setCurrency(domain.getCurrency());
        result.setPrice(domain.getPrice());
        result.setGoodsPicturesUrls(domain.getGoodsPicturesUrls());

        if (isNotNullNotEmpty(domain.getContacts())) {
            result.setContacts(new HashSet<>(contactMapper.toEntityCollection(domain.getContacts())));
        } else {
            result.setContacts(new HashSet<>());
        }

        if (result.getTags() == null) {
            result.setTags(new HashSet<>());
        }

        if (domain.getPickupAddress() != null) {
            result.setPickupAddress(addressMapper.toEntity(domain.getPickupAddress()));
        }

        if (domain.getDestinationAddress() != null) {
            result.setDestinationAddress(addressMapper.toEntity(domain.getDestinationAddress()));
        }

        if (domain.getTenant() != null) {
            TenantEntity tenant = findTenant(domain.getTenant().getId());
            result.setTenant(tenant);
        }

        if (domain.getShipper() != null) {
            CompanyEntity shipper = findCompany(domain.getShipper().getId());
            result.setShipper(shipper);
        }

        if (domain.getCarrier() != null) {
            CompanyEntity carrier = findCompany(domain.getCarrier().getId());
            result.setCarrier(carrier);
        }

        if (domain.getOrderManager() != null) {
            UserEntity orderManager = findUser(domain.getOrderManager().getId());
            result.setOrderManager(orderManager);
        }

        if (domain.getDriver() != null) {
            UserEntity driver = findUser(domain.getDriver().getId());
            result.setDriver(driver);
        }

        if (domain.getTrailer() != null) {
            TrailerEntity trailer = findTrailer(domain.getTrailer().getId());
            result.setTrailer(trailer);
        }

        if (domain.getTruck() != null) {
            TruckEntity truck = findTruck(domain.getTruck().getId());
            result.setTruck(truck);
        }

        if (domain.getTractor() != null) {
            TractorHeadEntity tractorHead = findTractor(domain.getTractor().getId());
            result.setTractor(tractorHead);
        }

        return result;
    }

    //Since some fields not available for updating we skipped those
    private OrderEntity updateOrder(OrderEntity oldOrder, OrderEntity newOrder) {
        oldOrder.setOrderStatus(newOrder.getOrderStatus());
        oldOrder.setShipmentStatus(newOrder.getShipmentStatus());
        oldOrder.setPublicity(newOrder.isPublicity());
        oldOrder.setNotes(newOrder.getNotes());

        if (isNotNullNotEmpty(newOrder.getGoodsPicturesUrls())) {
            oldOrder.setGoodsPicturesUrls(newOrder.getGoodsPicturesUrls());
        }

//        if (isNotNullNotEmpty(newOrder.getContacts())){
//           Set<ContactEntity> oldContacts = oldOrder.getContacts();
//           oldContacts.addAll(newOrder.getContacts());
//           oldOrder.setContacts(oldContacts);
//        }

        if (newOrder.getPickupDate() != null) {
            oldOrder.setPickupDate(newOrder.getPickupDate());
        }

        if (newOrder.getDeliveryDate() != null) {
            oldOrder.setDeliveryDate(newOrder.getDeliveryDate());
        }

        if (newOrder.getShipper() != null) {
            CompanyEntity shipper = findCompany(newOrder.getShipper().getId());
            oldOrder.setShipper(shipper);
        }

        if (newOrder.getCarrier() != null) {
            CompanyEntity carrier = findCompany(newOrder.getCarrier().getId());
            oldOrder.setCarrier(carrier);
        }

        if (newOrder.getOrderManager() != null) {
            UserEntity orderManager = findUser(newOrder.getOrderManager().getId());
            oldOrder.setOrderManager(orderManager);
        }

        if (newOrder.getDriver() != null) {
            UserEntity driver = findUser(newOrder.getDriver().getId());
            oldOrder.setDriver(driver);
        }

        if (newOrder.getTrailer() != null) {
            TrailerEntity trailer = findTrailer(newOrder.getTrailer().getId());
            oldOrder.setTrailer(trailer);
        }

        if (newOrder.getTractor() != null) {
            TractorHeadEntity tractor = findTractor(newOrder.getTractor().getId());
            oldOrder.setTractor(tractor);
        }

        if (newOrder.getTruck() != null) {
            TruckEntity truck = findTruck(newOrder.getTruck().getId());
            oldOrder.setTruck(truck);
        }

        return oldOrder;
    }

    private OrderEntity updateTags(@NonNull UUID orderId,@Nullable UUID creatorCompanyId) {
        OrderEntity order = findOrder(orderId);

        Set<TagEntity> tags = order.getTags();

        //we should add creator company tag to the order entity when we're creating order  because if
        //a shipper or carrier not tenant owner company -> tenant owner company will not be able to see this order
        //on the dashboard because that values calculates via TAG's
        if (creatorCompanyId != null){
            CompanyEntity creatorCompany = findCompany(creatorCompanyId);
            tags.add(creatorCompany.getTag());
        }

        if (order.getDriver() != null) {
            UserEntity driver = findUser(order.getDriver().getId());
            //add driver tag to the order to share order though the tenants
            if (driver.getTag() != null) {
                tags.add(driver.getTag());
            }
        }

        if (order.getCarrier() != null) {
            CompanyEntity carrier = findCompany(order.getCarrier().getId());
            if (carrier.getTag() != null) {
                tags.add(carrier.getTag());
            }
        }

        if (order.getShipper() != null) {
            CompanyEntity shipper = findCompany(order.getShipper().getId());
            if (shipper.getTag() != null) {
                tags.add(shipper.getTag());
            }
        }

        return orderRepository.save(order);
    }

    @Override
    public void leftContactInfo(UUID orderId, UUID contactId) {
        OrderEntity order = findOrder(orderId);
        UserEntity contactUser = findUser(contactId);
        ContactEntity contact = createContact(contactUser);

        Set<ContactEntity> orderContacts = order.getContacts();

        if (orderContacts == null) {
            orderContacts = new HashSet<>();
        }

        orderContacts.add(contact);

        orderRepository.save(order);
    }

    private ContactEntity createContact(UserEntity contactUser) {
        ContactEntity contact = new ContactEntity();
        contact.setContactName(contactUser.getContactName());
        contact.setCompanyName(contactUser.getContactName());
        contact.setEmail(contactUser.getEmail());
        contact.setUserType(contactUser.getUserType());

        Set<AddressEntity> userAddresses = contactUser.getAddresses();
        if (isNotNullNotEmpty(userAddresses)) {
            contact.setAddresses(copyAddresses(userAddresses));
        }

        Set<PhoneNumberEntity> userPhoneNumbers = contactUser.getPhoneNumbers();
        if (isNotNullNotEmpty(userPhoneNumbers)) {
            contact.setPhoneNumbers(copyPhoneNumber(userPhoneNumbers));
        }


        CompanyEntity contactUserCompany = contactUser.getCompany();
        if (contactUserCompany != null) {
            contact.setCompanyName(contactUserCompany.getName());
            contact.setCompanyType(contactUserCompany.getCompanyType());
        }

        return contact;
    }

    @Override
    public PageDTO<Order> findOrdersByOrderStatus(OrderStatus orderStatus, UUID tenantId, UUID companyTagId, int page, int size) {
        Pageable pageable = getSimplePageable(page, size, sortOrdersByPickupDateAsc());
//        Specification<OrderEntity> spec = byOrderStatus(orderStatus)
//                .and(byTenant(tenantId))
//                .and(byTag(companyTagId));

        Specification<OrderEntity> spec = byOrderStatus(orderStatus)
                .and(byTag(companyTagId));

        Page<OrderEntity> result = orderRepository.findAll(spec, pageable);
        return orderMapper.toDomainPage(result);
    }

    @Override
    public PageDTO<Order> findOrdersByShipmentStatus(OrderShipmentStatus shipmentStatus, UUID tenantId, UUID companyTagId, int page, int size) {
        Pageable pageable = getSimplePageable(page, size, sortOrdersByPickupDateAsc());
//        Specification<OrderEntity> spec = byOrderShipmentStatus(shipmentStatus)
//                .and(byTenant(tenantId))
//                .and(byTag(companyTagId));

        Specification<OrderEntity> spec = byOrderShipmentStatus(shipmentStatus)
                .and(byTag(companyTagId));

        Page<OrderEntity> result = orderRepository.findAll(spec, pageable);
        return orderMapper.toDomainPage(result);
    }

    @Override
    public PageDTO<Order> getPublicOrders(int page, int size) {
        Pageable pageable = getSimplePageable(page, size, sortByCreationDesc());
        Page<OrderEntity> result = orderRepository.findAll(byPrivacy(true), pageable);
        return orderMapper.toDomainPage(result);
    }

    @Override
    public void setOrderCargoLocation(UUID orderId, LocationStatistics mapLocation) {
        OrderEntity order = findOrder(orderId);
        Set<LocationStatisticsEntity> location = order.getCargoLocation();
        location.add(latLongMapper.toEntity(mapLocation));
        orderRepository.save(order);
    }

    @Override
    public List<LocationStatistics> getOrderCargoRoute(UUID orderId) {
        OrderEntity order = findOrder(orderId);
        Collection<LocationStatistics> result = latLongMapper.toDomainCollection(order.getCargoLocation());
        if (isNotNullNotEmpty(result)) {
            return new ArrayList<>(result);
        } else {
            return null;
        }
    }

    //--------------------------------- Helpers --------------------------------

    private TenantEntity getGlobalTenant() {
        Optional<TenantEntity> globalTenant = tenantRepository.findTenantByTenantName("global");
        if (globalTenant.isPresent()) {
            return globalTenant.get();
        } else {
            throw new IllegalArgumentException("Can't find global tenant");
        }
    }

    private TenantEntity findTenant(@NonNull UUID id) {
        Optional<com.moovex.moovexserver2.entity.tenant.TenantEntity> tenant = tenantRepository.findTenantById(id);
        if (tenant.isPresent()) {
            return tenant.get();
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "This tenant does not exist:" + id);
        }
    }

    private OrderEntity findOrder(@NonNull UUID id) {
        Optional<OrderEntity> o = orderRepository.findOrderById(id);
        if (o.isPresent()) {
            return o.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Order with this id: " + id + " wan't found in the DB");
        }
    }

    private CompanyEntity findCompany(@NonNull UUID id) {
        Optional<CompanyEntity> o = companyRepository.findCompanyById(id);
        if (o.isPresent()) {
            return o.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Company with this id: " + id + " wan't found in the DB");
        }
    }

    private UserEntity findUser(@NonNull UUID id) {
        Optional<UserEntity> o = userRepository.findUserById(id);
        if (o.isPresent()) {
            return o.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User with this id: " + id + " wan't found in the DB");
        }
    }


    private TrailerEntity findTrailer(@NonNull UUID id) {
        Optional<TrailerEntity> o = trailerRepository.findVehicleById(id);
        if (o.isPresent()) {
            return o.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Vehicle with this id: " + id + " wan't found in the DB");
        }
    }

    private TruckEntity findTruck(@NonNull UUID id) {
        Optional<TruckEntity> o = truckRepository.findVehicleById(id);
        if (o.isPresent()) {
            return o.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Vehicle with this id: " + id + " wan't found in the DB");
        }
    }

    private TractorHeadEntity findTractor(@NonNull UUID id) {
        Optional<TractorHeadEntity> o = tractorRepository.findVehicleById(id);
        if (o.isPresent()) {
            return o.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Vehicle with this id: " + id + " wan't found in the DB");
        }
    }

    //------------------------------- Specifications --------------------------------


    @NotNull
    private Specification<OrderEntity> byTenant(@NonNull UUID tenantId) {
        return (Specification<OrderEntity>) (root, criteriaQuery, criteriaBuilder) -> {
            Join<OrderEntity, TenantEntity> tenant = root.join(OrderEntity_.tenant);
            return criteriaBuilder.equal(tenant.get(TenantEntity_.id), tenantId);
        };

    }

    @NotNull
    private Specification<OrderEntity> byTag(@NonNull UUID tagId) {
        return (Specification<OrderEntity>) (root, criteriaQuery, criteriaBuilder) -> {
            SetJoin<OrderEntity, TagEntity> tags = root.join(OrderEntity_.tags);
            return criteriaBuilder.equal(tags.get(TagEntity_.id), tagId);
        };
    }

    @NotNull
    private Specification<OrderEntity> byOrderStatus(@NonNull OrderStatus orderStatus) {
        return (Specification<OrderEntity>) (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(OrderEntity_.orderStatus), orderStatus);
    }

    @NotNull
    private Specification<OrderEntity> byOrderShipmentStatus(@NonNull OrderShipmentStatus shipmentStatus) {
        return (Specification<OrderEntity>) (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(OrderEntity_.shipmentStatus), shipmentStatus);
    }

    @NonNull
    private Specification<OrderEntity> byPrivacy(boolean isPublic) {
        return (Specification<OrderEntity>) (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(OrderEntity_.publicity), isPublic);
    }


}
