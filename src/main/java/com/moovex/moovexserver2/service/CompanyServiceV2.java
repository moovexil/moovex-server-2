package com.moovex.moovexserver2.service;

import com.moovex.moovexserver2.entity.company.CompanyType;
import com.moovex.moovexserver2.entity.company.ContractStatus;
import com.moovex.moovexserver2.entity.user.VerificationState;
import com.moovex.moovexserver2.model.company.Company;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.UUID;

public interface CompanyServiceV2 {

    Company createPartnerCompany(
            @NonNull UUID companyTenantId,
            @NonNull String name,
            @NonNull CompanyType companyType,
            @NonNull VerificationState verificationState,
            @NonNull ContractStatus contractStatus,
            boolean cbtAvailable,
            boolean gitAvailable,
            @Nullable String aboutCompany,
            @Nullable String specialty
    );

    Company createUserCompany(
            @NonNull UUID companyAdminId,
            @NonNull String name,
            @NonNull CompanyType companyType,
            @NonNull VerificationState verificationState,
            @NonNull ContractStatus contractStatus,
            boolean cbtAvailable,
            boolean gitAvailable,
            @Nullable String aboutCompany,
            @Nullable String specialty
    );

    Company updateCompany(
            @NonNull UUID companyId,
            @Nullable UUID tagId,
            @NonNull String name,
            @NonNull CompanyType companyType,
            @NonNull VerificationState verificationState,
            @NonNull ContractStatus contractStatus,
            boolean cbtAvailable,
            boolean gitAvailable,
            @Nullable String aboutCompany,
            @Nullable String specialty
    );

}
