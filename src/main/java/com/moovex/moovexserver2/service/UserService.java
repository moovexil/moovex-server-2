package com.moovex.moovexserver2.service;

import com.moovex.moovexserver2.entity.user.UserType;
import com.moovex.moovexserver2.model.PageDTO;
import com.moovex.moovexserver2.model.filters.UserFiler;
import com.moovex.moovexserver2.model.tenant.Tenant;
import com.moovex.moovexserver2.model.user.User;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;

public interface UserService {

    @Nullable
    User findUserById(@NonNull UUID id);

    /**
     * Returns new user copy.
     */
    @NonNull
    User copyAndDisableOldUser (@NonNull UUID oldUserId, @Nullable UserType newUserRole, @NotNull UUID workerTenantId);

    @NonNull
    User disableUser(@NonNull UUID id, @Nullable UUID workerTenantId);

    void updateFcmToken(@NonNull UUID userId, @NonNull String token);

    @NonNull
    User createUser(@NonNull User user);

    @NonNull
    User createUserByParameters(@NonNull String name, @NonNull String phoneNumber, @NonNull String city, @NonNull String country, @Nullable String specialty);

    @NonNull
    User updateUser(@NonNull User user);

    @Nullable
    Set<User> findAllUsers(@Nullable UUID tenantId);

    @Nullable
    User findUserByPhoneNumber(@NonNull String phoneNumber, @Nullable UUID tenantId);

    @Nullable
    User findUserAsPerPhoneNumberAndWorkerTenant(@NonNull String phoneNumber);

    @Nullable
    PageDTO<User> findUsersByFilters(
            @NonNull UserFiler filter,
            int page,
            int size
    );

    boolean isUserPhoneNumberExist (@NonNull String phoneNumber, @Nullable UUID tenantId);
}
