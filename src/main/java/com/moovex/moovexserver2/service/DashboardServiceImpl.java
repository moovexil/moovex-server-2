package com.moovex.moovexserver2.service;

import com.moovex.moovexserver2.entity.company.CompanyEntity;
import com.moovex.moovexserver2.entity.company.ContractStatus;
import com.moovex.moovexserver2.entity.order.OrderEntity;
import com.moovex.moovexserver2.entity.order.OrderEntity_;
import com.moovex.moovexserver2.entity.order.OrderShipmentStatus;
import com.moovex.moovexserver2.entity.order.OrderStatus;
import com.moovex.moovexserver2.entity.tag.TagEntity;
import com.moovex.moovexserver2.entity.tag.TagEntity_;
import com.moovex.moovexserver2.entity.tenant.TenantEntity;
import com.moovex.moovexserver2.entity.tenant.TenantEntity_;
import com.moovex.moovexserver2.entity.user.VerificationState;
import com.moovex.moovexserver2.model.dashboard.CompanyDashboard;
import com.moovex.moovexserver2.model.dashboard.order_dashboard.AdminOrderDashboard;
import com.moovex.moovexserver2.repository.CompanyRepository;
import com.moovex.moovexserver2.repository.OrderRepository;
import com.moovex.moovexserver2.repository.TenantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.SetJoin;
import javax.validation.constraints.NotNull;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
public class DashboardServiceImpl implements DashboardService {

    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private TenantRepository tenantRepository;
    @Autowired
    private CompanyRepository companyRepository;


    @Override
    public AdminOrderDashboard generateAdminOrderDashboard(UUID tenantId, UUID companyId) {
        long pending;
        long approved;
        long delayed;
        long completed;
        long rejected;
        long dispute;
        long inTransit;
        long delivered;

        pending = orderRepository.count(getCountSpec(tenantId, companyId, OrderStatus.PENDING, null));
        approved = orderRepository.count(getCountSpec(tenantId, companyId, OrderStatus.APPROVED, null));
        delayed = orderRepository.count(getCountSpec(tenantId, companyId, OrderStatus.DELAYED, null));
        completed = orderRepository.count(getCountSpec(tenantId, companyId, OrderStatus.COMPLETED, null));
        rejected = orderRepository.count(getCountSpec(tenantId, companyId, OrderStatus.REJECTED, null));
        dispute = orderRepository.count(getCountSpec(tenantId, companyId, OrderStatus.DISPUTE, null));
        inTransit = orderRepository.count(getCountSpec(tenantId, companyId, null, OrderShipmentStatus.IN_TRANSIT));
        delivered = orderRepository.count(getCountSpec(tenantId, companyId, null, OrderShipmentStatus.DELIVERED));

        return new AdminOrderDashboard(pending, approved, inTransit, delayed, delivered, completed, rejected, dispute);
    }

    @Override
    public CompanyDashboard generateCompanyDashboard(UUID tenantId, UUID tenantOwnerCompanyId) {
        TenantEntity countTenant = findTenant(tenantId);
        return new CompanyDashboard(
                companyRepository.countCompanyByTenantAndVerificationStateAndIdNot(countTenant, VerificationState.APPROVED,tenantOwnerCompanyId),
                companyRepository.countCompanyByTenantAndVerificationStateAndIdNot(countTenant, VerificationState.PENDING,tenantOwnerCompanyId),
                companyRepository.countCompanyByTenantAndVerificationStateAndIdNot(countTenant, VerificationState.NO_ANSWER,tenantOwnerCompanyId),
                companyRepository.countCompanyByTenantAndVerificationStateAndIdNot(countTenant, VerificationState.REJECTED,tenantOwnerCompanyId),
                companyRepository.countCompanyByTenantAndVerificationStateAndIdNot(countTenant, VerificationState.NO_WHATS_APP,tenantOwnerCompanyId),
                companyRepository.countCompanyByTenantAndContractStatusAndIdNot(countTenant, ContractStatus.NOT_SENT,tenantOwnerCompanyId),
                companyRepository.countCompanyByTenantAndContractStatusAndIdNot(countTenant, ContractStatus.NEGOTIATION,tenantOwnerCompanyId),
                companyRepository.countCompanyByTenantAndContractStatusAndIdNot(countTenant, ContractStatus.SIGNED,tenantOwnerCompanyId)
        );
    }


    //----------------------------------- Helper methods ------------------------------

    private TenantEntity findTenant(@NonNull UUID id) {
        Optional<TenantEntity> tenant = tenantRepository.findTenantById(id);
        if (tenant.isPresent()) {
            return tenant.get();
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "This tenant does not exist:" + id);
        }
    }

    private TagEntity findTagByCompany(@NonNull UUID companyId) {
        Optional<CompanyEntity> o = companyRepository.findCompanyById(companyId);
        if (o.isPresent()) {
            return o.get().getTag();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Company with this id: " + companyId + " wan't found in the DB");
        }
    }

    //--------------------- Specifications ----------------------------------------

    private Specification<OrderEntity> getCountSpec(
            @NotNull UUID tenantId,
            @NotNull UUID companyTagId,
            @Nullable OrderStatus orderStatus,
            @Nullable OrderShipmentStatus shipmentStatus
    ) {
        Specification<OrderEntity> specification;

        if (orderStatus != null) {
            specification = byOrderStatus(orderStatus).and(byTag(companyTagId));
        } else if (shipmentStatus != null) {
            specification = byShipmentStatus(shipmentStatus).and(byTag(companyTagId));
        } else {
            throw new IllegalArgumentException("The order status and shipment status are nulls");
        }

        return specification;
    }

    private Specification<OrderEntity> byTenant(@NonNull UUID tenantId) {
        return (Specification<OrderEntity>) (root, criteriaQuery, criteriaBuilder) -> {
            Join<OrderEntity, TenantEntity> tenantJoin = root.join(OrderEntity_.tenant);
            return criteriaBuilder.equal(tenantJoin.get(TenantEntity_.id), tenantId);
        };
    }

    private Specification<OrderEntity> byTag(@NonNull UUID tagId) {
        return (Specification<OrderEntity>) (root, criteriaQuery, criteriaBuilder) -> {
            SetJoin<OrderEntity, TagEntity> tags = root.join(OrderEntity_.tags);
            return criteriaBuilder.equal(tags.get(TagEntity_.id), tagId);
        };
    }

    private Specification<OrderEntity> byOrderStatus(@NonNull OrderStatus orderStatus) {
        return (Specification<OrderEntity>) (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(OrderEntity_.orderStatus), orderStatus);
    }

    private Specification<OrderEntity> byShipmentStatus(@NonNull OrderShipmentStatus status) {
        return (Specification<OrderEntity>) (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(OrderEntity_.shipmentStatus), status);
    }

}
