package com.moovex.moovexserver2.service;

import com.google.firebase.messaging.*;
import lombok.SneakyThrows;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class MessengerServiceImpl implements MessengerService {

    public enum MessageEvent {
        //tracking events
        EVENT_TRACKING,
        EVENT_UPDATE_TRACKING_DETAILS,
        //geofence events
        EVENT_REGISTER_GEOFENCE,
        EVENT_UNREGISTER_GEOFENCE,
        EVENT_GEOFENCE_TRANSITION
    }

    public static final String PROPERTY_EVENT_TYPE = "event_type";
    public static final String PROPERTY_TRACKING_STATUS = "tracking_status";
    public static final String PROPERTY_TRACKING_OBJECT_ID = "tracking_object_id";
    public static final String PROPERTY_TRACKING_OBJECT_DISPLAY_NAME = "tracking_object_display_name";

    //geofence properties
    public static final String PROPERTY_GEOFENCE_NAME = "geofence_name";
    public static final String PROPERTY_GEOFENCE_DESC = "geofence_description";
    public static final String PROPERTY_GEOFENCE_ID = "geofence_id";
    public static final String PROPERTY_GEOFENCE_LOCATION = "geofence_location_point";
    public static final String PROPERTY_GEOFENCE_TRANSITION = "geofence_transition_type";
    public static final String PROPERTY_GEOFENCE_RADIUS = "geofence_radius";
    public static final String PROPERTY_GEOFENCE_DWELL_DELAY  = "geofence_dwell_delay";



    @SneakyThrows
    @Override
    public String sendMessage(@NonNull String fcmToken, @Nullable Map<String, String> data) {
        AndroidFcmOptions androidFcmOptions = AndroidFcmOptions.builder()
                .setAnalyticsLabel("user_action_event")
                .build();

        AndroidConfig androidConfig = AndroidConfig.builder()
                .setPriority(AndroidConfig.Priority.HIGH)
                .setFcmOptions(androidFcmOptions)
                .build();

        Message message = Message.builder()
                .setToken(fcmToken)
                .putAllData(data)
                .setAndroidConfig(androidConfig)
                .build();

        return FirebaseMessaging.getInstance().send(message);
    }

    @SneakyThrows
    @Override
    public BatchResponse sendMessages(@NonNull List<String> fcmTokens, @Nullable Map<String, String> data) {
        AndroidFcmOptions androidFcmOptions = AndroidFcmOptions.builder()
                .setAnalyticsLabel("manager_action_event")
                .build();

        AndroidConfig androidConfig = AndroidConfig.builder()
                .setPriority(AndroidConfig.Priority.HIGH)
                .setFcmOptions(androidFcmOptions)
                .build();

        MulticastMessage message = MulticastMessage.builder()
                .putAllData(data)
                .addAllTokens(fcmTokens)
                .setAndroidConfig(androidConfig)
                .build();

        return FirebaseMessaging.getInstance().sendMulticast(message);
    }
}
