package com.moovex.moovexserver2.service;

import com.moovex.moovexserver2.entity.company.CompanyEntity;
import com.moovex.moovexserver2.entity.tag.TagEntity;
import com.moovex.moovexserver2.entity.tag.TagType;
import com.moovex.moovexserver2.entity.user.UserEntity;
import com.moovex.moovexserver2.mapper.TagMapper;
import com.moovex.moovexserver2.model.tag.Tag;
import com.moovex.moovexserver2.repository.CompanyRepository;
import com.moovex.moovexserver2.repository.TagRepository;
import com.moovex.moovexserver2.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;


@Service
@Transactional
public class TagGeneratorServiceV2Impl implements TagGeneratorServiceV2 {

    @Autowired
    TagRepository tagRepository;
    @Autowired
    CompanyRepository companyRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    TagMapper tagMapper;


    @Override
    public void createAndAddCompanyTag(@NonNull UUID companyId) {
        CompanyEntity company = findCompany(companyId);
        TagEntity companyTag = createTag(companyId, TagType.COMPANY, company.getName());
        company.setTag(companyTag);
        companyRepository.save(company);
    }


    @Override
    public void createAndAddUserTag(@NonNull UUID userId) {
        UserEntity user = findUser(userId);
        TagEntity userTag = createTag(userId, TagType.USER, user.getContactName());
        user.setTag(userTag);
        userRepository.save(user);
    }

    @NonNull
    @Override
    public Set<Tag> findTagsByName(@NonNull String nameQuery, @NonNull TagType tagType) {
        String query = nameQuery.toLowerCase().replaceAll("\\s", "");
        Collection<TagEntity> result = tagRepository.findTagsByTagNameLikeAndTagType(query, tagType);
        return new HashSet<>(tagMapper.toDomainCollection(result));
    }

    private TagEntity createTag(@NonNull UUID ownerId, @NonNull TagType type, @NonNull String tagName) {
        TagEntity newTag = new TagEntity();
        newTag.setTagName(generateUniqueTagName(tagName));
        newTag.setTagOwnerId(ownerId);
        newTag.setTagType(type);

        return tagRepository.save(newTag);
    }

    private String generateUniqueTagName(@NonNull String requestName) {
        int requestCount = 0;
        StringBuilder tagNameBuilder = new StringBuilder();
        Optional<TagEntity> entity;

        do {
            if (requestCount == 0) {
                tagNameBuilder.append(requestName.toLowerCase().replaceAll("\\s", ""));
            } else {
                tagNameBuilder.append(requestCount);
            }

            requestCount++;
            entity = tagRepository.findTagByTagName(tagNameBuilder.toString());
        } while (entity.isPresent());

        return tagNameBuilder.toString();
    }

    @Override
    public void addTagToCompany(@NonNull UUID companyId, @NonNull UUID tagId) {
        CompanyEntity company = findCompany(companyId);
        TagEntity tag = findTag(tagId);
        company.setTag(tag);
        companyRepository.save(company);
    }

    @Override
    public void addTagToUser(@NonNull UUID userId, @NonNull UUID tagId) {
        UserEntity user = findUser(userId);
        TagEntity tag = findTag(tagId);
        user.setTag(tag);
        userRepository.save(user);
    }

    //------------------------------------------- Helpers ----------------------------------------------

    private TagEntity findTag(@NonNull UUID id) {
        Optional<TagEntity> o = tagRepository.findTagById(id);
        if (o.isPresent()) {
            return o.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Tag with this id: " + id + " wan't found in the DB");
        }
    }

    private CompanyEntity findCompany(@NonNull UUID id) {
        Optional<CompanyEntity> o = companyRepository.findCompanyById(id);
        if (o.isPresent()) {
            return o.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Company with this id: " + id + " wan't found in the DB");
        }
    }

    private UserEntity findUser(@NonNull UUID id) {
        Optional<UserEntity> o = userRepository.findUserById(id);
        if (o.isPresent()) {
            return o.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User with this id: " + id + " wan't found in the DB");
        }
    }


}
