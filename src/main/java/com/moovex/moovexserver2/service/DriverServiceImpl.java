package com.moovex.moovexserver2.service;

import com.moovex.moovexserver2.entity.order.OrderEntity;
import com.moovex.moovexserver2.entity.order.OrderEntity_;
import com.moovex.moovexserver2.entity.order.OrderShipmentStatus;
import com.moovex.moovexserver2.entity.order.OrderStatus;
import com.moovex.moovexserver2.entity.tag.TagEntity;
import com.moovex.moovexserver2.entity.tag.TagEntity_;
import com.moovex.moovexserver2.mapper.OrderMapper;
import com.moovex.moovexserver2.model.order.Order;
import com.moovex.moovexserver2.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.SetJoin;
import java.util.List;
import java.util.UUID;

import static com.moovex.moovexserver2.util.CriteriaUtil.sortOrdersByPickupDateAsc;

@Service
@Transactional
public class DriverServiceImpl implements DriverService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderMapper orderMapper;

    @Override
    public Order currentTask(UUID driverTagId) {
        Specification<OrderEntity> spec = byOrderStatus(OrderStatus.APPROVED)
                .and(byOrderShipmentStatusNot(OrderShipmentStatus.DELIVERED))
                .and(byTag(driverTagId));

        List<OrderEntity> result = orderRepository.findAll(spec, sortOrdersByPickupDateAsc());
        if (result.isEmpty()) {
            return null;
        } else {
            return orderMapper.toDomain(result.get(0));
        }
    }

    private Specification<OrderEntity> byTag(@NonNull UUID tagId) {
        return (Specification<OrderEntity>) (root, criteriaQuery, criteriaBuilder) -> {
            SetJoin<OrderEntity, TagEntity> tags = root.join(OrderEntity_.tags);
            return criteriaBuilder.equal(tags.get(TagEntity_.id), tagId);
        };
    }

    private Specification<OrderEntity> byOrderStatus(@NonNull OrderStatus orderStatus) {
        return (Specification<OrderEntity>) (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(OrderEntity_.orderStatus), orderStatus);
    }

    private Specification<OrderEntity> byOrderShipmentStatusNot(@NonNull OrderShipmentStatus shipmentStatus) {
        return (Specification<OrderEntity>) (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.notEqual(root.get(OrderEntity_.shipmentStatus), shipmentStatus);
    }

}
