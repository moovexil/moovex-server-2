package com.moovex.moovexserver2.service;

import com.moovex.moovexserver2.entity.place.PlaceType;
import com.moovex.moovexserver2.model.place.Place;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

public interface PlaceService {

    @NonNull
    Place createPlace (
            double latitude,
            double longitude,
            @NonNull PlaceType placeType,
            @NonNull String name,
            @Nullable String description
    );
}
