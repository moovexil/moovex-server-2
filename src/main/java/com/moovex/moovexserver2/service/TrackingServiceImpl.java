package com.moovex.moovexserver2.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.messaging.BatchResponse;
import com.google.gson.JsonObject;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.Point;
import com.mapbox.turf.TurfMeasurement;
import com.moovex.moovexserver2.entity.address.LocationStatisticsEntity;
import com.moovex.moovexserver2.entity.events.EventEntity;
import com.moovex.moovexserver2.entity.events.EventType;
import com.moovex.moovexserver2.entity.tenant.TenantEntity;
import com.moovex.moovexserver2.entity.tracking.TrackingEntity;
import com.moovex.moovexserver2.entity.tracking.TrackingEntity_;
import com.moovex.moovexserver2.entity.tracking.TrackingObjectType;
import com.moovex.moovexserver2.entity.tracking.TrackingStatus;
import com.moovex.moovexserver2.entity.user.UserEntity;
import com.moovex.moovexserver2.mapper.LatLongMapper;
import com.moovex.moovexserver2.mapper.MapGeometryMapper;
import com.moovex.moovexserver2.mapper.TrackingMapper;
import com.moovex.moovexserver2.model.address.LocationStatistics;
import com.moovex.moovexserver2.model.traking.Tracking;
import com.moovex.moovexserver2.repository.*;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.constraints.NotNull;
import java.util.*;

import static com.moovex.moovexserver2.service.MessengerServiceImpl.MessageEvent.EVENT_TRACKING;
import static com.moovex.moovexserver2.service.MessengerServiceImpl.MessageEvent.EVENT_UPDATE_TRACKING_DETAILS;
import static com.moovex.moovexserver2.service.MessengerServiceImpl.PROPERTY_TRACKING_OBJECT_ID;
import static com.moovex.moovexserver2.service.MessengerServiceImpl.PROPERTY_TRACKING_STATUS;
import static com.moovex.moovexserver2.util.CollectionUtil.filter;
import static com.moovex.moovexserver2.util.CollectionUtil.isNotNullNotEmpty;

@Service
@Slf4j
@Transactional
public class TrackingServiceImpl implements TrackingService {

    public static final String PROPERTY_TIMESTAMP = "timestamp";
    public static final String PROPERTY_ID = "feature_id";
    public static final String PROPERTY_EVENT_TYPE = "event_type";

    private final ObjectMapper jsonMapper = new ObjectMapper();

    @Autowired
    TrackingMapper trackingMapper;

    @Autowired
    LocationRepository locationRepository;

    @Autowired
    TrackingRepository trackingRepository;

    @Autowired
    LatLongMapper latLongMapper;

    @Autowired
    TenantRepository tenantRepository;

    @Autowired
    MapGeometryMapper mapGeometryMapper;

    @Autowired
    MessengerService messengerService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    EventRepository eventRepository;

    @Override
    public Tracking changeTrackingStatus(@NonNull UUID trackingId) {
        TrackingEntity trackingEntity = findTracking(trackingId);


        //sets STARTING or STOPPING status to the appropriate actions when we want to start or stop service
        TrackingStatus newIntermediateStatus;
        if (trackingEntity.getTrackingStatus() == TrackingStatus.NOT_TRACKING) {
            newIntermediateStatus = TrackingStatus.STARTING;
        } else {
            newIntermediateStatus = TrackingStatus.STOPPING;
        }
        trackingEntity.setTrackingStatus(newIntermediateStatus);
        TrackingEntity updatedEntity = trackingRepository.save(trackingEntity);

        String fcmToken = userFcmToken(updatedEntity.getTrackingObjectId());

        Map<String, String> properties = new HashMap<>();
        properties.put(PROPERTY_EVENT_TYPE, EVENT_TRACKING.toString());

        String result = messengerService.sendMessage(fcmToken, properties);
        log.info("TrackingService -> startLocationTracking, message result->{}", result);

        return trackingMapper.toDomain(updatedEntity);
    }

    @SneakyThrows
    @Override
    public void updateTrackingInformation(@NonNull UUID objectId, @NonNull TrackingStatus newStatus) {
        TrackingEntity tracking = findTrackingByTrackingObjectId(objectId);
        tracking.setTrackingStatus(newStatus);
        trackingRepository.save(tracking);

        List<String> fcms = managementFcmTokens(objectId);

        Map<String, String> properties = new HashMap<>();
        properties.put(PROPERTY_EVENT_TYPE, EVENT_UPDATE_TRACKING_DETAILS.toString());
        properties.put(PROPERTY_TRACKING_STATUS, newStatus.toString());
        properties.put(PROPERTY_TRACKING_OBJECT_ID, objectId.toString());

        BatchResponse result = messengerService.sendMessages(fcms, properties);
        log.info("TrackingService -> updateTrackingInformation, send messages result->{}", jsonMapper.writeValueAsString(result));
    }

    @NonNull
    @Override
    public List<Tracking> getTrackingKeys(@NonNull UUID tenantId, @Nullable TrackingObjectType trackingObjectType, @NonNull UUID userId) {
        TenantEntity tenant = findTenant(tenantId);
        Specification<TrackingEntity> spec;
        if (trackingObjectType != null) {
            spec = findByTenant(tenant).and(excludeUser(userId)).and(findByType(trackingObjectType));
        } else {
            spec = findByTenant(tenant).and(excludeUser(userId));
        }

        List<TrackingEntity> resultPage = trackingRepository.findAll(spec);
        return trackingMapper.toDomainCollection(resultPage);
    }

    @NonNull
    @Override
    public String getTrackingEventDetails(@NonNull UUID trackingId, @NonNull Long from, @NonNull Long to) {
        List<LocationStatisticsEntity> rawLocations = locationRepository.getLocationsDetailsForTrackingKey(trackingId, from, to);
        Set<LocationStatisticsEntity> locations = getRoutePointsBySteps(rawLocations, 0.05);
        List<EventEntity> events = eventRepository.findEventsByEventTimestampBetween(from, to);
        FeatureCollection featureCollection = FeatureCollection.fromFeatures(createTrackingEventFeatures(locations, events));
        return featureCollection.toJson();
    }

    @SneakyThrows
    @NonNull
    @Override
    public String getTrackingDetails(
            @NonNull UUID trackingId,
            @NonNull Long from,
            @NonNull Long to,
            double stepLength
    ) {
        List<LocationStatisticsEntity> locations = locationRepository.getLocationsDetailsForTrackingKey(
                trackingId, from, to);


        //since points was sorted when fetch it from the DB we don't need sort it again
        Set<LocationStatisticsEntity> filteredBySpedRoutePoints = getRoutePointsBySteps(locations, stepLength);
        List<Feature> features = createTrackingDetailsFeatures(filteredBySpedRoutePoints);
        FeatureCollection resultGeometriesCollection = FeatureCollection.fromFeatures(features);

        return resultGeometriesCollection.toJson();
    }

    /**
     * @param sourceLocationStatistics route geometry
     * @param step                     location step in kilometers
     * @return made up list of points fetched as per specified step length through the route
     */
    private Set<LocationStatisticsEntity> getRoutePointsBySteps(
            @NonNull List<LocationStatisticsEntity> sourceLocationStatistics,
            double step
    ) {
        //user set to prevent duplications. Especially for the start end end coordinates of the route
        Set<LocationStatisticsEntity> filteredLocations = new LinkedHashSet<>();
        if (!isNotNullNotEmpty(sourceLocationStatistics)) return filteredLocations;
        //add a route start point to the list
        filteredLocations.add(sourceLocationStatistics.get(0));

        int itemIndex = 0;

        while (itemIndex < sourceLocationStatistics.size()) {
            //fetch location for comparing
            LocationStatisticsEntity startLocation = sourceLocationStatistics.get(itemIndex);
            Point startPoint = mapGeometryMapper.toPoint(startLocation);

            //starting comparing next item with start location
            int nextItemIndex = itemIndex + 1;
            double distance;
            do {
                //prevent IndexOutOfBoundException -> jumps to the outer loop execution
                if (nextItemIndex >= sourceLocationStatistics.size()) {
                    //if the new index out of bound of our source list and we still not found next item
                    //which satisfies distance condition we should return previous item because this is
                    //coordinate will be the start or end point of the route
                    break;
                }
                LocationStatisticsEntity endLocation = sourceLocationStatistics.get(nextItemIndex);
                Point endPoint = mapGeometryMapper.toPoint(endLocation);
                //distance between points
                distance = TurfMeasurement.distance(startPoint, endPoint);
                nextItemIndex++;
            } while (distance <= step);

            //fetch result point
            LocationStatisticsEntity result;
            if (nextItemIndex < sourceLocationStatistics.size()) {
                result = sourceLocationStatistics.get(nextItemIndex);
            } else {
                //adds last item to the list if we reached end of it
                result = sourceLocationStatistics.get(nextItemIndex - 1);
            }


            filteredLocations.add(result);

            //prepare start index for the next iteration
            itemIndex = nextItemIndex;
        }

        return filteredLocations;
    }

    private List<Feature> createTrackingDetailsFeatures(@NonNull Collection<LocationStatisticsEntity> locations) {
        List<Feature> result = new ArrayList<>();
        if (!isNotNullNotEmpty(locations)) return result;

        for (LocationStatisticsEntity l : locations) {
            JsonObject properties = new JsonObject();
            properties.addProperty(PROPERTY_TIMESTAMP, l.getTime());
            properties.addProperty(PROPERTY_ID, l.getId().toString());

            Point p = mapGeometryMapper.toPoint(l);
            String featureId = l.getId().toString();
            Feature f = Feature.fromGeometry(p, properties, featureId);

            result.add(f);
        }

        return result;
    }

    private List<Feature> createTrackingEventFeatures(
            @NonNull Collection<LocationStatisticsEntity> locations,
            @NonNull Collection<EventEntity> events
    ) {

        if (!isNotNullNotEmpty(locations) || !isNotNullNotEmpty(events)) {
            return new ArrayList<>();
        }

        //list with locations which represents driver route with attached events
        List<Feature> features = locationsToFeatures(locations);

        for (EventEntity event : events) {
            int closestFeatureId = findClosedFeatureId(event.getEventTimestamp(), features);
            Feature closestFeature = features.get(closestFeatureId);

            EventType oldFeatureEventType = featureEventType(closestFeature);
            EventType newFeatureEventType = prioritizeEvent(oldFeatureEventType, event.getType());
            closestFeature.addStringProperty(PROPERTY_EVENT_TYPE, newFeatureEventType.name());
            features.set(closestFeatureId,closestFeature);
        }

        List<Feature> result = filter(features, feature -> featureEventType(feature) != null );
        if (result != null){
            return result;
        }else {
            return new ArrayList<>();
        }
    }

    private List<Feature> locationsToFeatures(@NonNull Collection<LocationStatisticsEntity> locations) {
        List<Feature> result = new ArrayList<>();
        for (LocationStatisticsEntity location : locations) {
            JsonObject properties = new JsonObject();
            properties.addProperty(PROPERTY_TIMESTAMP, location.getTime());
            properties.addProperty(PROPERTY_ID, location.getId().toString());

            Point geometry = mapGeometryMapper.toPoint(location);
            Feature feature = Feature.fromGeometry(geometry, properties);
            result.add(feature);
        }
        return result;
    }

    @Nullable
    private EventType featureEventType(@NonNull Feature feature) {
        String featureEventType = feature.getStringProperty(PROPERTY_EVENT_TYPE);
        if (featureEventType != null) {
            return EventType.valueOf(featureEventType);
        } else {
            return null;
        }
    }

    private EventType prioritizeEvent(@Nullable EventType oldType, @NotNull EventType newType) {
        if (oldType == null) {
            return newType;
        } else {
            if (oldType == newType || oldType.getPriority() < newType.getPriority()) {
                return oldType;
            } else {
                return newType;
            }
        }
    }

    private int findClosedFeatureId(long timestamp, @NonNull List<Feature> locations) {
        long min = Long.MAX_VALUE;
        Integer closestFeatureId = null;

        for (int i = 0; i < locations.size(); i++){
            Feature f = locations.get(i);
            long featureTimestamp = (long) f.getNumberProperty(PROPERTY_TIMESTAMP);
            final long diff = Math.abs(featureTimestamp - timestamp);

            if (diff < min) {
                min = diff;
                closestFeatureId = i;
            }
        }

        if (closestFeatureId != null){
            return closestFeatureId;
        }else {
            throw new IllegalArgumentException("Feature collection is empty");
        }
    }

    @Nullable
    private LocationStatisticsEntity findClosedLocationByTimestamp(long timestamp, @NonNull Collection<LocationStatisticsEntity> locations) {
        long min = Long.MAX_VALUE;
        LocationStatisticsEntity closest = null;

        for (LocationStatisticsEntity l : locations) {
            long locationTimestamp = l.getTime();
            final long diff = Math.abs(locationTimestamp - timestamp);

            if (diff < min) {
                min = diff;
                closest = l;
            }
        }

        return closest;
    }

    @Override
    public void saveTrackingInformation(@NonNull UUID userId, @NonNull List<LocationStatistics> locationReport) {
        TrackingEntity tracking = findTrackingByTrackingObjectId(userId);

        List<LocationStatisticsEntity> locations = tracking.getLocations();
        if (locations == null) {
            locations = new ArrayList<>();
        }

        locations.addAll(latLongMapper.toEntityCollection(locationReport));
        trackingRepository.save(tracking);
    }

    private TrackingEntity findTracking(@NonNull UUID id) {
        Optional<TrackingEntity> tracking = trackingRepository.findTrackingById(id);
        if (tracking.isPresent()) {
            return tracking.get();
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "This tracking does not exist:" + id);
        }
    }

    private TrackingEntity findTrackingByTrackingObjectId(@NonNull UUID userId) {
        Optional<TrackingEntity> tracking = trackingRepository.findTrackingByTrackingObjectId(userId);
        if (tracking.isPresent()) {
            return tracking.get();
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Tracking by this object id not found :" + userId);
        }
    }

    private TenantEntity findTenant(@NonNull UUID id) {
        Optional<TenantEntity> tenant = tenantRepository.findTenantById(id);
        if (tenant.isPresent()) {
            return tenant.get();
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "This tenant does not exist:" + id);
        }
    }

    private String userFcmToken(@NonNull UUID userId) {
        Optional<UserEntity> user = userRepository.findUserById(userId);
        if (user.isPresent()) {
            String fcmToken = user.get().getFcmToken();
            if (fcmToken != null) {
                return fcmToken;
            } else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Fcm token doesn't exist for user with id::" + userId);
            }
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User does not exist:" + userId);
        }
    }

    private List<String> managementFcmTokens(@NonNull UUID objectId) {
        String companyId = trackingRepository.findTrackingObjectCompany(objectId);
        return trackingRepository.findManagersFcmTokens(UUID.fromString(companyId));
    }


    private Specification<TrackingEntity> findByTenant(@NonNull TenantEntity tenant) {
        return (Specification<TrackingEntity>) (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(TrackingEntity_.tenant), tenant);
    }

    private Specification<TrackingEntity> findByType(@NonNull TrackingObjectType type) {
        return (Specification<TrackingEntity>) (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(TrackingEntity_.trackingObjectType), type);
    }

    private Specification<TrackingEntity> excludeUser(@NonNull UUID objectId) {
        return (Specification<TrackingEntity>) (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.notEqual(root.get(TrackingEntity_.trackingObjectId), objectId);
    }

}
