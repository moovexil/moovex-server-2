package com.moovex.moovexserver2.service;

import com.google.firebase.messaging.BatchResponse;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.List;
import java.util.Map;

public interface MessengerService {

    String sendMessage(@NonNull String fcmToken, @Nullable Map<String, String> data);

    BatchResponse sendMessages(@NonNull List<String> fcmTokens, @Nullable Map<String, String> data);
}
