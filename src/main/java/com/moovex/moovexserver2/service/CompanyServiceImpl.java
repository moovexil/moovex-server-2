package com.moovex.moovexserver2.service;

import com.moovex.moovexserver2.entity.address.AddressEntity;
import com.moovex.moovexserver2.entity.company.CompanyEntity;
import com.moovex.moovexserver2.entity.company.CompanyEntity_;
import com.moovex.moovexserver2.entity.company.CompanyType;
import com.moovex.moovexserver2.entity.company.ContractStatus;
import com.moovex.moovexserver2.entity.phone_number.PhoneNumberEntity;
import com.moovex.moovexserver2.entity.phone_number.PhoneNumberEntity_;
import com.moovex.moovexserver2.entity.tag.TagEntity;
import com.moovex.moovexserver2.entity.tenant.TenantEntity;
import com.moovex.moovexserver2.entity.user.UserEntity;
import com.moovex.moovexserver2.entity.user.UserType;
import com.moovex.moovexserver2.entity.user.VerificationState;
import com.moovex.moovexserver2.mapper.*;
import com.moovex.moovexserver2.model.company.CompanyFull;
import com.moovex.moovexserver2.model.company.Company;
import com.moovex.moovexserver2.model.tag.Tag;
import com.moovex.moovexserver2.model.user.User;
import com.moovex.moovexserver2.model.user.UserPreview;
import com.moovex.moovexserver2.model.vechile.TractorHead;
import com.moovex.moovexserver2.model.vechile.Trailer;
import com.moovex.moovexserver2.model.vechile.Truck;
import com.moovex.moovexserver2.repository.CompanyRepository;
import com.moovex.moovexserver2.repository.TagRepository;
import com.moovex.moovexserver2.repository.TenantRepository;
import com.moovex.moovexserver2.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.criteria.SetJoin;
import java.util.*;

import static com.moovex.moovexserver2.util.CollectionUtil.*;

@Service
@Transactional
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CompanyMapper companyMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private TenantRepository tenantRepository;
    @Autowired
    private TruckMapper truckMapper;
    @Autowired
    private TrailerMapper trailerMapper;
    @Autowired
    private TractorMapper tractorMapper;
    @Autowired
    private UserPreviewMapper userPreviewMapper;
    @Autowired
    private CompanyPreviewMapper companyPreviewMapper;
    @Autowired
    private TagGeneratorService tagGeneratorService;
    @Autowired
    private TagRepository tagRepository;
    @Autowired
    private UserService userService;

    @Override
    public CompanyFull createCompany(CompanyFull companyFull, boolean isPrivate, UUID tenantId) {
        //Create company with the base data which not required relation mapping
        CompanyEntity entity = companyMapper.toEntity(companyFull);
        TenantEntity companyTenant = resolveCreationTenant(companyFull.getName(), tenantId, isPrivate);
        entity.setTenant(companyTenant);
        CompanyEntity baseCompany = companyRepository.save(entity);

        CompanyEntity result = null;

        //If tenant not null it means that we are creating a fake company under some tenant and we don't need to crate a tag
        if (tenantId == null) {
            result = setCompanyTag(baseCompany.getId());
        } else {
            //This ELSE branch represents FAKE company - if tag not null we should add tag to it
            if (companyFull.getTag() != null) {
                tagGeneratorService.addTagToCompany(baseCompany.getId(), companyFull.getTag().getTagId());
            }
        }

        //Create or add employees for the company
        if (isNotNullNotEmpty(companyFull.getEmployees())) {
            result = updateEmployees(baseCompany.getId(), companyFull.getEmployees());
        }

        //return result
        if (result != null) {
            return companyMapper.toDomain(result);
        }

        return companyMapper.toDomain(baseCompany);
    }

    private CompanyEntity setCompanyTag(@NonNull UUID companyId) {
        CompanyEntity company = findCompany(companyId);

        Tag userTag = tagGeneratorService.createCompanyTag(companyId);
        TagEntity tagEntity = findTag(userTag.getTagId());

        company.setTag(tagEntity);
        return companyRepository.save(company);
    }

    @Override
    public CompanyFull updateCompany(CompanyFull companyFull) {
        CompanyEntity oldEntity = findCompany(companyFull.getId());
        CompanyEntity newEntity = companyMapper.toEntity(companyFull);
        CompanyEntity updatedEntity = companyMapper.updateEntity(newEntity, oldEntity);

        //update company with merged fields
        CompanyEntity result = companyRepository.save(updatedEntity);

        //update employees for the company
        if (isNotNullNotEmpty(companyFull.getEmployees())) {
            result = updateEmployees(
                    oldEntity.getId(),
                    companyFull.getEmployees()
            );
        }

        //if old company doesn't have a tag but new company has it we should update tag also
        //This case happens when we will add a tag to the fake company under the parent tenant
        if (companyFull.getTag() != null) {
            tagGeneratorService.addTagToCompany(oldEntity.getId(), companyFull.getTag().getTagId());
        }

        return companyMapper.toDomain(result);
    }

    @Override
    public Set<Trailer> findCompanyTrailers(UUID companyId) {
        CompanyEntity company = findCompany(companyId);
        if (isNotNullNotEmpty(company.getTrailers())) {
            return new HashSet<>(trailerMapper.toDomainCollection(company.getTrailers()));
        } else {
            return null;
        }
    }

    @Override
    public Set<Truck> findCompanyTrucks(UUID companyId) {
        CompanyEntity company = findCompany(companyId);
        if (isNotNullNotEmpty(company.getTrucks())) {
            return new HashSet<>(truckMapper.toDomainCollection(company.getTrucks()));
        } else {
            return null;
        }
    }

    @Override
    public Set<TractorHead> findCompanyTractors(UUID companyId) {
        CompanyEntity company = findCompany(companyId);
        if (isNotNullNotEmpty(company.getTractorHeads())) {
            return new HashSet<>(tractorMapper.toDomainCollection(company.getTractorHeads()));

        } else {
            return null;
        }
    }

    @Override
    public boolean isCompanyPhoneNumberExist(UUID tenantId, String phoneNumber) {
        Specification<CompanyEntity> spec;
        if (tenantId != null) {
            spec = byPhoneNumber(phoneNumber).and(byTenant(findTenant(tenantId)));
        } else {
            spec = byPhoneNumber(phoneNumber).and(byTenant(getGlobalTenant()));
        }

        Optional<CompanyEntity> entity = companyRepository.findOne(spec);
        return entity.isPresent();
    }

    @Override
    public Set<CompanyFull> findAllCompanies(
            UUID tenantId,
            CompanyType companyType,
            VerificationState verificationState,
            ContractStatus contractStatus
    ) {
        Specification<CompanyEntity> spec = byTenant(findTenant(tenantId));


        if (companyType != null) {
            spec = spec.and(byCompanyType(companyType));
        }

        if (verificationState != null) {
            spec = spec.and(byVerificationState(verificationState));
        }

        if (contractStatus != null) {
            spec = spec.and(byContractState(contractStatus));
        }

        Collection<CompanyEntity> entities = companyRepository.findAll(spec);
        if (isNotNullNotEmpty(entities)) {
            return new HashSet<>(companyMapper.toDomainCollection(entities));
        } else {
            return null;
        }
    }

    @Override
    public Set<Company> findTransportersPreviews(UUID tenantId) {
        TenantEntity tenant = findTenant(tenantId);
        Specification<CompanyEntity> spec = byTenant(tenant).and(isTransporter());
        Collection<CompanyEntity> result = companyRepository.findAll(spec);
        if (isNotNullNotEmpty(result)) {
            return new HashSet<>(companyPreviewMapper.toDomainCollection(result));
        } else {
            return null;
        }
    }

    @Override
    public CompanyFull findCompanyById(UUID id) {
        Optional<CompanyEntity> o = companyRepository.findCompanyById(id);
        return o.map(companyMapper::toDomain)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Co company found for id:"));
    }

    private CompanyEntity updateEmployees(@NonNull UUID companyId, @NonNull Iterable<User> newCollection) {
        CompanyEntity company = findCompany(companyId);

        //some magic happens and the user will appear in the company entity without company.setEmployees
        // maybe because transaction was not closed at the moment
        Set<UserEntity> companyEmployees = company.getEmployees();

        for (User newItem : newCollection) {
            if (newItem != null) {
                //check if item present in old collection
                UserEntity oldItem = findFirstOrNull(companyEmployees, item -> item.getId().equals(newItem.getId()));
                if (oldItem != null) {
                    //The new Item is present in the old collection -> update old item
                    userService.updateUser(newItem);
                } else {

                    UserEntity resultUser;
                    //The new item is not present in the old collection and DB
                    if (newItem.getId() == null) {
                        //create new user with the company tenant code
                        UserEntity newUser = userMapper.toEntity(newItem);
                        newUser.setTenant(company.getTenant());
                        //add tag to user if exist
                        if (newItem.getTag() != null) {
                            TagEntity userTag = findTag(newItem.getTag().getTagId());
                            newUser.setTag(userTag);
                        }
                        resultUser = userRepository.save(newUser);
                    }
                    //The new item is not present in the old collection but exist in the DB
                    else {
                        //update user with company tenant code
                        User userCopy = userService.copyAndDisableOldUser(newItem.getId(), newItem.getUserType(), company.getTenant().getId());
                        resultUser = updateAfterCopy(userCopy.getId(), newItem);
                    }
                    companyEmployees.add(resultUser);
                }
            }
        }

        return companyRepository.save(company);
    }

    /**
     * Temporary solution to update BASIC user fields (exclude ids,tenant,tag,company..) after user was copied to the
     * company employee list from the GT.
     *
     * @return updated user
     */
    private UserEntity updateAfterCopy(@NonNull UUID copiedUserId, User requestedUserData) {
        UserEntity user = findUser(copiedUserId);
        UserEntity request = userMapper.toEntity(requestedUserData);

        user.setContactName(request.getContactName());
        user.setAdminComment(request.getAdminComment());
        user.setEmail(request.getEmail());

        //for addresses and phone numbers we'll fetch new items (without id) and add it to the existing collections
        Set<AddressEntity> userAddresses = user.getAddresses();
        Set<AddressEntity> newAddresses = request.getAddresses();

        for (AddressEntity address : newAddresses) {
            if (address.getId() == null) {
                userAddresses.add(address);
            }
        }

        Set<PhoneNumberEntity> userPhoneNumbers = user.getPhoneNumbers();
        Set<PhoneNumberEntity> newPhoneNumbers = request.getPhoneNumbers();

        for (PhoneNumberEntity phoneNumber : newPhoneNumbers) {
            if (phoneNumber.getId() == null) {
                userPhoneNumbers.add(phoneNumber);
            }
        }

        return userRepository.save(user);
    }

    @Override
    public CompanyFull addEmployeeToCompany(UUID userId, UUID companyId, UserType tenantUserRole) {
        //TODO in future we also need send request to  disable user in global scope while it works in some worker tenant and only
        // when request was accepted by the user need to disable it in global and allow him to use user in another tenant
        CompanyEntity company = findCompany(companyId);
        //disable original user and insert his copy into the db with global tenant
        User userCopy = userService.copyAndDisableOldUser(userId, tenantUserRole, company.getTenant().getId());
        UserEntity userCopyEntity = findUser(userCopy.getId());
        //add user copy into the company employee list, updateCompanyRequest will automatically assign
        //corresponding company tenant to the user copy
        Set<UserEntity> employees = company.getEmployees();

        if (company.getEmployees() == null) {
            employees = new HashSet<>();
        }

        //add user copy to the company employee
        employees.add(userCopyEntity);
        company.setEmployees(employees);

        CompanyEntity updatedResult = companyRepository.save(company);
        return companyMapper.toDomain(updatedResult);
    }

    @Override
    public Set<Company> findAllCompaniesPreview(UUID tenantId, CompanyType companyType) {
        Specification<CompanyEntity> spec;

        if (tenantId != null) {
            TenantEntity tenant = findTenant(tenantId);
            if (companyType != null) {
                spec = byTenant(tenant).and(byCompanyType(companyType));
            } else {
                spec = byTenant(tenant);
            }
        } else {
            if (companyType != null) {
                spec = byCompanyType(companyType);
            } else {
                spec = null;
            }
        }

        Collection<CompanyEntity> entities = companyRepository.findAll(spec);

        if (isNotNullNotEmpty(entities)) {
            return new HashSet<>(companyPreviewMapper.toDomainCollection(entities));
        } else {
            return null;
        }
    }

    @Override
    public Set<UserPreview> findCompanyEmployeePreview(UUID companyId, UserType... userTypes) {
        CompanyEntity company = findCompany(companyId);
        Collection<UserEntity> result;

        if (userTypes == null) {
            result = company.getEmployees();
        } else {
            result = findOrNull(company.getEmployees(), employee -> isAnyTypeMatch(employee, userTypes));
        }

        if (isNotNullNotEmpty(result)) {
            return new HashSet<>(userPreviewMapper.toDomainCollection(result));
        } else {
            return null;
        }
    }

    private boolean isAnyTypeMatch(UserEntity user, UserType... userTypes) {
        for (UserType type : userTypes) {
            if (user.getUserType() == type) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Truck createTruck(UUID companyId, Truck truck) {
        return null;
    }

    @Override
    public Trailer createTrailer(UUID companyId, Trailer trailer) {
        return null;
    }

    @Override
    public TractorHead createTractor(UUID companyId, TractorHead tractor) {
        return null;
    }

    @Override
    public User createCompanyEmployee(UUID companyId, User employee) {
        CompanyEntity company = findCompany(companyId);
        UserEntity request = userMapper.toEntity(employee);
        request.setTenant(company.getTenant());
        request.setCompany(company);
        UserEntity result = userRepository.save(request);
        return userMapper.toDomain(result);
    }


    //------------------------ Helpers ------------------------------------------

    private TenantEntity resolveCreationTenant(@NonNull String companyName, @Nullable UUID tenantId, boolean isPrivate) {
        if (tenantId == null) {
            if (isPrivate) {
                String tenantName = companyName.toLowerCase().replaceAll("\\s", "") + "_" + UUID.randomUUID();
                TenantEntity newTenant = new TenantEntity(null, tenantName);
                return tenantRepository.save(newTenant);
            } else {
                return getGlobalTenant();
            }
        } else {
            return findTenant(tenantId);
        }
    }

    private TenantEntity getGlobalTenant() {
        Optional<TenantEntity> globalTenant = tenantRepository.findTenantByTenantName("global");
        if (globalTenant.isPresent()) {
            return globalTenant.get();
        } else {
            throw new IllegalArgumentException("Can't find global tenant");
        }
    }

    private TenantEntity findTenant(@NonNull UUID id) {
        Optional<TenantEntity> tenant = tenantRepository.findTenantById(id);
        if (tenant.isPresent()) {
            return tenant.get();
        } else {
            throw new IllegalArgumentException("Can't find tenant with id:" + id);
        }
    }

    private CompanyEntity findCompany(@NonNull UUID id) {
        Optional<CompanyEntity> o = companyRepository.findCompanyById(id);
        if (o.isPresent()) {
            return o.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Company with this id: " + id + " wan't found in the DB");
        }
    }

    private UserEntity findUser(@NonNull UUID id) {
        Optional<UserEntity> o = userRepository.findUserById(id);
        if (o.isPresent()) {
            return o.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User with this id: " + id + " wan't found in the DB");
        }
    }

    private TagEntity findTag(@NonNull UUID id) {
        Optional<TagEntity> o = tagRepository.findTagById(id);
        if (o.isPresent()) {
            return o.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Tag with this id: " + id + " wan't found in the DB");
        }
    }

    //------------------ Specifications --------------------------------------------------

    private Specification<CompanyEntity> byTenant(@NonNull TenantEntity tenant) {
        return (Specification<CompanyEntity>) (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(CompanyEntity_.tenant), tenant);
    }

    private Specification<CompanyEntity> byCompanyType(@NonNull CompanyType type) {
        return (Specification<CompanyEntity>) (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(CompanyEntity_.companyType), type);
    }

    private Specification<CompanyEntity> byVerificationState(@NonNull VerificationState state) {
        return (Specification<CompanyEntity>) (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(CompanyEntity_.verificationState), state);
    }

    private Specification<CompanyEntity> byContractState(@NonNull ContractStatus state) {
        return (Specification<CompanyEntity>) (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(CompanyEntity_.contractStatus), state);
    }

    private Specification<CompanyEntity> byPhoneNumber(@NonNull String phoneNumber) {
        return (Specification<CompanyEntity>) (root, criteriaQuery, criteriaBuilder) -> {
            SetJoin<CompanyEntity, PhoneNumberEntity> phoneNumbers = root.join(CompanyEntity_.phoneNumbers);
            return criteriaBuilder.equal(phoneNumbers.get(PhoneNumberEntity_.phoneNumber), phoneNumber);
        };
    }

    private Specification<CompanyEntity> isTransporter() {
        return hasTruck().or(
                hasTrailers().and(hasTractorHead()) // or combi tractor + trailer
        );
    }

    private Specification<CompanyEntity> hasTrailers() {
        return (Specification<CompanyEntity>) (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.isNotEmpty(root.get((CompanyEntity_.trailers)));
    }

    private Specification<CompanyEntity> hasTractorHead() {
        return (Specification<CompanyEntity>) (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.isNotEmpty(root.get((CompanyEntity_.tractorHeads)));
    }

    private Specification<CompanyEntity> hasTruck() {
        return (Specification<CompanyEntity>) (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.isNotEmpty(root.get((CompanyEntity_.trucks)));
    }

}
