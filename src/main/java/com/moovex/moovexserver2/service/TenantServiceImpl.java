package com.moovex.moovexserver2.service;

import com.moovex.moovexserver2.entity.tenant.TenantEntity;
import com.moovex.moovexserver2.mapper.TenantMapper;
import com.moovex.moovexserver2.model.tenant.Tenant;
import com.moovex.moovexserver2.repository.TenantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static com.moovex.moovexserver2.util.CollectionUtil.isNotNullNotEmpty;

@Service
@Transactional
public class TenantServiceImpl implements TenantService {

    @Autowired
    private TenantRepository tenantRepository;
    @Autowired
    private TenantMapper tenantMapper;

    @Override
    public Tenant createTenant(String name) {
        TenantEntity entity = new TenantEntity(null, name.toLowerCase().replaceAll("\\s",""));
        TenantEntity result = tenantRepository.save(entity);
        return tenantMapper.toDomain(result);
    }

    @Override
    public Set<Tenant> findAllTenants() {
        Iterable<TenantEntity> entities = tenantRepository.findAll();
        Collection<Tenant> result = tenantMapper.toDomainCollection(entities);
        if (isNotNullNotEmpty(result)) {
            return new HashSet<>(result);
        } else {
            return null;
        }
    }

    @Override
    public boolean isExist(String tenantName) {
        Optional<TenantEntity> tenant = tenantRepository.findTenantByTenantName(tenantName);
        return tenant.isPresent();
    }

}
