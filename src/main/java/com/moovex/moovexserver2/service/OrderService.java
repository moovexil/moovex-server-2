package com.moovex.moovexserver2.service;

import com.moovex.moovexserver2.entity.order.OrderShipmentStatus;
import com.moovex.moovexserver2.entity.order.OrderStatus;
import com.moovex.moovexserver2.model.PageDTO;
import com.moovex.moovexserver2.model.address.LocationStatistics;
import com.moovex.moovexserver2.model.order.Order;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.List;
import java.util.UUID;

public interface OrderService {

    @NonNull
    Order createOrder(@NonNull Order order,@NonNull UUID companyId);

    @NonNull
    Order updateOrder(@NonNull Order order);

    void leftContactInfo(@NonNull UUID orderId,@NonNull UUID contactId);

    @NonNull
    PageDTO<Order> findOrdersByOrderStatus(
            @NonNull OrderStatus orderStatus,
            @NonNull UUID tenantId,
            @NonNull UUID companyTagId,
            int page,
            int size
    );

    @NonNull
    PageDTO<Order> findOrdersByShipmentStatus(
            @NonNull OrderShipmentStatus shipmentStatus,
            @NonNull UUID tenantId,
            @NonNull UUID companyTagId,
            int page,
            int size
    );

    @NonNull
    PageDTO<Order> getPublicOrders(int page, int size);

    void setOrderCargoLocation(@NonNull UUID orderId, @NonNull LocationStatistics mapLocation);

    @Nullable
    List<LocationStatistics> getOrderCargoRoute(@NonNull UUID orderId);

}
