package com.moovex.moovexserver2.service;

import com.moovex.moovexserver2.entity.place.PlaceEntity;
import com.moovex.moovexserver2.entity.place.PlaceType;
import com.moovex.moovexserver2.model.place.Place;
import com.moovex.moovexserver2.repository.PlaceRepository;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.PrecisionModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;


@Service
@Transactional
public class PlaceServiceImpl implements PlaceService {

    @Autowired
    PlaceRepository placeRepository;

    @NonNull
    @Override
    public Place createPlace(
            double latitude,
            double longitude,
            @NonNull PlaceType placeType,
            @NonNull String name,
            @Nullable String description
    ) {
        PlaceEntity place = new PlaceEntity();
        place.setPlaceType(placeType);
        place.setName(name);
        place.setDescription(description);
        place.setLocation(latLongToPoint(latitude, longitude));

        PlaceEntity result = placeRepository.save(place);
        return placeEntityToPlace(result);
    }

    public static Point latLongToPoint(double latitude, double longitude) {
        Coordinate coordinate = new Coordinate(longitude, latitude);
        GeometryFactory factory = new GeometryFactory(new PrecisionModel(), 4326);
        return factory.createPoint(coordinate);
    }

    public static String pointToGeoJson(Point point) {
        com.mapbox.geojson.Point mapboxPoint = com.mapbox.geojson.Point.fromLngLat(point.getX(), point.getY());
        return mapboxPoint.toJson();
    }

    private static Place placeEntityToPlace(PlaceEntity entity){
        Place place = new Place();
        place.setId(entity.getId());
        place.setPlaceType(entity.getPlaceType());
        place.setName(entity.getName());
        place.setDescription(entity.getDescription());
        place.setLocationGeoJson(pointToGeoJson(entity.getLocation()));
        return place;
    }

}
