package com.moovex.moovexserver2.service;

import com.moovex.moovexserver2.model.order.Order;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.UUID;

public interface DriverService {

    @Nullable
    Order currentTask(@NonNull UUID driverTag);
}
