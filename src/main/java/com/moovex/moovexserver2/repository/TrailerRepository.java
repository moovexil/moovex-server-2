package com.moovex.moovexserver2.repository;

import com.moovex.moovexserver2.entity.vehicle.TrailerEntity;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.UUID;

public interface TrailerRepository extends CrudRepository<TrailerEntity, UUID>, JpaSpecificationExecutor<TrailerEntity> {

    Optional<TrailerEntity> findVehicleById(UUID id);
}
