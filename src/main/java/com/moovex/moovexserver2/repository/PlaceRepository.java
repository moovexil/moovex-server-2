package com.moovex.moovexserver2.repository;

import com.moovex.moovexserver2.entity.place.PlaceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;

import java.util.Optional;
import java.util.UUID;

public interface PlaceRepository extends JpaRepository<PlaceEntity, UUID> {

    Optional<PlaceEntity> findPlaceById(@NonNull UUID id);
}
