package com.moovex.moovexserver2.repository;

import com.moovex.moovexserver2.entity.events.EventEntity;
import com.moovex.moovexserver2.entity.events.EventType;
import com.moovex.moovexserver2.entity.post.CommentEntity;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.lang.NonNull;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface EventRepository extends CrudRepository<EventEntity, UUID> {

    Optional<EventEntity> findEventById(UUID id);

    List<EventEntity> findEventsByEventTimestampBetween(@NonNull Long from, @NonNull Long to);

}
