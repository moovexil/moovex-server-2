package com.moovex.moovexserver2.repository;

import com.moovex.moovexserver2.entity.tenant.TenantEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.NonNull;

import java.util.Optional;
import java.util.UUID;

public interface TenantRepository extends CrudRepository<TenantEntity, UUID> {

    Optional<TenantEntity> findTenantByTenantName(@NonNull String tenantName);

    Optional<TenantEntity> findTenantById (@NonNull UUID id);
}
