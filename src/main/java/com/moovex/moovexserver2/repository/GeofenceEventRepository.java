package com.moovex.moovexserver2.repository;

import com.moovex.moovexserver2.model.geofence.geofence_event.GeofenceEventEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;

import java.util.Optional;
import java.util.UUID;

public interface GeofenceEventRepository extends JpaRepository<GeofenceEventEntity, UUID> {

    Optional<GeofenceEventEntity> findGeofenceEventById(@NonNull UUID id);
}
