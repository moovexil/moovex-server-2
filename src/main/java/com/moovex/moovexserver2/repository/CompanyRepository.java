package com.moovex.moovexserver2.repository;

import com.moovex.moovexserver2.entity.company.CompanyEntity;
import com.moovex.moovexserver2.entity.company.ContractStatus;
import com.moovex.moovexserver2.entity.tenant.TenantEntity;
import com.moovex.moovexserver2.entity.user.VerificationState;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.UUID;

public interface CompanyRepository extends CrudRepository<CompanyEntity, UUID>, JpaSpecificationExecutor<CompanyEntity> {

    Optional<CompanyEntity> findCompanyById (UUID id);

    long countCompanyByTenantAndVerificationStateAndIdNot(TenantEntity tenant, VerificationState verificationState,UUID id);

    long countCompanyByTenantAndContractStatusAndIdNot(TenantEntity tenant, ContractStatus contractStatus, UUID id);
}
