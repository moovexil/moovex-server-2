package com.moovex.moovexserver2.repository;

import com.moovex.moovexserver2.entity.tag.TagEntity;
import com.moovex.moovexserver2.entity.tag.TagType;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

public interface TagRepository extends CrudRepository<TagEntity, UUID>, JpaSpecificationExecutor<TagEntity> {

    Optional<TagEntity> findTagById(UUID id);

    Optional<TagEntity> findTagByTagName(String tagName);

    Collection<TagEntity> findTagsByTagNameLikeAndTagType(String tagName, TagType tagType);
}
