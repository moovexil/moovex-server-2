package com.moovex.moovexserver2.repository;

import com.moovex.moovexserver2.entity.address.LocationStatisticsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.lang.NonNull;

import java.util.List;
import java.util.UUID;

public interface LocationRepository extends JpaRepository<LocationStatisticsEntity, UUID> {

    @Query(
            value = "select w_locations.* from w_locations, w_tracking, w_tracking_locations\n" +
                    "where w_tracking.id = w_tracking_locations.tracking_id\n" +
                    "and w_locations.id = w_tracking_locations.location_point_id\n" +
                    "and w_tracking.id = ?1\n" +
                    "and w_locations.time between ?2 and ?3\n" +
                    "order by w_locations.time asc",
            nativeQuery = true
    )
    List<LocationStatisticsEntity> getLocationsDetailsForTrackingKey(
            @NonNull UUID trackingKeyId,
            @NonNull Long timeFrom,
            @NonNull Long timeTo
    );


}
