package com.moovex.moovexserver2.repository;

import com.moovex.moovexserver2.entity.vehicle.TractorHeadEntity;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.UUID;

public interface TractorRepository extends CrudRepository<TractorHeadEntity, UUID>, JpaSpecificationExecutor<TractorHeadEntity> {

    Optional<TractorHeadEntity> findVehicleById(UUID id);
}
