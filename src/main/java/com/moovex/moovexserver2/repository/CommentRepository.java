package com.moovex.moovexserver2.repository;

import com.moovex.moovexserver2.entity.post.CommentEntity;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;
import java.util.UUID;

public interface CommentRepository extends
        CrudRepository<CommentEntity, UUID>,
        JpaSpecificationExecutor<CommentEntity>,
        PagingAndSortingRepository<CommentEntity,UUID>
{

    Optional<CommentEntity> findUserById(UUID id);

}
