package com.moovex.moovexserver2.repository;

import com.moovex.moovexserver2.entity.post.PostEntity;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;
import java.util.UUID;

public interface PostRepository extends CrudRepository<PostEntity, UUID>, JpaSpecificationExecutor<PostEntity>, PagingAndSortingRepository<PostEntity,UUID> {

    Optional<PostEntity> findPostById(UUID id);

}
