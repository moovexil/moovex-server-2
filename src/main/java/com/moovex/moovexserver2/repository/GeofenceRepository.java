package com.moovex.moovexserver2.repository;

import com.moovex.moovexserver2.model.geofence.GeofenceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;

import java.util.Optional;
import java.util.UUID;

public interface GeofenceRepository extends JpaRepository<GeofenceEntity, UUID> {

    Optional<GeofenceEntity> findGeofenceById(@NonNull UUID id);
}
