package com.moovex.moovexserver2.repository;

import com.moovex.moovexserver2.entity.vehicle.TruckEntity;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.UUID;

public interface TruckRepository extends CrudRepository<TruckEntity, UUID>, JpaSpecificationExecutor<TruckEntity> {

    Optional<TruckEntity> findVehicleById(UUID id);
}
