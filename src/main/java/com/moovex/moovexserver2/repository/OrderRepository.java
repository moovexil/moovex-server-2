package com.moovex.moovexserver2.repository;

import com.moovex.moovexserver2.entity.order.OrderEntity;
import com.moovex.moovexserver2.entity.order.OrderShipmentStatus;
import com.moovex.moovexserver2.entity.order.OrderStatus;
import com.moovex.moovexserver2.entity.tenant.TenantEntity;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.NonNull;

import java.util.Optional;
import java.util.UUID;

public interface OrderRepository extends CrudRepository<OrderEntity, UUID>, JpaSpecificationExecutor<OrderEntity> {

    Optional<OrderEntity> findOrderById(@NonNull UUID id);

    long countOrdersByOrderStatus(@NonNull OrderStatus orderStatus);

    long countOrdersByOrderStatusAndTenant(@NonNull OrderStatus orderStatus, @NonNull TenantEntity tenant);

    long countOrdersByShipmentStatus(@NonNull OrderShipmentStatus shipmentStatus);

    long countOrdersByShipmentStatusAndTenant(@NonNull OrderShipmentStatus shipmentStatus, @NonNull TenantEntity tenant);
}
