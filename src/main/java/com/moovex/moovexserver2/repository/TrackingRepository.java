package com.moovex.moovexserver2.repository;

import com.moovex.moovexserver2.entity.tracking.TrackingEntity;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.NonNull;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TrackingRepository extends
        CrudRepository<TrackingEntity, UUID>,
        JpaSpecificationExecutor<TrackingEntity> {

    Optional<TrackingEntity> findTrackingById(UUID id);

    Optional<TrackingEntity> findTrackingByTrackingObjectId(UUID id);

    @Query(
            value = " select cast(w_companies.id as varchar)\n" +
                    " from w_users, w_company_employees, w_companies\n" +
                    " where w_users.id = w_company_employees.employee_id\n" +
                    " and w_companies.id = w_company_employees.company_id\n" +
                    " and w_users.id = ?1",
            nativeQuery = true
    )
   String findTrackingObjectCompany(@NonNull UUID trackingObjectId);

    @Query(
            value = "select w_users.fcm_token\n" +
                    " from w_users, w_company_employees, w_companies\n" +
                    " where w_users.id = w_company_employees.employee_id\n" +
                    " and w_companies.id = w_company_employees.company_id\n" +
                    " and w_companies.id = ?1\n" +
                    " and w_users.user_type in ('ADMIN', 'EX_ADMIN')",
            nativeQuery = true
    )
    List<String> findManagersFcmTokens(@NonNull UUID companyId);

    @Query(
            value = "select w_users.fcm_token\n" +
                    "from w_users, w_tracking\n" +
                    "where w_tracking.tracking_object_id = w_users.id\n" +
                    "and w_tracking.id = ?1",
            nativeQuery = true
    )
    Optional<String> findUserTypeTrackingObjectFcm(@NonNull UUID trackingId);

    @Query(
            value = "select w_users.contact_name\n" +
                    "from w_users, w_tracking\n" +
                    "where w_tracking.tracking_object_id = w_users.id\n" +
                    "and w_tracking.id = ?1",
            nativeQuery = true
    )
    Optional<String> findUserTypeTrackingObjectName(@NonNull UUID trackingId);

}
