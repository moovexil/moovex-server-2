package com.moovex.moovexserver2.util;

import com.moovex.moovexserver2.entity.address.AddressEntity;
import com.moovex.moovexserver2.entity.phone_number.PhoneNumberEntity;
import com.moovex.moovexserver2.entity.user.UserEntity;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class CopyUtil {

    /**
     * Remove all primary keys from the user object and his nested objects.
     */
    public static UserEntity duplicateDefaults (UserEntity oldUser) {
        UserEntity clone = new UserEntity(oldUser);
        clone.setAddresses(copyAddresses(oldUser.getAddresses()));
        clone.setPhoneNumbers(copyPhoneNumber(oldUser.getPhoneNumbers()));
        return clone;
    }

    public static Set<AddressEntity> copyAddresses (Set<AddressEntity> old){
        return old.stream()
                .map(AddressEntity::new)
                .collect(Collectors.toSet());
    }

    public static Set<PhoneNumberEntity> copyPhoneNumber (Set<PhoneNumberEntity> old){
        return old.stream()
                .filter(Objects::nonNull)
                .map(PhoneNumberEntity::new)
                .collect(Collectors.toSet());
    }
}
