package com.moovex.moovexserver2.util;

import com.sun.istack.NotNull;
import lombok.SneakyThrows;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.*;
import java.util.function.Predicate;

public class CollectionUtil {

    public static <T> boolean isNotNullNotEmpty(Collection<T> collection) {
        return collection != null && !collection.isEmpty();
    }

    @Nullable
    @SneakyThrows
    public static <T> T findFirstOrNull(@NotNull Iterable<T> collection, @NotNull Predicate<T> predicate) {
        if (predicate == null) {
            throw new IllegalAccessException("Predicate not specified");
        }
        for (T item : collection) {
            if (predicate.test(item)) {
                return item;
            }
        }
        return null;
    }

    @SafeVarargs
    @Nullable
    @SneakyThrows
    public static <T> List<T> findOrNull(@NotNull Iterable<T> collection, @NotNull Predicate<T>... predicate) {
        if (predicate == null) {
            throw new IllegalAccessException("Predicate not specified");
        }

        List<T> result = new ArrayList<>();
        for (T item : collection) {
            //check all predicates if at least one matches condition
            for (Predicate<T> tester : predicate) {
                if (tester.test(item)) {
                    result.add(item);
                    break;
                }
            }
        }

        if (isNotNullNotEmpty(result)) {
            return result;
        } else {
            return null;
        }
    }

    @Nullable
    @SneakyThrows
    public static <T> List<T> filter(@NotNull Iterable<T> collection, @NotNull Predicate<T> predicate) {
        if (predicate == null) {
            throw new IllegalAccessException("Predicate not specified");
        }

        List<T> result = new ArrayList<>();
        for (T item : collection) {
            //check all predicates if at least one matches condition
            if (predicate.test(item)) {
                result.add(item);
            }
        }

        if (isNotNullNotEmpty(result)) {
            return result;
        } else {
            return null;
        }
    }

}
