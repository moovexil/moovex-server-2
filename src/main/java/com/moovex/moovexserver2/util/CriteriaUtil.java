package com.moovex.moovexserver2.util;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.lang.NonNull;

public class CriteriaUtil {

    public static Sort sortByLastModifyDesc() {
        return Sort.by(Sort.Direction.DESC, "lastModifiedDate");
    }

    public static Sort sortByCreationAsc() {
        return Sort.by(Sort.Direction.ASC, "creationDate");
    }

    public static Sort sortOrdersByPickupDateAsc() {
        return Sort.by("pickupDate").ascending();
    }

    public static Sort sortByCreationDesc() {
        return Sort.by("creationDate").descending();
    }

    public static Pageable getSimplePageable(int page, int size, @NonNull Sort sort) {
        return PageRequest.of(page, size, sort);
    }

}
