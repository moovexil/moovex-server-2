package com.moovex.moovexserver2.config.filter;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Slf4j
@WebFilter("/*")
@Component
public class RequestInterceptorFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        String userToken = httpServletRequest.getHeader( "user-token" );

        if (userToken == null){
            userToken = "unknown-token";
        }
        //put device Id in MDC
        MDC.put( "user-token", userToken );
        //continue work
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
