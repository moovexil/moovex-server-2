package com.moovex.moovexserver2.config;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.io.InputStream;

@Configuration
public class FirebaseAdminConfig {

    @Bean
    public FirebaseAuth communityAdminAuthentication() throws IOException {
        InputStream token = FirebaseAdminConfig.class.getResourceAsStream("/storage/imoovocommunitydev-firebase-adminsdk-w4j83-5c1ec326c8.json"); //dev
//        InputStream token = FirebaseAdminConfig.class.getResourceAsStream("/storage/imoovo-agent-prod-firebase-adminsdk-vdmlt-dabb052b73.json"); // prod

        FirebaseOptions options = FirebaseOptions.builder()
                .setCredentials(GoogleCredentials.fromStream(token))
                .build();

        FirebaseApp firebaseApp = FirebaseApp.initializeApp(options);
        return FirebaseAuth.getInstance(firebaseApp);
    }

}
