package com.moovex.moovexserver2.config.jpa;

import org.slf4j.MDC;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import java.util.Optional;

import static org.apache.logging.log4j.util.Strings.isNotBlank;

public class AuditorAwareImpl implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        String userName;
        String userToken = MDC.get( "user-token" );
        if (isNotBlank( userToken )){
            userName = "token-"+userToken;
        } else if ( SecurityContextHolder.getContext().getAuthentication() == null ){
            userName = "unknown";
        } else {
            userName = ((User)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        }
        return Optional.of( userName );
    }
}
