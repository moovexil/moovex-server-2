package com.moovex.moovexserver2.config.converters;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.moovex.moovexserver2.model.address.Address;
import lombok.SneakyThrows;
import org.springframework.core.convert.converter.Converter;

import java.util.List;

public class AddressListConverter implements Converter<String, List<Address>> {

    @SneakyThrows
    @Override
    public List<Address> convert(String s) {
        ObjectMapper mapper = new ObjectMapper();

        Address[] asArray = mapper.readValue(s,Address[].class);
        return Lists.newArrayList(asArray);
    }
}
