package com.moovex.moovexserver2.config;

import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;
import java.net.URI;
import java.net.URISyntaxException;

@Slf4j
@Configuration
public class DatabaseConfig {

    @Value("${DATABASE_URL}")
    private String databaseUrlStr;

    @Bean
    @Primary
    @ConfigurationProperties(prefix="spring.datasource")
    public DataSourceProperties dataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    @ConfigurationProperties(prefix="spring.datasource")
    public DataSource dataSource( DataSourceProperties dataSourceProperties ) throws URISyntaxException {

        URI dbUri = new URI(databaseUrlStr);

        String username = dbUri.getUserInfo().split(":")[0];
        String password = dbUri.getUserInfo().split(":")[1];
        String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + ':' + dbUri.getPort() + dbUri.getPath();
        DataSourceBuilder<HikariDataSource> dataSourceBuilder = DataSourceBuilder
                .create()
                .url( dbUrl )
                .username( username )
                .password( password )
                .type( HikariDataSource.class );


        return dataSourceBuilder.build();
    }
}
