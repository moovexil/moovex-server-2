package com.moovex.moovexserver2.mapper;

import com.moovex.moovexserver2.entity.company.CompanyEntity;
import com.moovex.moovexserver2.mapper.base.BaseUnidirectionalMapper;
import com.moovex.moovexserver2.model.company.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import static com.moovex.moovexserver2.util.CollectionUtil.isNotNullNotEmpty;

@Service
public class CompanyPreviewMapper implements
        BaseUnidirectionalMapper<CompanyEntity, Company> {

    @Autowired
    private AddressMapper addressMapper;

    @Autowired
    private PhoneNumberMapper phoneNumberMapper;

    @Autowired
    private TagMapper tagMapper;

    @Autowired
    private TenantMapper tenantMapper;

    @Override
    public Company toDomain(CompanyEntity entity) {
        Company result = new Company();
        result.setId(entity.getId());
        result.setName(entity.getName());
        result.setTenant(tenantMapper.toDomain(entity.getTenant()));

        if (entity.getTag() != null) {
            result.setTag(tagMapper.toDomain(entity.getTag()));
        }

        if (isNotNullNotEmpty(entity.getAddresses())) {
            result.setAddresses(new HashSet<>(addressMapper.toDomainCollection(entity.getAddresses())));
        }

        if (isNotNullNotEmpty(entity.getPhoneNumbers())) {
            result.setPhoneNumbers(new HashSet<>(phoneNumberMapper.toDomainCollection(entity.getPhoneNumbers())));
        }

        return result;
    }

    @NonNull
    public Collection<Company> toDomainCollection(@NonNull Iterable<CompanyEntity> entities) {
        Collection<Company> result = new ArrayList<>();
        for (CompanyEntity item : entities) {
            if (item != null) {
                result.add(toDomain(item));
            }
        }

        return result;
    }
}
