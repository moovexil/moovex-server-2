package com.moovex.moovexserver2.mapper;

import com.moovex.moovexserver2.entity.vehicle.TractorHeadEntity;
import com.moovex.moovexserver2.entity.vehicle.TruckEntity;
import com.moovex.moovexserver2.mapper.base.BaseBidirectionalCollectionMapper;
import com.moovex.moovexserver2.mapper.base.BaseBidirectionalMapper;
import com.moovex.moovexserver2.mapper.base.UpdateCollectionMapper;
import com.moovex.moovexserver2.model.vechile.TractorHead;
import com.moovex.moovexserver2.model.vechile.Truck;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

import static com.moovex.moovexserver2.util.CollectionUtil.findFirstOrNull;

@Service
public class TractorMapper implements
        BaseBidirectionalMapper<TractorHeadEntity, TractorHead>,
        BaseBidirectionalCollectionMapper<TractorHeadEntity, TractorHead>,
        UpdateCollectionMapper<TractorHeadEntity> {

    @Override
    public TractorHeadEntity toEntity(TractorHead domain) {
        TractorHeadEntity entity = new TractorHeadEntity();
        entity.setId(domain.getId());
        entity.setType(domain.getType());
        entity.setRegistrationNumber(domain.getRegistrationNumber());
        entity.setIssuer(domain.getIssuer());
        return entity;
    }

    @Override
    public TractorHead toDomain(TractorHeadEntity entity) {
        return new TractorHead(
                entity.getId(),
                entity.getType(),
                entity.getRegistrationNumber(),
                entity.getIssuer()
        );
    }

    @Override
    public Collection<TractorHead> toDomainCollection(Iterable<TractorHeadEntity> entities) {
        Collection<TractorHead> result = new ArrayList<>();
        for (TractorHeadEntity item : entities) {
            if (item != null) {
                result.add(toDomain(item));
            }
        }

        return result;
    }

    @Override
    public Collection<TractorHeadEntity> toEntityCollection(Iterable<TractorHead> domains) {
        Collection<TractorHeadEntity> result = new ArrayList<>();
        for (TractorHead item : domains) {
            if (item != null) {
                result.add(toEntity(item));
            }
        }

        return result;
    }

    @Override
    public TractorHeadEntity updateEntity(TractorHeadEntity newEntity, TractorHeadEntity oldEntity) {
        if (newEntity.getIssuer() != null) {
            oldEntity.setIssuer(newEntity.getIssuer());
        }

        return oldEntity;
    }

    @Override
    public Collection<TractorHeadEntity> updateEntityCollection(Collection<TractorHeadEntity> newCollection, Collection<TractorHeadEntity> oldCollection) {
        if (newCollection == null && oldCollection == null) {
            return null;
        }

        if (newCollection == null) {
            return oldCollection;
        }

        if (oldCollection == null) {
            return newCollection;
        }

        Collection<TractorHeadEntity> result = new ArrayList<>();

        for (TractorHeadEntity newItem : newCollection) {
            if (newItem != null) {
                //check if item present in old collection
                TractorHeadEntity oldItem = findFirstOrNull(oldCollection, item -> item.getId().equals(newItem.getId()));
                if (oldItem != null) {
                    //The new Item is present in the old collection -> update old item and insert into the result list
                    TractorHeadEntity resultObject = updateEntity(newItem, oldItem);
                    result.add(resultObject);
                } else {
                    //The new item is not present in the old collection -> insert item to the result list
                    result.add(newItem);
                }
            }
        }

        return result;
    }


}
