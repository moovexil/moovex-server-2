package com.moovex.moovexserver2.mapper;

import com.moovex.moovexserver2.entity.address.LocationStatisticsEntity;
import com.moovex.moovexserver2.mapper.base.BaseBidirectionalMapper;
import com.moovex.moovexserver2.mapper.base.BaseUnidirectionalCollectionMapper;
import com.moovex.moovexserver2.model.address.LocationStatistics;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class LatLongMapper implements
        BaseBidirectionalMapper<LocationStatisticsEntity, LocationStatistics> ,
        BaseUnidirectionalCollectionMapper<LocationStatisticsEntity, LocationStatistics>
{

    @Override
    public LocationStatisticsEntity toEntity(LocationStatistics domain) {
        LocationStatisticsEntity result = new LocationStatisticsEntity();
        result.setId(domain.getId());
        result.setLatitude(domain.getLatitude());
        result.setLongitude(domain.getLongitude());
        result.setAccuracy(domain.getAccuracy());
        result.setAltitude(domain.getAltitude());
        result.setBearing(domain.getBearing());
        result.setBearingAccuracyDegrees(domain.getBearingAccuracyDegrees());
        result.setElapsedRealTimeNanos(domain.getElapsedRealTimeNanos());
        result.setElapsedRealTimeUncertaintyNanos(domain.getElapsedRealTimeUncertaintyNanos());
        result.setProvider(domain.getProvider());
        result.setSpeedAccuracyMetersPerSecond(domain.getSpeedAccuracyMetersPerSecond());
        result.setSpeed(domain.getSpeed());
        result.setTime(domain.getTime());
        result.setVerticalAccuracyMeters(domain.getVerticalAccuracyMeters());
        return result;
    }

    @Override
    public LocationStatistics toDomain(LocationStatisticsEntity entity) {
        return new LocationStatistics(
                entity.getId(),
                entity.getLatitude(),
                entity.getLongitude(),
                entity.getAccuracy(),
                entity.getAltitude(),
                entity.getBearing(),
                entity.getBearingAccuracyDegrees(),
                entity.getElapsedRealTimeNanos(),
                entity.getElapsedRealTimeUncertaintyNanos(),
                entity.getProvider(),
                entity.getSpeed(),
                entity.getSpeedAccuracyMetersPerSecond(),
                entity.getTime(),
                entity.getVerticalAccuracyMeters()
        );
    }


    @Override
    public Collection<LocationStatistics> toDomainCollection(Iterable<LocationStatisticsEntity> entities) {
        Collection<LocationStatistics> result = new ArrayList<>();
        for (LocationStatisticsEntity item : entities) {
            if (item != null) {
                result.add(toDomain(item));
            }
        }

        return result;
    }


    public Collection<LocationStatisticsEntity> toEntityCollection(Iterable<LocationStatistics> entities) {
        Collection<LocationStatisticsEntity> result = new ArrayList<>();
        for (LocationStatistics item : entities) {
            if (item != null) {
                result.add(toEntity(item));
            }
        }

        return result;
    }

}
