package com.moovex.moovexserver2.mapper;

import com.moovex.moovexserver2.entity.order.ContactEntity;
import com.moovex.moovexserver2.mapper.base.BaseBidirectionalCollectionMapper;
import com.moovex.moovexserver2.mapper.base.BaseBidirectionalMapper;
import com.moovex.moovexserver2.model.order.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import static com.moovex.moovexserver2.util.CollectionUtil.isNotNullNotEmpty;

@Service
public class ContactMapper implements
        BaseBidirectionalMapper<ContactEntity, Contact>,
        BaseBidirectionalCollectionMapper<ContactEntity, Contact> {

    @Autowired
    private PhoneNumberMapper phoneNumberMapper;
    @Autowired
    private AddressMapper addressMapper;

    @Override
    public ContactEntity toEntity(Contact domain) {
        ContactEntity result = new ContactEntity();

        result.setId(domain.getId());
        result.setContactName(domain.getContactName());
        result.setUserType(domain.getUserType());
        result.setEmail(domain.getEmail());
        result.setAdminComment(domain.getAdminComment());
        result.setCompanyName(domain.getCompanyName());
        result.setCompanyType(domain.getCompanyType());
        result.setHandled(domain.isHandled());

        if (isNotNullNotEmpty(domain.getPhoneNumbers())) {
            result.setPhoneNumbers(new HashSet<>(phoneNumberMapper.toEntityCollection(domain.getPhoneNumbers())));
        }

        if (isNotNullNotEmpty(domain.getAddresses())) {
            result.setAddresses(new HashSet<>(addressMapper.toEntityCollection(domain.getAddresses())));
        }

        return result;
    }

    @Override
    public Contact toDomain(ContactEntity entity) {
        Contact result = new Contact();

        result.setId(entity.getId());
        result.setContactName(entity.getContactName());
        result.setUserType(entity.getUserType());
        result.setEmail(entity.getEmail());
        result.setAdminComment(entity.getAdminComment());
        result.setCompanyName(entity.getCompanyName());
        result.setCompanyType(entity.getCompanyType());
        result.setHandled(entity.isHandled());

        if (isNotNullNotEmpty(entity.getPhoneNumbers())) {
            result.setPhoneNumbers(new HashSet<>(phoneNumberMapper.toDomainCollection(entity.getPhoneNumbers())));
        }

        if (isNotNullNotEmpty(entity.getAddresses())) {
            result.setAddresses(new HashSet<>(addressMapper.toDomainCollection(entity.getAddresses())));
        }

        return result;
    }


    @Override
    public Collection<Contact> toDomainCollection(Iterable<ContactEntity> entities) {
        Collection<Contact> result = new ArrayList<>();
        for (ContactEntity item : entities) {
            if (item != null) {
                result.add(toDomain(item));
            }
        }

        return result;
    }

    @Override
    public Collection<ContactEntity> toEntityCollection(Iterable<Contact> domains) {
        Collection<ContactEntity> result = new ArrayList<>();
        for (Contact item : domains) {
            if (item != null) {
                result.add(toEntity(item));
            }
        }

        return result;
    }

}
