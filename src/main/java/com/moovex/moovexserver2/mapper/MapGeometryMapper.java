package com.moovex.moovexserver2.mapper;

import com.mapbox.geojson.Point;
import com.moovex.moovexserver2.entity.address.LocationStatisticsEntity;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.moovex.moovexserver2.util.CollectionUtil.isNotNullNotEmpty;

@Service
public class MapGeometryMapper {

    @NonNull
    public Point toPoint(@NonNull LocationStatisticsEntity locationStatistics) {
        return Point.fromLngLat(locationStatistics.getLongitude(), locationStatistics.getLatitude());
    }

    @NonNull
    public List<Point> toPoints(@Nullable Collection<LocationStatisticsEntity> locationStatistics) {
        List<Point> result = new ArrayList<>();

        if (isNotNullNotEmpty(locationStatistics)) {
            for (LocationStatisticsEntity location : locationStatistics) {
                result.add(toPoint(location));
            }
        }

        return result;
    }

}
