package com.moovex.moovexserver2.mapper;

import com.moovex.moovexserver2.entity.user.UserEntity;
import com.moovex.moovexserver2.mapper.base.BaseUnidirectionalMapper;
import com.moovex.moovexserver2.model.user.UserPreview;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import static com.moovex.moovexserver2.util.CollectionUtil.isNotNullNotEmpty;

@Service
public class UserPreviewMapper implements BaseUnidirectionalMapper<UserEntity, UserPreview> {

    @Autowired
    private AddressMapper addressMapper;

    @Autowired
    private PhoneNumberMapper phoneNumberMapper;

    @Autowired
    private TagMapper tagMapper;

    @Autowired
    private TenantMapper tenantMapper;

    @Override
    public UserPreview toDomain(UserEntity entity) {
        UserPreview result = new UserPreview();
        result.setId(entity.getId());
        result.setContactName(entity.getContactName());
        result.setTenant(tenantMapper.toDomain(entity.getTenant()));

        if (entity.getTag() != null){
            result.setTag(tagMapper.toDomain(entity.getTag()));
        }

        if (isNotNullNotEmpty(entity.getAddresses())) {
            result.setAddresses(new HashSet<>(addressMapper.toDomainCollection(entity.getAddresses())));
        }

        if (isNotNullNotEmpty(entity.getPhoneNumbers())) {
            result.setPhoneNumbers(new HashSet<>(phoneNumberMapper.toDomainCollection(entity.getPhoneNumbers())));
        }

        return result;
    }


    @NonNull
    public Collection<UserPreview> toDomainCollection(@NonNull Iterable<UserEntity> entities) {
        Collection<UserPreview> result = new ArrayList<>();
        for (UserEntity item : entities) {
            if (item != null) {
                result.add(toDomain(item));
            }
        }

        return result;
    }

}
