package com.moovex.moovexserver2.mapper;

import com.moovex.moovexserver2.entity.tag.TagEntity;
import com.moovex.moovexserver2.mapper.base.BaseBidirectionalMapper;
import com.moovex.moovexserver2.mapper.base.BaseUnidirectionalCollectionMapper;
import com.moovex.moovexserver2.model.tag.Tag;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class TagMapper implements
        BaseBidirectionalMapper<TagEntity, Tag>,
        BaseUnidirectionalCollectionMapper<TagEntity, Tag> {

    @Override
    public Tag toDomain(TagEntity entity) {
        Tag result = new Tag();
        result.setTagId(entity.getId());
        result.setTagName(entity.getTagName());
        result.setTagOwnerId(entity.getTagOwnerId());
        result.setTagType(entity.getTagType());
        return result;
    }

    @Override
    public TagEntity toEntity(Tag domain) {
        TagEntity result = new TagEntity();
        result.setId(domain.getTagId());
        result.setTagName(domain.getTagName());
        result.setTagOwnerId(domain.getTagOwnerId());
        result.setTagType(domain.getTagType());
        return result;
    }

    @Override
    public Collection<Tag> toDomainCollection(Iterable<TagEntity> entities) {
        Collection<Tag> result = new ArrayList<>();
        for (TagEntity item : entities) {
            if (item != null) {
                result.add(toDomain(item));
            }
        }
        return result;
    }
}


