package com.moovex.moovexserver2.mapper;

import com.moovex.moovexserver2.entity.address.LocationStatisticsEntity;
import com.moovex.moovexserver2.entity.tracking.TrackingEntity;
import com.moovex.moovexserver2.model.PageDTO;
import com.moovex.moovexserver2.model.address.LocationStatistics;
import com.moovex.moovexserver2.model.traking.Tracking;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static com.moovex.moovexserver2.util.CollectionUtil.isNotNullNotEmpty;

@Service
public class TrackingMapper {

    @Autowired
    LatLongMapper latLongMapper;


    public Tracking toDomain(@NonNull TrackingEntity entity) {
        return new Tracking(
                entity.getId(),
                entity.getTrackingObjectType(),
                entity.getTrackingStatus(),
                findLastLocation(entity.getLocations()),
                entity.getTrackingObjectId(),
                entity.getTrackingObjectName()
        );
    }

    @Nullable
    private LocationStatistics findLastLocation(@Nullable List<LocationStatisticsEntity> locations) {
        if (isNotNullNotEmpty(locations)) {
            return locations.stream()
                    .max(Comparator.comparing(LocationStatisticsEntity::getTime))
                    .map(latLongMapper::toDomain)
                    .orElse(null);
        }
        return null;
    }


    public List<Tracking> toDomainCollection(Iterable<TrackingEntity> entities) {
        List<Tracking> result = new ArrayList<>();
        for (TrackingEntity item : entities) {
            if (item != null) {
                result.add(toDomain(item));
            }
        }

        return result;
    }


    public PageDTO<Tracking> toDomainPage(Page<TrackingEntity> entitiesPage) {
        PageDTO<Tracking> pageDTO = new PageDTO<>();
        pageDTO.setTotalPages(entitiesPage.getTotalPages());
        pageDTO.setPageNumber(entitiesPage.getNumber());
        pageDTO.setTotalElements(entitiesPage.getTotalElements());

        List<Tracking> domainList = new ArrayList<>(toDomainCollection(entitiesPage.getContent()));
        if (isNotNullNotEmpty(domainList)) {
            pageDTO.setContent(domainList);
        }
        return pageDTO;
    }

}
