package com.moovex.moovexserver2.mapper;

import com.moovex.moovexserver2.entity.vehicle.TrailerEntity;
import com.moovex.moovexserver2.entity.vehicle.TruckEntity;
import com.moovex.moovexserver2.mapper.base.BaseBidirectionalCollectionMapper;
import com.moovex.moovexserver2.mapper.base.BaseBidirectionalMapper;
import com.moovex.moovexserver2.mapper.base.UpdateCollectionMapper;
import com.moovex.moovexserver2.model.vechile.Trailer;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

import static com.moovex.moovexserver2.util.CollectionUtil.findFirstOrNull;

@Service
public class TrailerMapper implements
        BaseBidirectionalMapper<TrailerEntity, Trailer>,
        BaseBidirectionalCollectionMapper<TrailerEntity, Trailer>,
        UpdateCollectionMapper<TrailerEntity> {


    @Override
    public TrailerEntity toEntity(Trailer domain) {
        TrailerEntity entity = new TrailerEntity();
        entity.setId(domain.getId());
        entity.setType(domain.getType());
        entity.setRegistrationNumber(domain.getRegistrationNumber());
        entity.setIssuer(domain.getIssuer());
        entity.setTonnage(domain.getTonnage());
        return entity;
    }

    @Override
    public Trailer toDomain(TrailerEntity entity) {
        return new Trailer(
                entity.getId(),
                entity.getType(),
                entity.getRegistrationNumber(),
                entity.getIssuer(),
                entity.getTonnage()
        );
    }

    @Override
    public Collection<Trailer> toDomainCollection(Iterable<TrailerEntity> entities) {
        Collection<Trailer> result = new ArrayList<>();
        for (TrailerEntity item : entities) {
            if (item != null) {
                result.add(toDomain(item));
            }
        }

        return result;
    }

    @Override
    public Collection<TrailerEntity> toEntityCollection(Iterable<Trailer> domains) {
        Collection<TrailerEntity> result = new ArrayList<>();
        for (Trailer item : domains) {
            if (item != null) {
                result.add(toEntity(item));
            }
        }

        return result;
    }

    @Override
    public TrailerEntity updateEntity(TrailerEntity newEntity, TrailerEntity oldEntity) {
        if (newEntity.getIssuer() != null) {
            oldEntity.setIssuer(newEntity.getIssuer());
        }

        return oldEntity;
    }

    @Override
    public Collection<TrailerEntity> updateEntityCollection(Collection<TrailerEntity> newCollection, Collection<TrailerEntity> oldCollection) {
        if (newCollection == null && oldCollection == null) {
            return null;
        }

        if (newCollection == null) {
            return oldCollection;
        }

        if (oldCollection == null) {
            return newCollection;
        }

        Collection<TrailerEntity> result = new ArrayList<>();

        for (TrailerEntity newItem : newCollection) {
            if (newItem != null) {
                //check if item present in old collection
                TrailerEntity oldItem = findFirstOrNull(oldCollection, item -> item.getId().equals(newItem.getId()));
                if (oldItem != null) {
                    //The new Item is present in the old collection -> update old item and insert into the result list
                    TrailerEntity resultObject = updateEntity(newItem, oldItem);
                    result.add(resultObject);
                } else {
                    //The new item is not present in the old collection -> insert item to the result list
                    result.add(newItem);
                }
            }
        }

        return result;
    }


}
