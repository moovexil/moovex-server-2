package com.moovex.moovexserver2.mapper;

import com.moovex.moovexserver2.entity.address.AddressEntity;
import com.moovex.moovexserver2.mapper.base.BaseBidirectionalCollectionMapper;
import com.moovex.moovexserver2.mapper.base.BaseBidirectionalMapper;
import com.moovex.moovexserver2.mapper.base.UpdateCollectionMapper;
import com.moovex.moovexserver2.model.address.Address;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

import static com.moovex.moovexserver2.util.CollectionUtil.findFirstOrNull;

@Service
public class AddressMapper implements
        BaseBidirectionalMapper<AddressEntity, Address>,
        BaseBidirectionalCollectionMapper<AddressEntity, Address>,
        UpdateCollectionMapper<AddressEntity> {

    @Override
    public AddressEntity toEntity(Address domain) {
        return new AddressEntity(
                domain.getId(),
                domain.getCountry(),
                domain.getRegion(),
                domain.getCity()
        );
    }

    @Override
    public Address toDomain(AddressEntity entity) {
        return new Address(
                entity.getId(),
                entity.getCountry(),
                entity.getRegion(),
                entity.getCity()
        );
    }

    @Override
    public Collection<Address> toDomainCollection(Iterable<AddressEntity> entities) {
        Collection<Address> result = new ArrayList<>();
        for (AddressEntity item : entities) {
            if (item != null) {
                result.add(toDomain(item));
            }
        }

        return result;
    }

    @Override
    public Collection<AddressEntity> toEntityCollection(Iterable<Address> domains) {
        Collection<AddressEntity> result = new ArrayList<>();
        for (Address item : domains) {
            if (item != null) {
                result.add(toEntity(item));
            }
        }

        return result;
    }

    @Override
    public AddressEntity updateEntity(AddressEntity newEntity, AddressEntity oldEntity) {

        if (newEntity.getCountry() != null) {
            oldEntity.setCountry(newEntity.getCountry());
        }

        if (newEntity.getRegion() != null) {
            oldEntity.setRegion(newEntity.getRegion());
        }

        if (newEntity.getCity() != null) {
            oldEntity.setCity(newEntity.getCity());
        }

        return oldEntity;
    }

    @Override
    public Collection<AddressEntity> updateEntityCollection(Collection<AddressEntity> newCollection, Collection<AddressEntity> oldCollection) {
        if (newCollection == null && oldCollection == null) {
            return null;
        }

        if (newCollection == null) {
            return oldCollection;
        }

        if (oldCollection == null) {
            return newCollection;
        }

        Collection<AddressEntity> result = new ArrayList<>();

        for (AddressEntity newItem : newCollection) {
            if (newItem != null) {
                //check if item present in old collection
                AddressEntity oldItem = findFirstOrNull(oldCollection, item -> item.getId().equals(newItem.getId()));
                if (oldItem != null) {
                    //The new Item is present in the old collection -> update old item and insert into the result list
                    AddressEntity resultObject = updateEntity(newItem, oldItem);
                    result.add(resultObject);
                } else {
                    //The new item is not present in the old collection -> insert item to the result list
                    result.add(newItem);
                }
            }
        }

        return result;
    }
}
