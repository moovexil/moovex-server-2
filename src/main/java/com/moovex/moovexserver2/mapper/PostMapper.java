package com.moovex.moovexserver2.mapper;

import com.moovex.moovexserver2.entity.post.PostEntity;
import com.moovex.moovexserver2.entity.tag.TagEntity;
import com.moovex.moovexserver2.mapper.base.BasePageableMapper;
import com.moovex.moovexserver2.mapper.base.BaseUnidirectionalCollectionMapper;
import com.moovex.moovexserver2.mapper.base.BaseUnidirectionalMapper;
import com.moovex.moovexserver2.model.PageDTO;
import com.moovex.moovexserver2.model.post.Post;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.moovex.moovexserver2.util.CollectionUtil.isNotNullNotEmpty;

@Service
public class PostMapper implements
        BaseUnidirectionalMapper<PostEntity, Post>,
        BaseUnidirectionalCollectionMapper<PostEntity, Post>,
        BasePageableMapper<PostEntity, Post> {

    @Override
    public Post toDomain(PostEntity entity) {
        Post domain = new Post();
        domain.setId(entity.getId());
        domain.setImgUrl(entity.getImgUrl());
        domain.setCreatorName(entity.getCreator().getContactName());
        domain.setCreatorId(entity.getCreator().getId());
        domain.setText(entity.getText());
        domain.setCreationDate(entity.getCreationDate());
        domain.setCreatorSpecialty(entity.getCreator().getSpecialty());
        domain.setNumberOfLikes(getNumberOfLikes(entity.getLikeTags()));
        return domain;
    }

    private int getNumberOfLikes(Collection<TagEntity> tags) {
        if (isNotNullNotEmpty(tags)) {
            return tags.size();
        } else {
            return 0;
        }
    }

    @Override
    public PageDTO<Post> toDomainPage(Page<PostEntity> entitiesPage) {
        PageDTO<Post> pageDTO = new PageDTO<>();
        pageDTO.setTotalPages(entitiesPage.getTotalPages());
        pageDTO.setPageNumber(entitiesPage.getNumber());
        pageDTO.setTotalElements(entitiesPage.getTotalElements());

        List<Post> domainList = new ArrayList<>(toDomainCollection(entitiesPage.getContent()));
        if (isNotNullNotEmpty(domainList)) {
            pageDTO.setContent(domainList);
        }
        return pageDTO;
    }

    @Override
    public Collection<Post> toDomainCollection(Iterable<PostEntity> entities) {
        Collection<Post> result = new ArrayList<>();
        for (PostEntity item : entities) {
            if (item != null) {
                result.add(toDomain(item));
            }
        }

        return result;
    }

}
