package com.moovex.moovexserver2.mapper.base;

import org.springframework.lang.NonNull;

public interface BaseBidirectionalMapper<E,D> extends BaseUnidirectionalMapper<E,D> {

    @NonNull
    E toEntity(@NonNull D domain);
}
