package com.moovex.moovexserver2.mapper.base;

import org.springframework.lang.NonNull;

import java.util.Collection;

public interface BaseBidirectionalCollectionMapper<E,D>  extends BaseUnidirectionalCollectionMapper<E,D>{

    @NonNull
    Collection<E> toEntityCollection(@NonNull Iterable<D> domains);
}
