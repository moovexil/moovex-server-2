package com.moovex.moovexserver2.mapper.base;

import org.springframework.lang.NonNull;

public interface BaseUnidirectionalMapper<E, D> {

    @NonNull
    D toDomain(@NonNull E entity);
}
