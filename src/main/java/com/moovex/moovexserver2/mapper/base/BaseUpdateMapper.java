package com.moovex.moovexserver2.mapper.base;

import org.springframework.lang.NonNull;

public interface BaseUpdateMapper<E> {

    @NonNull
    E updateEntity(@NonNull E newEntity, @NonNull E oldEntity);

}
