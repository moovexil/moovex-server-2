package com.moovex.moovexserver2.mapper.base;

import com.moovex.moovexserver2.model.PageDTO;
import org.springframework.data.domain.Page;
import org.springframework.lang.NonNull;

public interface BasePageableMapper<E,D> {

    @NonNull
    PageDTO<D> toDomainPage(@NonNull Page<E> entitiesPage);

}
