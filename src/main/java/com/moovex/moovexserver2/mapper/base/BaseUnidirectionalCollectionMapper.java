package com.moovex.moovexserver2.mapper.base;

import org.springframework.lang.NonNull;

import java.util.Collection;

public interface BaseUnidirectionalCollectionMapper<E, D> {

    @NonNull
    Collection<D> toDomainCollection(@NonNull Iterable<E> entities);
}
