package com.moovex.moovexserver2.mapper.base;

import org.springframework.lang.Nullable;

import java.util.Collection;

public interface UpdateCollectionMapper<E> extends BaseUpdateMapper<E> {

    @Nullable
    Collection<E> updateEntityCollection(@Nullable Collection<E> newCollection, @Nullable Collection<E> oldCollection);
}
