package com.moovex.moovexserver2.mapper;

import com.moovex.moovexserver2.entity.address.AddressEntity;
import com.moovex.moovexserver2.entity.company.CompanyEntity;
import com.moovex.moovexserver2.entity.phone_number.PhoneNumberEntity;
import com.moovex.moovexserver2.entity.vehicle.TractorHeadEntity;
import com.moovex.moovexserver2.entity.vehicle.TrailerEntity;
import com.moovex.moovexserver2.entity.vehicle.TruckEntity;
import com.moovex.moovexserver2.mapper.base.BaseBidirectionalCollectionMapper;
import com.moovex.moovexserver2.mapper.base.BaseBidirectionalMapper;
import com.moovex.moovexserver2.mapper.base.BasePageableMapper;
import com.moovex.moovexserver2.mapper.base.BaseUpdateMapper;
import com.moovex.moovexserver2.model.PageDTO;
import com.moovex.moovexserver2.model.company.Company;
import com.moovex.moovexserver2.model.company.CompanyFull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import static com.moovex.moovexserver2.util.CollectionUtil.isNotNullNotEmpty;

@Service
public class CompanyMapper implements
        BaseBidirectionalMapper<CompanyEntity, CompanyFull>,
        BaseBidirectionalCollectionMapper<CompanyEntity, CompanyFull>,
        BaseUpdateMapper<CompanyEntity>,
        BasePageableMapper<CompanyEntity, CompanyFull> {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private PhoneNumberMapper phoneNumberMapper;
    @Autowired
    private AddressMapper addressMapper;
    @Autowired
    private TrailerMapper trailerMapper;
    @Autowired
    private TruckMapper truckMapper;
    @Autowired
    private TractorMapper tractorMapper;
    @Autowired
    private TagMapper tagMapper;
    @Autowired
    private TenantMapper tenantMapper;

    public Company toCompany(@NonNull CompanyEntity entity) {
        Company company = new Company();
        company.setName(entity.getName());
        company.setCompanyType(entity.getCompanyType());
        company.setVerificationState(entity.getVerificationState());
        company.setContractStatus(entity.getContractStatus());
        company.setCbtAvailable(entity.isCbtAvailable());
        company.setGitAvailable(entity.isGitAvailable());
        company.setSpecialty(entity.getSpecialty());
        company.setAbout(entity.getAbout());
        company.setTenant(tenantMapper.toDomain(entity.getTenant()));
        company.setTag(tagMapper.toDomain(entity.getTag()));

        if (isNotNullNotEmpty(entity.getAddresses())) {
            company.setAddresses(new HashSet<>(addressMapper.toDomainCollection(entity.getAddresses())));
        }

        if (isNotNullNotEmpty(entity.getPhoneNumbers())) {
            company.setPhoneNumbers(new HashSet<>(phoneNumberMapper.toDomainCollection(entity.getPhoneNumbers())));
        }
        return company;
    }

    @Override
    public CompanyEntity toEntity(CompanyFull domain) {
        CompanyEntity result = new CompanyEntity();
        result.setId(domain.getId());
        result.setName(domain.getName());
        result.setCompanyType(domain.getCompanyType());
        result.setVerificationState(domain.getVerificationState());
        result.setContractStatus(domain.getContractStatus());
        result.setAgentPaymentStatus(domain.getAgentPaymentStatus());
        result.setAgentPaymentTransactionId(domain.getAgentPaymentTransactionId());
        result.setGitAvailable(domain.isGitAvailable());
        result.setCbtAvailable(domain.isCbtAvailable());
        result.setAbout(domain.getAbout());
        result.setSpecialty(domain.getSpecialty());
        result.setEmployees(new HashSet<>());

        if (isNotNullNotEmpty(domain.getTrucks())) {
            result.setTrucks(new HashSet<>(truckMapper.toEntityCollection(domain.getTrucks())));
        }

        if (isNotNullNotEmpty(domain.getTrailers())) {
            result.setTrailers(new HashSet<>(trailerMapper.toEntityCollection(domain.getTrailers())));
        }

        if (isNotNullNotEmpty(domain.getTractorHeads())) {
            result.setTractorHeads(new HashSet<>(tractorMapper.toEntityCollection(domain.getTractorHeads())));
        }

        if (isNotNullNotEmpty(domain.getPhoneNumbers())) {
            result.setPhoneNumbers(new HashSet<>(phoneNumberMapper.toEntityCollection(domain.getPhoneNumbers())));
        }

        if (isNotNullNotEmpty(domain.getAddresses())) {
            result.setAddresses(new HashSet<>(addressMapper.toEntityCollection(domain.getAddresses())));
        }

        return result;
    }

    @Override
    public CompanyFull toDomain(CompanyEntity entity) {
        CompanyFull result = new CompanyFull();
        result.setId(entity.getId());
        result.setName(entity.getName());
        result.setVerificationState(entity.getVerificationState());
        result.setCompanyType(entity.getCompanyType());
        result.setContractStatus(entity.getContractStatus());
        result.setAgentPaymentStatus(entity.getAgentPaymentStatus());
        result.setAgentPaymentTransactionId(entity.getAgentPaymentTransactionId());
        result.setGitAvailable(entity.isGitAvailable());
        result.setCbtAvailable(entity.isCbtAvailable());
        result.setAbout(entity.getAbout());
        result.setSpecialty(entity.getSpecialty());

        if (entity.getTenant() != null) {
            result.setTenant(tenantMapper.toDomain(entity.getTenant()));
        }

        if (entity.getTag() != null) {
            result.setTag(tagMapper.toDomain(entity.getTag()));
        }

        if (isNotNullNotEmpty(entity.getTrucks())) {
            result.setTrucks(new HashSet<>(truckMapper.toDomainCollection(entity.getTrucks())));
        }

        if (isNotNullNotEmpty(entity.getTrailers())) {
            result.setTrailers(new HashSet<>(trailerMapper.toDomainCollection(entity.getTrailers())));
        }

        if (isNotNullNotEmpty(entity.getTractorHeads())) {
            result.setTractorHeads(new HashSet<>(tractorMapper.toDomainCollection(entity.getTractorHeads())));
        }

        if (isNotNullNotEmpty(entity.getPhoneNumbers())) {
            result.setPhoneNumbers(new HashSet<>(phoneNumberMapper.toDomainCollection(entity.getPhoneNumbers())));
        }

        if (isNotNullNotEmpty(entity.getAddresses())) {
            result.setAddresses(new HashSet<>(addressMapper.toDomainCollection(entity.getAddresses())));
        }

        if (isNotNullNotEmpty(entity.getEmployees())) {
            result.setEmployees(new HashSet<>(userMapper.toDomainCollection(entity.getEmployees())));
        }

        return result;
    }

    @Override
    public Collection<CompanyFull> toDomainCollection(Iterable<CompanyEntity> entities) {
        Collection<CompanyFull> result = new ArrayList<>();
        for (CompanyEntity item : entities) {
            if (item != null) {
                result.add(toDomain(item));
            }
        }

        return result;
    }

    @Override
    public Collection<CompanyEntity> toEntityCollection(Iterable<CompanyFull> domains) {
        Collection<CompanyEntity> result = new ArrayList<>();
        for (CompanyFull item : domains) {
            if (item != null) {
                result.add(toEntity(item));
            }
        }

        return result;
    }


    @Override
    public CompanyEntity updateEntity(CompanyEntity newEntity, CompanyEntity oldEntity) {

        oldEntity.setGitAvailable(newEntity.isGitAvailable());
        oldEntity.setCbtAvailable(newEntity.isCbtAvailable());

        if (newEntity.getSpecialty() != null) {
            oldEntity.setSpecialty(newEntity.getSpecialty());
        }

        if (newEntity.getAbout() != null) {
            oldEntity.setAbout(newEntity.getAbout());
        }

        if (newEntity.getName() != null) {
            oldEntity.setName(newEntity.getName());
        }

        if (newEntity.getContractStatus() != null) {
            oldEntity.setContractStatus(newEntity.getContractStatus());
        }

        if (newEntity.getAgentPaymentStatus() != null) {
            oldEntity.setAgentPaymentStatus(newEntity.getAgentPaymentStatus());
        }

        if (newEntity.getAgentPaymentTransactionId() != null) {
            oldEntity.setAgentPaymentTransactionId(newEntity.getAgentPaymentTransactionId());
        }

        Collection<PhoneNumberEntity> updatedPhoneNumberCollection = phoneNumberMapper.updateEntityCollection(
                newEntity.getPhoneNumbers(), oldEntity.getPhoneNumbers()
        );

        if (isNotNullNotEmpty(updatedPhoneNumberCollection)) {
            oldEntity.setPhoneNumbers(new HashSet<>(updatedPhoneNumberCollection));
        }

        Collection<AddressEntity> updatedAddressesCollection = addressMapper.updateEntityCollection(
                newEntity.getAddresses(), oldEntity.getAddresses()
        );

        if (isNotNullNotEmpty(updatedAddressesCollection)) {
            oldEntity.setAddresses(new HashSet<>(updatedAddressesCollection));
        }

        Collection<TrailerEntity> updatedVehiclesCollection = trailerMapper.updateEntityCollection(
                newEntity.getTrailers(), oldEntity.getTrailers()
        );

        if (isNotNullNotEmpty(updatedVehiclesCollection)) {
            oldEntity.setTrailers(new HashSet<>(updatedVehiclesCollection));
        }

        Collection<TractorHeadEntity> updatedTractorsCollection = tractorMapper.updateEntityCollection(
                newEntity.getTractorHeads(), oldEntity.getTractorHeads()
        );

        if (isNotNullNotEmpty(updatedTractorsCollection)) {
            oldEntity.setTractorHeads(new HashSet<>(updatedTractorsCollection));
        }

        Collection<TruckEntity> updatedTruckCollection = truckMapper.updateEntityCollection(
                newEntity.getTrucks(), oldEntity.getTrucks()
        );

        if (isNotNullNotEmpty(updatedTruckCollection)) {
            oldEntity.setTrucks(new HashSet<>(updatedTruckCollection));
        }

        return oldEntity;
    }

    @Override
    public PageDTO<CompanyFull> toDomainPage(Page<CompanyEntity> entitiesPage) {
        PageDTO<CompanyFull> pageDTO = new PageDTO<>();
        pageDTO.setTotalPages(entitiesPage.getTotalPages());
        pageDTO.setPageNumber(entitiesPage.getNumber());
        pageDTO.setTotalElements(entitiesPage.getTotalElements());

        List<CompanyFull> domainList = new ArrayList<>(toDomainCollection(entitiesPage.getContent()));
        if (isNotNullNotEmpty(domainList)) {
            pageDTO.setContent(domainList);
        }
        return pageDTO;
    }
}
