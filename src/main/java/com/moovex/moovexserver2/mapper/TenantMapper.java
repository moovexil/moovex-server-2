package com.moovex.moovexserver2.mapper;

import com.moovex.moovexserver2.entity.tenant.TenantEntity;
import com.moovex.moovexserver2.mapper.base.BaseBidirectionalCollectionMapper;
import com.moovex.moovexserver2.mapper.base.BaseBidirectionalMapper;
import com.moovex.moovexserver2.model.tenant.Tenant;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class TenantMapper implements
        BaseBidirectionalMapper<TenantEntity, Tenant>,
        BaseBidirectionalCollectionMapper<TenantEntity, Tenant> {

    @Override
    public TenantEntity toEntity(Tenant domain) {
        return new TenantEntity(domain.getId(),domain.getTenantName());
    }

    @Override
    public Tenant toDomain(TenantEntity entity) {
        return new Tenant(entity.getId(),entity.getTenantName());
    }

    @Override
    public Collection<TenantEntity> toEntityCollection(Iterable<Tenant> domains) {
        Collection<TenantEntity> result = new ArrayList<>();
        for (Tenant item : domains) {
            if (item != null) {
                result.add(toEntity(item));
            }
        }

        return result;
    }

    @Override
    public Collection<Tenant> toDomainCollection(Iterable<TenantEntity> entities) {
        Collection<Tenant> result = new ArrayList<>();
        for (TenantEntity item : entities) {
            if (item != null) {
                result.add(toDomain(item));
            }
        }

        return result;
    }
}
