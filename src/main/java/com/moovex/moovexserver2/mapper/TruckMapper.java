package com.moovex.moovexserver2.mapper;

import com.moovex.moovexserver2.entity.vehicle.TruckEntity;
import com.moovex.moovexserver2.mapper.base.BaseBidirectionalCollectionMapper;
import com.moovex.moovexserver2.mapper.base.BaseBidirectionalMapper;
import com.moovex.moovexserver2.mapper.base.UpdateCollectionMapper;
import com.moovex.moovexserver2.model.vechile.Truck;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

import static com.moovex.moovexserver2.util.CollectionUtil.findFirstOrNull;

@Service
public class TruckMapper implements
        BaseBidirectionalMapper<TruckEntity, Truck>,
        BaseBidirectionalCollectionMapper<TruckEntity, Truck>,
        UpdateCollectionMapper<TruckEntity> {

    @Override
    public TruckEntity toEntity(Truck domain) {
        TruckEntity entity = new TruckEntity();
        entity.setId(domain.getId());
        entity.setType(domain.getType());
        entity.setRegistrationNumber(domain.getRegistrationNumber());
        entity.setIssuer(domain.getIssuer());
        entity.setTonnage(domain.getTonnage());
        return entity;
    }

    @Override
    public Truck toDomain(TruckEntity entity) {
        return new Truck(
                entity.getId(),
                entity.getType(),
                entity.getRegistrationNumber(),
                entity.getIssuer(),
                entity.getTonnage()
        );
    }

    @Override
    public Collection<Truck> toDomainCollection(Iterable<TruckEntity> entities) {
        Collection<Truck> result = new ArrayList<>();
        for (TruckEntity item : entities) {
            if (item != null) {
                result.add(toDomain(item));
            }
        }

        return result;
    }

    @Override
    public Collection<TruckEntity> toEntityCollection(Iterable<Truck> domains) {
        Collection<TruckEntity> result = new ArrayList<>();
        for (Truck item : domains) {
            if (item != null) {
                result.add(toEntity(item));
            }
        }

        return result;
    }

    @Override
    public TruckEntity updateEntity(TruckEntity newEntity, TruckEntity oldEntity) {
        if (newEntity.getIssuer() != null) {
            oldEntity.setIssuer(newEntity.getIssuer());
        }

        return oldEntity;
    }

    @Override
    public Collection<TruckEntity> updateEntityCollection(Collection<TruckEntity> newCollection, Collection<TruckEntity> oldCollection) {
        if (newCollection == null && oldCollection == null) {
            return null;
        }

        if (newCollection == null) {
            return oldCollection;
        }

        if (oldCollection == null) {
            return newCollection;
        }

        Collection<TruckEntity> result = new ArrayList<>();

        for (TruckEntity newItem : newCollection) {
            if (newItem != null) {
                //check if item present in old collection
                TruckEntity oldItem = findFirstOrNull(oldCollection, item -> item.getId().equals(newItem.getId()));
                if (oldItem != null) {
                    //The new Item is present in the old collection -> update old item and insert into the result list
                    TruckEntity resultObject = updateEntity(newItem, oldItem);
                    result.add(resultObject);
                } else {
                    //The new item is not present in the old collection -> insert item to the result list
                    result.add(newItem);
                }
            }
        }

        return result;
    }


}
