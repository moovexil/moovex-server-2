package com.moovex.moovexserver2.mapper;

import com.moovex.moovexserver2.entity.address.AddressEntity;
import com.moovex.moovexserver2.entity.phone_number.PhoneNumberEntity;
import com.moovex.moovexserver2.entity.user.*;
import com.moovex.moovexserver2.mapper.base.*;
import com.moovex.moovexserver2.model.PageDTO;
import com.moovex.moovexserver2.model.user.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import static com.moovex.moovexserver2.util.CollectionUtil.isNotNullNotEmpty;
import static org.springframework.data.util.CastUtils.cast;

@Service
public class UserMapper implements
        BaseBidirectionalMapper<UserEntity, User>,
        BaseBidirectionalCollectionMapper<UserEntity, User>,
        BaseUpdateMapper<UserEntity>,
        BasePageableMapper<UserEntity,User> {

    @Autowired private PhoneNumberMapper phoneNumberMapper;
    @Autowired private AddressMapper addressMapper;
    @Autowired private TenantMapper tenantMapper;
    @Autowired private CompanyPreviewMapper companyPreviewMapper;
    @Autowired private TagMapper tagMapper;

    @Override
    public UserEntity toEntity(User domain) {
        UserEntity result = new UserEntity();
        result.setId(domain.getId());
        result.setContactName(domain.getContactName());
        result.setUserStatus(domain.getVerificationState());
        result.setUserType(domain.getUserType());
        result.setEmail(domain.getEmail());
        result.setAdminComment(domain.getAdminComment());
        result.setSpecialty(domain.getSpecialty());

        if (domain.getTenant() != null){
            result.setTenant(tenantMapper.toEntity(domain.getTenant()));
        }

        if (domain.getWorkerTenant() != null){
            result.setWorkerTenant(tenantMapper.toEntity(domain.getWorkerTenant()));
        }

        if (isNotNullNotEmpty(domain.getPhoneNumbers())) {
            result.setPhoneNumbers(new HashSet<>(phoneNumberMapper.toEntityCollection(domain.getPhoneNumbers())));
        }

        if (isNotNullNotEmpty(domain.getAddresses())) {
            result.setAddresses(new HashSet<>(addressMapper.toEntityCollection(domain.getAddresses())));
        }

        //note we skipped company data because we need to create company object separately
        return cast(result);
    }

    @Override
    public User toDomain(UserEntity entity) {
        User result = new User();
        result.setId(entity.getId());
        result.setContactName(entity.getContactName());
        result.setVerificationState(entity.getUserStatus());
        result.setUserType(entity.getUserType());
        result.setAdminComment(entity.getAdminComment());
        result.setEmail(entity.getEmail());
        result.setSpecialty(entity.getSpecialty());


        if (entity.getWorkerTenant() != null){
            result.setWorkerTenant(tenantMapper.toDomain(entity.getWorkerTenant()));
        }

        if (entity.getTenant() != null){
            result.setTenant(tenantMapper.toDomain(entity.getTenant()));
        }

        if (entity.getTag() != null){
            result.setTag(tagMapper.toDomain(entity.getTag()));
        }

        if (isNotNullNotEmpty(entity.getPhoneNumbers())) {
            result.setPhoneNumbers(new HashSet<>(phoneNumberMapper.toDomainCollection(entity.getPhoneNumbers())));
        }

        if (isNotNullNotEmpty(entity.getAddresses())) {
            result.setAddresses(new HashSet<>(addressMapper.toDomainCollection(entity.getAddresses())));
        }


        if (entity.getCompany() != null){
            result.setCompany(companyPreviewMapper.toDomain(entity.getCompany()));
        }

        return result;
    }

    @Override
    public Collection<User> toDomainCollection(Iterable<UserEntity> entities) {
        Collection<User> result = new ArrayList<>();
        for (UserEntity item : entities) {
            if (item != null) {
                result.add(toDomain(item));
            }
        }

        return result;
    }

    @Override
    public Collection<UserEntity> toEntityCollection(Iterable<User> domains) {
        Collection<UserEntity> result = new ArrayList<>();
        for (User item : domains) {
            if (item != null) {
                result.add(toEntity(item));
            }
        }

        return result;
    }

    @Override
    public UserEntity updateEntity(UserEntity newEntity, UserEntity oldEntity) {

        //NOTE! userType can't be updated as per entity rules

        oldEntity.setContactName(newEntity.getContactName());
        oldEntity.setUserStatus(newEntity.getUserStatus());
        oldEntity.setAdminComment(newEntity.getAdminComment());
        oldEntity.setEmail(newEntity.getEmail());

        Collection<PhoneNumberEntity> updatedPhoneNumberCollection = phoneNumberMapper.updateEntityCollection(
                newEntity.getPhoneNumbers(), oldEntity.getPhoneNumbers()
        );

        if (isNotNullNotEmpty(updatedPhoneNumberCollection)) {
            oldEntity.setPhoneNumbers(new HashSet<>(updatedPhoneNumberCollection));
        }

        Collection<AddressEntity> updatedAddressesCollection = addressMapper.updateEntityCollection(
                newEntity.getAddresses(), oldEntity.getAddresses()
        );

        if (isNotNullNotEmpty(updatedAddressesCollection)) {
            oldEntity.setAddresses(new HashSet<>(updatedAddressesCollection));
        }

        return oldEntity;
    }

    @Override
    public PageDTO<User> toDomainPage(Page<UserEntity> entitiesPage) {
        PageDTO<User> pageDTO = new PageDTO<>();
        pageDTO.setTotalPages(entitiesPage.getTotalPages());
        pageDTO.setPageNumber(entitiesPage.getNumber());
        pageDTO.setTotalElements(entitiesPage.getTotalElements());

        List<User> domainList = new ArrayList<>(toDomainCollection(entitiesPage.getContent()));
        if (isNotNullNotEmpty(domainList)) {
            pageDTO.setContent(domainList);
        }
        return pageDTO;
    }
}
