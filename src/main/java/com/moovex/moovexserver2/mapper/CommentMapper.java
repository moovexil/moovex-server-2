package com.moovex.moovexserver2.mapper;

import com.moovex.moovexserver2.entity.post.CommentEntity;
import com.moovex.moovexserver2.entity.post.PostEntity;
import com.moovex.moovexserver2.mapper.base.BasePageableMapper;
import com.moovex.moovexserver2.mapper.base.BaseUnidirectionalCollectionMapper;
import com.moovex.moovexserver2.mapper.base.BaseUnidirectionalMapper;
import com.moovex.moovexserver2.model.PageDTO;
import com.moovex.moovexserver2.model.post.Comment;
import com.moovex.moovexserver2.model.post.Post;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.moovex.moovexserver2.util.CollectionUtil.isNotNullNotEmpty;

@Service
public class CommentMapper implements
        BaseUnidirectionalMapper<CommentEntity, Comment>,
        BaseUnidirectionalCollectionMapper<CommentEntity, Comment>,
        BasePageableMapper<CommentEntity, Comment> {


    @Override
    public Comment toDomain(CommentEntity entity) {
        Comment domain = new Comment();
        domain.setCommentId(entity.getId());
        domain.setCreatorName(entity.getCreator().getContactName());
        domain.setCreatorId(entity.getCreator().getId());
        domain.setCreatorSpecialty(entity.getCreator().getSpecialty());
        domain.setText(entity.getText());
        domain.setCreationDate(entity.getCreationDate());
        domain.setFeedId(entity.getFeed().getId());
        return domain;
    }

    @Override
    public PageDTO<Comment> toDomainPage(Page<CommentEntity> entitiesPage) {
        PageDTO<Comment> pageDTO = new PageDTO<>();
        pageDTO.setTotalPages(entitiesPage.getTotalPages());
        pageDTO.setPageNumber(entitiesPage.getNumber());
        pageDTO.setTotalElements(entitiesPage.getTotalElements());

        List<Comment> domainList = new ArrayList<>(toDomainCollection(entitiesPage.getContent()));
        if (isNotNullNotEmpty(domainList)) {
            pageDTO.setContent(domainList);
        }
        return pageDTO;
    }

    @Override
    public Collection<Comment> toDomainCollection(Iterable<CommentEntity> entities) {
        Collection<Comment> result = new ArrayList<>();
        for (CommentEntity item : entities) {
            if (item != null) {
                result.add(toDomain(item));
            }
        }

        return result;
    }

}
