package com.moovex.moovexserver2.mapper;

import com.moovex.moovexserver2.entity.phone_number.PhoneNumberEntity;
import com.moovex.moovexserver2.mapper.base.BaseBidirectionalCollectionMapper;
import com.moovex.moovexserver2.mapper.base.BaseBidirectionalMapper;
import com.moovex.moovexserver2.mapper.base.UpdateCollectionMapper;
import com.moovex.moovexserver2.model.phone_number.PhoneNumber;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

import static com.moovex.moovexserver2.util.CollectionUtil.findFirstOrNull;

@Service
public class PhoneNumberMapper implements
        BaseBidirectionalMapper<PhoneNumberEntity, PhoneNumber>,
        BaseBidirectionalCollectionMapper<PhoneNumberEntity, PhoneNumber>,
        UpdateCollectionMapper<PhoneNumberEntity> {


    @Override
    public PhoneNumberEntity toEntity(PhoneNumber domain) {
        return new PhoneNumberEntity(
                domain.getId(),
                domain.getPhoneNumber()
        );
    }

    @Override
    public PhoneNumber toDomain(PhoneNumberEntity entity) {
        return new PhoneNumber(
                entity.getId(),
                entity.getPhoneNumber()
        );
    }


    @Override
    public Collection<PhoneNumber> toDomainCollection(Iterable<PhoneNumberEntity> entities) {
        Collection<PhoneNumber> result = new ArrayList<>();
        for (PhoneNumberEntity item : entities) {
            if (item != null) {
                result.add(toDomain(item));
            }
        }

        return result;
    }

    @Override
    public Collection<PhoneNumberEntity> toEntityCollection(Iterable<PhoneNumber> domains) {
        Collection<PhoneNumberEntity> result = new ArrayList<>();
        for (PhoneNumber item : domains) {
            if (item != null) {
                result.add(toEntity(item));
            }
        }

        return result;
    }

    @Override
    public PhoneNumberEntity updateEntity(PhoneNumberEntity newEntity, PhoneNumberEntity oldEntity) {
        if (newEntity.getPhoneNumber() != null) {
            oldEntity.setPhoneNumber(newEntity.getPhoneNumber());
        }
        return oldEntity;
    }


    @Override
    public Collection<PhoneNumberEntity> updateEntityCollection(Collection<PhoneNumberEntity> newCollection, Collection<PhoneNumberEntity> oldCollection) {
        if (newCollection == null && oldCollection == null) {
            return null;
        }

        if (newCollection == null) {
            return oldCollection;
        }

        if (oldCollection == null) {
            return newCollection;
        }

        Collection<PhoneNumberEntity> result = new ArrayList<>();

        for (PhoneNumberEntity newItem : newCollection) {
            if (newItem != null) {
                //check if item present in old collection
                PhoneNumberEntity oldItem = findFirstOrNull(oldCollection, oldPhone -> oldPhone.getId().equals(newItem.getId()));
                if (oldItem != null) {
                    //The new Item is present in the old collection -> update old item and insert into the result list
                    PhoneNumberEntity resultObject = updateEntity(newItem, oldItem);
                    result.add(resultObject);
                } else {
                    //The new item is not present in the old collection -> insert item to the result list
                    result.add(newItem);
                }
            }
        }

        return result;
    }
}
