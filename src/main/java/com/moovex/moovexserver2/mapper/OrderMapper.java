package com.moovex.moovexserver2.mapper;

import com.moovex.moovexserver2.entity.order.OrderEntity;
import com.moovex.moovexserver2.mapper.base.*;
import com.moovex.moovexserver2.model.PageDTO;
import com.moovex.moovexserver2.model.order.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.moovex.moovexserver2.util.CollectionUtil.isNotNullNotEmpty;

@Service
public class OrderMapper implements
        BaseUnidirectionalMapper<OrderEntity, Order>,
        BaseUnidirectionalCollectionMapper<OrderEntity, Order>,
        BasePageableMapper<OrderEntity, Order>{

    @Autowired private AddressMapper addressMapper;
    @Autowired private CompanyPreviewMapper companyMapper;
    @Autowired private UserPreviewMapper userMapper;
    @Autowired private TrailerMapper trailerMapper;
    @Autowired private TruckMapper truckMapper;
    @Autowired private TractorMapper tractorMapper;
    @Autowired private TagMapper tagMapper;
    @Autowired private TenantMapper tenantMapper;
    @Autowired private ContactMapper contactMapper;

    @Override
    public Order toDomain(OrderEntity entity) {
        Order result = new Order();
        result.setId(entity.getId());
        result.setOrderStatus(entity.getOrderStatus());
        result.setShipmentStatus(entity.getShipmentStatus());
        result.setNotes(entity.getNotes());
        result.setPickupTime(entity.getPickupDate());
        result.setDeliveryTime(entity.getDeliveryDate());
        result.setGoodsPicturesUrls(entity.getGoodsPicturesUrls());
        result.setPublicity(entity.isPublicity());
        result.setGoodsDescription(entity.getGoodsDescription());
        result.setPrice(entity.getPrice());
        result.setRequestedVehicleType(entity.getRequestedVehicleType());
        result.setCurrency(entity.getCurrency());

        if (entity.getTenant() != null){
            result.setTenant(tenantMapper.toDomain(entity.getTenant()));
        }

        if (entity.getPickupAddress() != null) {
            result.setPickupAddress(addressMapper.toDomain(entity.getPickupAddress()));
        }

        if (isNotNullNotEmpty(entity.getContacts())){
            result.setContacts(new HashSet<>(contactMapper.toDomainCollection(entity.getContacts())));
        }

        if (entity.getDestinationAddress() != null) {
            result.setDestinationAddress(addressMapper.toDomain(entity.getDestinationAddress()));
        }

        if (entity.getShipper() != null) {
            result.setShipper(companyMapper.toDomain(entity.getShipper()));
        }

        if (entity.getCarrier() != null) {
            result.setCarrier(companyMapper.toDomain(entity.getCarrier()));
        }

        if (entity.getOrderManager() != null) {
            result.setOrderManager(userMapper.toDomain(entity.getOrderManager()));
        }

        if (entity.getDriver() != null) {
            result.setDriver(userMapper.toDomain(entity.getDriver()));
        }

        if (entity.getTruck() != null) {
            result.setTruck(truckMapper.toDomain(entity.getTruck()));
        }

        if (entity.getTrailer() != null) {
            result.setTrailer(trailerMapper.toDomain(entity.getTrailer()));
        }

        if (entity.getTractor() != null) {
            result.setTractor(tractorMapper.toDomain(entity.getTractor()));
        }

        if (isNotNullNotEmpty(entity.getTags())){
            result.setTags(new HashSet<>(tagMapper.toDomainCollection(entity.getTags())));
        }

        return result;
    }

    @Override
    public PageDTO<Order> toDomainPage(Page<OrderEntity> entitiesPage) {
        PageDTO<Order> pageDTO = new PageDTO<>();
        pageDTO.setTotalPages(entitiesPage.getTotalPages());
        pageDTO.setPageNumber(entitiesPage.getNumber());
        pageDTO.setTotalElements(entitiesPage.getTotalElements());

        List<Order> domainList = new ArrayList<>(toDomainCollection(entitiesPage.getContent()));
        if (isNotNullNotEmpty(domainList)) {
            pageDTO.setContent(domainList);
        }
        return pageDTO;
    }

    @Override
    public Collection<Order> toDomainCollection(Iterable<OrderEntity> entities) {
        Collection<Order> result = new ArrayList<>();
        for (OrderEntity item : entities) {
            if (item != null) {
                result.add(toDomain(item));
            }
        }

        return result;
    }

}
