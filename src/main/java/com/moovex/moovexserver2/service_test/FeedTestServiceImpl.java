package com.moovex.moovexserver2.service_test;

import com.moovex.moovexserver2.entity.post.CommentEntity;
import com.moovex.moovexserver2.entity.post.PostEntity;
import com.moovex.moovexserver2.entity.user.UserEntity;
import com.moovex.moovexserver2.mapper.CommentMapper;
import com.moovex.moovexserver2.mapper.PostMapper;
import com.moovex.moovexserver2.model.PageDTO;
import com.moovex.moovexserver2.model.post.Comment;
import com.moovex.moovexserver2.model.post.Post;
import com.moovex.moovexserver2.repository.CommentRepository;
import com.moovex.moovexserver2.repository.PostRepository;
import com.moovex.moovexserver2.repository.UserRepository;
import com.moovex.moovexserver2.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.moovex.moovexserver2.util.CollectionUtil.findFirstOrNull;
import static com.moovex.moovexserver2.util.CriteriaUtil.getSimplePageable;
import static com.moovex.moovexserver2.util.CriteriaUtil.sortByCreationDesc;


@Service
@Transactional
public class FeedTestServiceImpl implements FeedTestService {

    @Autowired
    PostMapper postMapper;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PostRepository postRepository;

    @Autowired
    CommentRepository commentRepository;

    @Autowired
    CommentMapper commentMapper;

    @Override
    public void createFeedsWithoutComments(int numOfFeeds) {
        for (int i = 0; i < numOfFeeds; i ++){
            createFeed();
        }
    }

    public void createFeed () {
        UserEntity creatorEntity = findUser(UUID.fromString("03865258-fd62-4779-bad0-aa9124e6cab4"));
        PostEntity entity = new PostEntity();
        entity.setCreator(creatorEntity);
        entity.setText("This chapter covers the Spring Framework implementation of the Inversion of Control (IoC) [1]principle. IoC is also known as dependency injection (DI). It is a process whereby objects define their dependencies, that is, the other objects they work with, only through constructor arguments, arguments to a factory method, or properties that are set on the object instance after it is constructed or returned from a factory method. The container then injects those dependencies when it creates the bean. This process is fundamentally the inverse, hence the name Inversion of Control (IoC), of the bean itself controlling the instantiation or location of its dependencies by using direct construction of classes, or a mechanism such as the Service Locator pattern.\n" +
                "\n" +
                "The org.springframework.beans and org.springframework.context packages are the basis for Spring Framework's IoC container. The BeanFactory interface provides an advanced configuration mechanism capable of managing any type of object. ApplicationContext is a sub-interface of BeanFactory. It adds easier integration with Spring's AOP features; message resource handling (for use in internationalization), event publication; and application-layer specific contexts such as the WebApplicationContext for use in web applications.\n" +
                "\n" +
                "In short, the BeanFactory provides the configuration framework and basic functionality, and the ApplicationContext adds more enterprise-specific functionality. The ApplicationContext is a complete superset of the BeanFactory, and is used exclusively in this chapter in descriptions of Spring's IoC container. For more information on using the BeanFactory instead of the ApplicationContext, refer to Section 5.15, “The BeanFactory”.\n" +
                "\n" +
                "In Spring, the objects that form the backbone of your application and that are managed by the Spring IoC container are called beans. A bean is an object that is instantiated, assembled, and otherwise managed by a Spring IoC container. Otherwise, a bean is simply one of many objects in your application. Beans, and the dependencies among them, are reflected in the configuration metadata used by a container.");
        entity.setImgUrl("https://firebasestorage.googleapis.com/v0/b/imoovocommunitydev.appspot.com/o/images%2Fpost_images%2Fbb9aebda-9bcd-41d1-bd3e-0eb427155452.webp?alt=media&token=e70d5fcd-8bc1-493d-aa72-5cfffd8566da");
        entity.setComments(new ArrayList<>());

        postRepository.save(entity);
    }

    @Override
    public void createCommentsForPost(UUID feedId, int numberOfComments) {
        PostEntity post = findPost(feedId);
        List<CommentEntity> comments = post.getComments();

        for (int i = 0; i < numberOfComments; i ++){
            CommentEntity newComment = createCommentEntity(feedId);
            comments.add(newComment);
        }

        postRepository.save(post);
    }

    //------------------------- Helpers ---------------------------------

    private CommentEntity createCommentEntity(UUID feedId) {
        UserEntity creatorUser = findUser(UUID.fromString("03865258-fd62-4779-bad0-aa9124e6cab4"));
        PostEntity feed = findPost(feedId);
        CommentEntity commentEntity = new CommentEntity();
        commentEntity.setText("The MediaWiki software is used by tens of thousands of websites and thousands of companies and organizations. It powers Wikipedia and also this website. MediaWiki helps you collect and organize knowledge and make it available to people");
        commentEntity.setCreator(creatorUser);
        commentEntity.setFeed(feed);
        return commentRepository.save(commentEntity);
    }

    private UserEntity findUser(@NonNull UUID id) {
        Optional<UserEntity> o = userRepository.findUserById(id);
        if (o.isPresent()) {
            return o.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User with this id: " + id + " wan't found in the DB");
        }
    }

    private PostEntity findPost(@NonNull UUID id) {
        Optional<PostEntity> o = postRepository.findPostById(id);
        if (o.isPresent()) {
            return o.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Post with this id: " + id + " wan't found in the DB");
        }
    }

}
