package com.moovex.moovexserver2.service_test;

import java.util.UUID;

public interface FeedTestService {

    void createFeedsWithoutComments(int numOfFeeds);

    void createCommentsForPost(UUID postId, int numberOfComments);
}
