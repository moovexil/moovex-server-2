package com.moovex.moovexserver2.model.tenant;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class Tenant {
    private UUID id;
    private String tenantName;
}
