package com.moovex.moovexserver2.model.post;

import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
public class Post {
    private UUID id;
    private String text;
    private String creatorSpecialty;
    private String imgUrl;
    private Date creationDate;
    private String creatorName;
    private UUID creatorId;
    private boolean liked;
    private int numberOfLikes;
}
