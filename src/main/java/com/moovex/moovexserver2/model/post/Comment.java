package com.moovex.moovexserver2.model.post;

import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
public class Comment {
    private UUID commentId;
    private UUID feedId;
    private UUID creatorId;
    private String text;
    private Date creationDate;
    private String creatorName;
    private String creatorSpecialty;
}
