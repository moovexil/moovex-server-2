package com.moovex.moovexserver2.model.geofence;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum GeofenceRadius {

    SMALL(Constants.small),
    MEDIUM(Constants.medium),
    LARGE(Constants.large);

    @Getter
    private final float radius;

    private static class Constants {
        public static final int small = 150;
        public static final int medium = 500;
        public static final int large = 1000;
    }
}
