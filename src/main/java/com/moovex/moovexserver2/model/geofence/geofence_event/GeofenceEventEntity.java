package com.moovex.moovexserver2.model.geofence.geofence_event;

import com.moovex.moovexserver2.model.geofence.GeofenceEntity;
import lombok.Getter;
import lombok.Setter;
import org.locationtech.jts.geom.Point;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity(name = "w_geofence_events")
public class GeofenceEventEntity {

    @Id
    @GeneratedValue
    @Column(columnDefinition = "uuid")
    private UUID id;

    @Enumerated(EnumType.STRING)
    private GeofenceEventType eventType;

    private String rationale;

    private Long timestamp;

    @ManyToOne
    @JoinColumn(columnDefinition = "uuid")
    private GeofenceEntity hostGeofence;

    @Column(columnDefinition = "geometry")
    private Point location;
}
