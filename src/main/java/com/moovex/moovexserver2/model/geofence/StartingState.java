package com.moovex.moovexserver2.model.geofence;

public enum  StartingState {
    NOT_REGISTERED, REGISTERING, REGISTERED, UNREGISTERED
}
