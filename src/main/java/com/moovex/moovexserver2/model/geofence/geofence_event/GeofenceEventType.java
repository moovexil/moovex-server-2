package com.moovex.moovexserver2.model.geofence.geofence_event;

public enum  GeofenceEventType {
    ENTER, EXIT, DWELL, ERROR
}
