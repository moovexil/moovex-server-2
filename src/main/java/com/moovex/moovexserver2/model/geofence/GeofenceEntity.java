package com.moovex.moovexserver2.model.geofence;

import com.moovex.moovexserver2.entity.tracking.TrackingEntity;
import com.moovex.moovexserver2.model.geofence.geofence_event.GeofenceEventEntity;
import lombok.Getter;
import lombok.Setter;
import org.locationtech.jts.geom.Point;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity(name = "w_geofences")
public class GeofenceEntity {

    @Id
    @GeneratedValue
    @Column(columnDefinition = "uuid")
    private UUID id;

    @Enumerated(EnumType.STRING)
    private StartingState startingState;

    private String landmarkName;

    private String landmarkDescription;

    @Column(columnDefinition = "geometry")
    private Point location;

    private float radius;

    private Long dwellDelay;

    @ManyToOne
    @JoinColumn(columnDefinition = "uuid")
    private TrackingEntity tracking;

    @ManyToOne
    @JoinColumn(columnDefinition = "uuid")
    private GeofenceEntity parent;

    @OneToMany(mappedBy = "parent")
    private List<GeofenceEntity> child;

    @OneToMany(mappedBy = "hostGeofence")
    private List<GeofenceEventEntity> events;

}
