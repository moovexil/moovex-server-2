package com.moovex.moovexserver2.model.filters;

import com.moovex.moovexserver2.model.vechile.VehicleType;
import lombok.Data;

@Data
public class VehicleFilter {
    private VehicleType vehicleType;
    private Double tonnage;
    private String issuer;
    private String registrationNumber;
}
