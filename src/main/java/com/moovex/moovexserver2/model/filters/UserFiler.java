package com.moovex.moovexserver2.model.filters;

import com.moovex.moovexserver2.entity.user.VerificationState;
import com.moovex.moovexserver2.entity.user.UserType;
import com.moovex.moovexserver2.model.tenant.Tenant;
import lombok.Data;

@Data
public class UserFiler {
    private Tenant tenant;
    private String contactName;
    private UserType userType;
    private VerificationState userStatus;
    private AddressFilters addressFilters;
}
