package com.moovex.moovexserver2.model.filters;

import lombok.Data;

@Data
public class AddressFilters {
    private String city;
    private String region;
    private String country;
}
