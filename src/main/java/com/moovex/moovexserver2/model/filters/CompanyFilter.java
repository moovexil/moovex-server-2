package com.moovex.moovexserver2.model.filters;

import com.moovex.moovexserver2.entity.company.CompanyType;
import lombok.Data;

@Data
public class CompanyFilter {
    private String name;
    private CompanyType companyType;
    private AddressFilters addressFilters;
    private VehicleFilter vehicleFilter;
    private boolean cbt;
    private boolean git;

}
