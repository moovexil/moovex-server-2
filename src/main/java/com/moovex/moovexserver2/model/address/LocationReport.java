package com.moovex.moovexserver2.model.address;

import lombok.Data;

import java.util.List;

@Data
public class LocationReport {
    private List<LocationStatistics> locations;
}
