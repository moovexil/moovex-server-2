package com.moovex.moovexserver2.model.address;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LocationStatistics {
    private UUID id;
    private Double latitude;
    private Double longitude;
    private Float accuracy;
    private Double altitude;
    private Float bearing;
    private Float bearingAccuracyDegrees;
    private Long elapsedRealTimeNanos;
    private Double elapsedRealTimeUncertaintyNanos;
    private String provider;
    private Float speed;
    private Float speedAccuracyMetersPerSecond;
    private Long time;
    private Float verticalAccuracyMeters;
}
