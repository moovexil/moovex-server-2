package com.moovex.moovexserver2.model.address;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class Address {

    private UUID id;
    private String country;
    private String region;
    private String city;
}
