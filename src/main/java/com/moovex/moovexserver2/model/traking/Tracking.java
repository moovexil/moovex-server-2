package com.moovex.moovexserver2.model.traking;

import com.moovex.moovexserver2.entity.tracking.TrackingObjectType;
import com.moovex.moovexserver2.entity.tracking.TrackingStatus;
import com.moovex.moovexserver2.model.address.LocationStatistics;
import com.moovex.moovexserver2.model.phone_number.PhoneNumber;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Tracking {
    private UUID id;
    private TrackingObjectType trackingObjectType;
    private TrackingStatus trackingStatus;
    private LocationStatistics lastLocation;
    private UUID trackingObjectId;
    private String trackingObjectName;
}
