package com.moovex.moovexserver2.model.place;

import com.moovex.moovexserver2.entity.place.PlaceType;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.locationtech.jts.geom.Point;

import javax.persistence.*;
import java.util.UUID;

@Data
public class Place {
    private UUID id;
    private PlaceType placeType;
    private String name;
    private String description;
    private String locationGeoJson;
}
