package com.moovex.moovexserver2.model.vechile;

public enum VehicleType {
    FLATBED,
    ENCLOSED,
    REFRIGERATED,
    LOWBOY,
    TANKER,
    TRAILER
}
