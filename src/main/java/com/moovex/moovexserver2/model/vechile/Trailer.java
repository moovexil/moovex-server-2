package com.moovex.moovexserver2.model.vechile;

import com.moovex.moovexserver2.entity.vehicle.TrailerType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Trailer {
    private UUID id;
    private TrailerType type;
    private String registrationNumber;
    private String issuer;
    private Double tonnage;
}
