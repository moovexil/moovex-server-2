package com.moovex.moovexserver2.model.vechile;

import com.moovex.moovexserver2.entity.vehicle.TruckType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Truck {
    private UUID id;
    private TruckType type;
    private String registrationNumber;
    private String issuer;
    private Double tonnage;
}
