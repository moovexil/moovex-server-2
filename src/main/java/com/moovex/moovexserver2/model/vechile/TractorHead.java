package com.moovex.moovexserver2.model.vechile;

import com.moovex.moovexserver2.entity.vehicle.TractorHeadType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TractorHead {
    private UUID id;
    private TractorHeadType type;
    private String registrationNumber;
    private String issuer;
}
