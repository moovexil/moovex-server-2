package com.moovex.moovexserver2.model.company;

import com.moovex.moovexserver2.entity.company.CompanyType;
import com.moovex.moovexserver2.entity.company.ContractStatus;
import com.moovex.moovexserver2.entity.user.VerificationState;
import com.moovex.moovexserver2.model.address.Address;
import com.moovex.moovexserver2.model.phone_number.PhoneNumber;
import lombok.Data;

import java.util.Set;
import java.util.UUID;

@Data
public class CreateUserCompanyRequest {
    private UUID companyAdminId;
    private String name;
    private String about;
    private String specialty;
    private CompanyType companyType;
    private ContractStatus contractStatus;
    private VerificationState verificationState;
    private Set<Address> addresses;
    private Set<PhoneNumber> phoneNumbers;
    private boolean cbt;
    private boolean git;
}
