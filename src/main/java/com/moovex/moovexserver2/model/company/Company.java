package com.moovex.moovexserver2.model.company;

import com.moovex.moovexserver2.entity.company.AgentPaymentStatus;
import com.moovex.moovexserver2.entity.company.CompanyType;
import com.moovex.moovexserver2.entity.company.ContractStatus;
import com.moovex.moovexserver2.entity.user.VerificationState;
import com.moovex.moovexserver2.model.address.Address;
import com.moovex.moovexserver2.model.phone_number.PhoneNumber;
import com.moovex.moovexserver2.model.tag.Tag;
import com.moovex.moovexserver2.model.tenant.Tenant;
import com.moovex.moovexserver2.model.user.User;
import com.moovex.moovexserver2.model.vechile.TractorHead;
import com.moovex.moovexserver2.model.vechile.Trailer;
import com.moovex.moovexserver2.model.vechile.Truck;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Set;
import java.util.UUID;

@Data
public class Company {
    private UUID id;
    private Tenant tenant;
    private String name;
    private Tag tag;
    private String about;
    private String specialty;
    private CompanyType companyType;
    private ContractStatus contractStatus;
    private VerificationState verificationState;
    private AgentPaymentStatus agentPaymentStatus;
    private Set<Address> addresses;
    private Set<PhoneNumber> phoneNumbers;
    private boolean cbtAvailable;
    private boolean gitAvailable;
}
