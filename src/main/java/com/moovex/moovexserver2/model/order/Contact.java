package com.moovex.moovexserver2.model.order;

import com.moovex.moovexserver2.entity.company.CompanyType;
import com.moovex.moovexserver2.entity.user.UserType;
import com.moovex.moovexserver2.model.address.Address;
import com.moovex.moovexserver2.model.phone_number.PhoneNumber;
import lombok.Data;

import java.util.Set;
import java.util.UUID;

@Data
public class Contact {
    private UUID id;
    private boolean handled;
    private String adminComment;
    private String companyName;
    private CompanyType companyType;
    private UserType userType;
    private String contactName;
    private String email;
    private Set<Address> addresses;
    private Set<PhoneNumber> phoneNumbers;
}
