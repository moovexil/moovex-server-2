package com.moovex.moovexserver2.model.order;

import com.moovex.moovexserver2.entity.CurrencyType;
import com.moovex.moovexserver2.entity.order.OrderShipmentStatus;
import com.moovex.moovexserver2.entity.order.OrderStatus;
import com.moovex.moovexserver2.model.vechile.TractorHead;
import com.moovex.moovexserver2.model.vechile.Trailer;
import com.moovex.moovexserver2.model.vechile.Truck;
import com.moovex.moovexserver2.model.vechile.VehicleType;
import com.moovex.moovexserver2.model.address.Address;
import com.moovex.moovexserver2.model.company.Company;
import com.moovex.moovexserver2.model.tag.Tag;
import com.moovex.moovexserver2.model.tenant.Tenant;
import com.moovex.moovexserver2.model.user.UserPreview;
import lombok.Data;

import java.util.Date;
import java.util.Set;
import java.util.UUID;

@Data
public class Order {

    private UUID id;
    private Tenant tenant;
    private OrderStatus orderStatus;
    private OrderShipmentStatus shipmentStatus;
    private Set<Tag> tags;
    private String notes;
    private Address pickupAddress;
    private Address destinationAddress;
    private Date pickupTime;
    private Date deliveryTime;
    private Company shipper;
    private Company carrier;
    private UserPreview orderManager;
    private UserPreview driver;
    private Truck truck;
    private Trailer trailer;
    private TractorHead tractor;
    private boolean publicity;
    private String goodsDescription;
    private Set<String> goodsPicturesUrls;
    private Integer price;
    private CurrencyType currency;
    private VehicleType requestedVehicleType;
    private Set<Contact> contacts;
}
