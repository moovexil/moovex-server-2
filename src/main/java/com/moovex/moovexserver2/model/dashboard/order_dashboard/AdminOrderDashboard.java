package com.moovex.moovexserver2.model.dashboard.order_dashboard;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdminOrderDashboard {
    long pending;
    long approved;
    long inTransit;
    long delayed;
    long delivered;
    long completed;
    long rejected;
    long dispute;
}
