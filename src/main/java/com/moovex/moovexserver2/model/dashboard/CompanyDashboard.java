package com.moovex.moovexserver2.model.dashboard;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompanyDashboard {

    private long approved;
    private long pending;
    private long notAnswered;
    private long rejected;
    private long noWhatsApp;
    private long notSentContract;
    private long negotiationContract;
    private long signedContract;
}
