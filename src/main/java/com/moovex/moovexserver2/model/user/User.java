package com.moovex.moovexserver2.model.user;

import com.moovex.moovexserver2.entity.user.VerificationState;
import com.moovex.moovexserver2.entity.user.UserType;
import com.moovex.moovexserver2.model.address.Address;
import com.moovex.moovexserver2.model.company.Company;
import com.moovex.moovexserver2.model.phone_number.PhoneNumber;
import com.moovex.moovexserver2.model.tag.Tag;
import com.moovex.moovexserver2.model.tenant.Tenant;
import lombok.Data;

import java.util.Set;
import java.util.UUID;

@Data
public class User {

    private UUID id;
    private Tenant tenant;
    private Tenant workerTenant;
    private Tag tag;
    private String contactName;
    private String specialty;
    private UserType userType;
    private VerificationState verificationState;
    private Set<Address> addresses;
    private Set<PhoneNumber> phoneNumbers;
    private Company company;
    private String adminComment;
    private String email;
}
