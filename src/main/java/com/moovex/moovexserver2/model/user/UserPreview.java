package com.moovex.moovexserver2.model.user;

import com.moovex.moovexserver2.model.address.Address;
import com.moovex.moovexserver2.model.phone_number.PhoneNumber;
import com.moovex.moovexserver2.model.tag.Tag;
import com.moovex.moovexserver2.model.tenant.Tenant;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Set;
import java.util.UUID;

@Data
public class UserPreview {
    private UUID id;
    private String contactName;
    private Tag tag;
    private Tenant tenant;
    private Set<Address> addresses;
    private Set<PhoneNumber> phoneNumbers;
}
