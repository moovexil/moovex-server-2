package com.moovex.moovexserver2.model.tag;

import com.moovex.moovexserver2.entity.tag.TagType;
import lombok.Data;

import java.util.UUID;

@Data
public class Tag {

    private UUID tagId;
    private String tagName;
    private UUID tagOwnerId;
    private TagType tagType;
}
