package com.moovex.moovexserver2.model.phone_number;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Data
@AllArgsConstructor
public class PhoneNumber {
    private UUID id;
    private String phoneNumber;
}
