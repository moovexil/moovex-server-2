package com.moovex.moovexserver2.entity.phone_number;

import com.moovex.moovexserver2.entity.Auditable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "w_phone_numbers")
@AllArgsConstructor
@NoArgsConstructor
public class PhoneNumberEntity extends Auditable<String> {

    /**
     * Deep copy constructor.
     */
    public PhoneNumberEntity(PhoneNumberEntity that){
        this(null,that.phoneNumber);
    }

    @Id
    @GeneratedValue
    private UUID id;

    @Column(nullable = false)
    private String phoneNumber;

}
