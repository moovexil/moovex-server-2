package com.moovex.moovexserver2.entity.tracking;

import com.moovex.moovexserver2.entity.address.LocationStatisticsEntity;
import com.moovex.moovexserver2.entity.events.EventEntity;
import com.moovex.moovexserver2.entity.tenant.TenantEntity;
import com.moovex.moovexserver2.model.geofence.GeofenceEntity;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity(name = "w_tracking")
public class TrackingEntity {

    @Id
    @GeneratedValue
    @Column(columnDefinition = "uuid")
    private UUID id;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "TEXT", nullable = false, updatable = false)
    private TrackingObjectType trackingObjectType;

    @OneToOne
    @JoinColumn(name = "tenant_id")
    private TenantEntity tenant;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "TEXT")
    private TrackingStatus trackingStatus;

    @OneToMany(
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            }
    )
    @JoinTable(
            name = "w_tracking_locations",
            joinColumns = {@JoinColumn(name = "tracking_id")},
            inverseJoinColumns = {@JoinColumn(name = "location_point_id")}
    )
    private List<LocationStatisticsEntity> locations;

    private UUID trackingObjectId;

    private String trackingObjectName;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "trackingKey")
    private List<EventEntity> events;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "tracking")
    private List<GeofenceEntity> geofences;

}
