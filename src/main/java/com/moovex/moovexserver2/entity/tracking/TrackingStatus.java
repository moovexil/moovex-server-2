package com.moovex.moovexserver2.entity.tracking;

public enum TrackingStatus {
    NOT_TRACKING, TRACKING, STARTING, STOPPING
}
