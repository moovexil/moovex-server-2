package com.moovex.moovexserver2.entity.tracking;


public enum  TrackingObjectType {
    EMPLOYEE, VEHICLE
}
