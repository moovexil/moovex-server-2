package com.moovex.moovexserver2.entity.tenant;

import com.moovex.moovexserver2.entity.Auditable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "w_tenants")
public class TenantEntity extends Auditable<String> {

    @Id
    @GeneratedValue
    private UUID id;

    @Column(updatable = false,nullable = false,unique = true)
    private String tenantName;

}
