package com.moovex.moovexserver2.entity.vehicle;

public enum TruckType {
    FLATBED,
    ENCLOSED,
    REFRIGERATED,
    LOWBOY,
    TANKER
}
