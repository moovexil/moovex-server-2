package com.moovex.moovexserver2.entity.vehicle;

import com.moovex.moovexserver2.entity.Auditable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "w_truck")
public class TruckEntity extends Auditable<String> {

    @Id
    @GeneratedValue
    private UUID id;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "TEXT")
    private TruckType type;

    private String registrationNumber;

    private String issuer;

    private Double tonnage;
}
