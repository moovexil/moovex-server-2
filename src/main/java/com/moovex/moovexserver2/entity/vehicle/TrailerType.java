package com.moovex.moovexserver2.entity.vehicle;

public enum TrailerType {
    FLATBED,
    ENCLOSED,
    REFRIGERATED,
    LOWBOY,
    TANKER
}
