package com.moovex.moovexserver2.entity.place;

import lombok.Getter;
import lombok.Setter;
import org.locationtech.jts.geom.Point;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity(name = "w_places")
public class PlaceEntity {

    @Id
    @GeneratedValue
    @Column(columnDefinition = "uuid")
    private UUID id;

    @Enumerated(EnumType.STRING)
    private PlaceType placeType;

    private String name;

    private String description;

    @Column(columnDefinition = "geometry")
    private Point location;
}
