package com.moovex.moovexserver2.entity.place;

public enum PlaceType {
    PARKING, OFFICE, GARRAGE, CHECK_POINT
}
