package com.moovex.moovexserver2.entity.address;

import com.moovex.moovexserver2.entity.Auditable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Getter
@Setter
@Entity
@Table(name = "w_addresses")
@AllArgsConstructor
@NoArgsConstructor
public class AddressEntity extends Auditable<String> {

    @Id
    @GeneratedValue
    private UUID id;
    private String country;
    private String region;
    private String city;

    /**
     * Deep copy constructor.
     */
    public AddressEntity (AddressEntity that){
        this(null,that.country,that.region,that.city);
    }

}
