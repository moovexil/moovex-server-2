package com.moovex.moovexserver2.entity.address;

import com.moovex.moovexserver2.entity.Auditable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "w_locations")
public class LocationStatisticsEntity extends Auditable<String> {

    @Id
    @GeneratedValue
    private UUID id;
    private Double latitude;
    private Double longitude;
    private Float accuracy;
    private Double altitude;
    private Float bearing;
    private Float bearingAccuracyDegrees;
    private Long elapsedRealTimeNanos;
    private Double elapsedRealTimeUncertaintyNanos;
    private String provider;
    private Float speed;
    private Float speedAccuracyMetersPerSecond;
    private Long time;
    private Float verticalAccuracyMeters;
}
