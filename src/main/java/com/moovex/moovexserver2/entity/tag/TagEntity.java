package com.moovex.moovexserver2.entity.tag;

import com.moovex.moovexserver2.entity.Auditable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "w_system_tags")
public class TagEntity extends Auditable<String> {

    @Id
    @GeneratedValue
    private UUID id;

    @Column(nullable = false,unique = true)
    private String tagName;

    private UUID tagOwnerId;

    @Enumerated(EnumType.STRING)
    private TagType tagType;

}
