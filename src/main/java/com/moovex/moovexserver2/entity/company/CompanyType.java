package com.moovex.moovexserver2.entity.company;

public enum CompanyType {
    CARRIER,
    SHIPPER
}
