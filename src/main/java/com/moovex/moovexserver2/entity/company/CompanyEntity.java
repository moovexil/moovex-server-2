package com.moovex.moovexserver2.entity.company;

import com.moovex.moovexserver2.entity.Auditable;
import com.moovex.moovexserver2.entity.address.AddressEntity;
import com.moovex.moovexserver2.entity.phone_number.PhoneNumberEntity;
import com.moovex.moovexserver2.entity.tag.TagEntity;
import com.moovex.moovexserver2.entity.tenant.TenantEntity;
import com.moovex.moovexserver2.entity.user.UserEntity;
import com.moovex.moovexserver2.entity.user.VerificationState;
import com.moovex.moovexserver2.entity.vehicle.TractorHeadEntity;
import com.moovex.moovexserver2.entity.vehicle.TrailerEntity;
import com.moovex.moovexserver2.entity.vehicle.TruckEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "w_companies")
public class CompanyEntity extends Auditable<String> {

    @Id
    @GeneratedValue
    private UUID id;

    @Column(nullable = false)
    private String name;

    @OneToOne
    @JoinColumn(name = "tenant_id")
    private TenantEntity tenant;

    @JoinColumn(name = "tenant_id")
    private String about;

    @JoinColumn(name = "tenant_id")
    private String specialty;

    @OneToOne
    @JoinColumn(name = "tag_id")
    private TagEntity tag;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "TEXT", nullable = false, updatable = false)
    private CompanyType companyType;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "TEXT", nullable = false, updatable = false)
    private VerificationState verificationState;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "TEXT", nullable = false)
    private ContractStatus contractStatus;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "TEXT", nullable = false)
    private AgentPaymentStatus agentPaymentStatus;

    private String agentPaymentTransactionId;

    @OneToMany(
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            }
    )
    @JoinTable(
            name = "w_company_addresses",
            joinColumns = @JoinColumn(name = "company_id"),
            inverseJoinColumns = @JoinColumn(name = "address_id")
    )
    private Set<AddressEntity> addresses;

    @OneToMany(
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            }
    )
    @JoinTable(
            name = "w_company_phone_numbers",
            joinColumns = @JoinColumn(name = "company_id"),
            inverseJoinColumns = @JoinColumn(name = "phone_number_id")
    )
    private Set<PhoneNumberEntity> phoneNumbers;

    @OneToMany(
            fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST
            }
    )
    @JoinTable(
            name = "w_company_employees",
            joinColumns = @JoinColumn(name = "company_id"),
            inverseJoinColumns = @JoinColumn(name = "employee_id")
    )
    private Set<UserEntity> employees;

    @OneToMany(
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            }
    )
    @JoinTable(
            name = "w_company_trucks",
            joinColumns = {@JoinColumn(name = "company_id")},
            inverseJoinColumns = {@JoinColumn(name = "truck_id")}
    )
    private Set<TruckEntity> trucks;

    @OneToMany(
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            }
    )
    @JoinTable(
            name = "w_company_trailers",
            joinColumns = {@JoinColumn(name = "company_id")},
            inverseJoinColumns = {@JoinColumn(name = "trailer_id")}
    )
    private Set<TrailerEntity> trailers;

    @OneToMany(
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            }
    )
    @JoinTable(
            name = "w_company_tractor_heads",
            joinColumns = {@JoinColumn(name = "company_id")},
            inverseJoinColumns = {@JoinColumn(name = "tractor_id")}
    )
    private Set<TractorHeadEntity> tractorHeads;

    private boolean cbtAvailable;

    private boolean gitAvailable;
}
