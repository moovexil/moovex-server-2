package com.moovex.moovexserver2.entity.company;

public enum ContractStatus {
    NOT_SENT,
    NO_RESPONSE,
    NEGOTIATION,
    SIGNED,
    NOT_APPROVED
}
