package com.moovex.moovexserver2.entity.post;

import com.moovex.moovexserver2.entity.Auditable;
import com.moovex.moovexserver2.entity.user.UserEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "w_comments")
public class CommentEntity extends Auditable<String> {

    @Id
    @GeneratedValue
    private UUID id;

    @Column(columnDefinition = "TEXT")
    private String text;

    @OneToOne
    @JoinColumn(
            name = "creator_id"
    )
    private UserEntity creator;

    @ManyToOne(
            fetch = FetchType.LAZY,
            cascade = CascadeType.PERSIST
    )
    @JoinColumn(
            name = "feed_id"
    )
    private PostEntity feed;
}
