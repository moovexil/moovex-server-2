package com.moovex.moovexserver2.entity.post;

import com.moovex.moovexserver2.entity.Auditable;
import com.moovex.moovexserver2.entity.tag.TagEntity;
import com.moovex.moovexserver2.entity.user.UserEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "w_posts")
public class PostEntity extends Auditable<String> {

    @Id
    @GeneratedValue
    private UUID id;

    @Column(columnDefinition = "TEXT")
    private String text;

    private String imgUrl;

    @OneToOne
    @JoinColumn(name = "post_creator_id")
    private UserEntity creator;

    @OneToMany(
            cascade = {CascadeType.PERSIST},
            mappedBy = "feed"
    )
    private List<CommentEntity> comments;

    @ManyToMany(fetch = FetchType.LAZY)
    private Set<TagEntity> likeTags;

}
