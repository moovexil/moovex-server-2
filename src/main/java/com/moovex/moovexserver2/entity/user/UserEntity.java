package com.moovex.moovexserver2.entity.user;

import com.moovex.moovexserver2.entity.Auditable;
import com.moovex.moovexserver2.entity.address.AddressEntity;
import com.moovex.moovexserver2.entity.company.CompanyEntity;
import com.moovex.moovexserver2.entity.phone_number.PhoneNumberEntity;
import com.moovex.moovexserver2.entity.tag.TagEntity;
import com.moovex.moovexserver2.entity.tenant.TenantEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "w_users")
public class UserEntity extends Auditable<String> {

    /**
     * Deep copy constructor.
     */
    public UserEntity(UserEntity that) {
        this(null,that.fcmToken, that.adminComment,that.specialty, that.userType, that.userStatus, that.contactName, that.email,null, null,null, null,null, null);
    }

    @Id
    @GeneratedValue
    private UUID id;

    private String fcmToken;


    private String adminComment;

    private String specialty;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "TEXT", nullable = false, updatable = false)
    private UserType userType;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "TEXT", nullable = false)
    private VerificationState userStatus;

    @Column(columnDefinition = "TEXT", nullable = false)
    private String contactName;

    private String email;

    @OneToOne
    @JoinColumn(name = "tenant_id")
    private TenantEntity tenant;

    @OneToOne
    @JoinColumn(name = "worker_tenant_id")
    private TenantEntity workerTenant;

    @OneToOne
    @JoinColumn(name = "tag_id")
    private TagEntity tag;

    @OneToMany(
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            }
    )
    @JoinTable(
            name = "w_user_addresses",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "address_id")
    )
    private Set<AddressEntity> addresses;

    @OneToMany(
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            }
    )
    @JoinTable(
            name = "w_user_phone_numbers",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "phone_number_id")
    )
    private Set<PhoneNumberEntity> phoneNumbers;

    @ManyToOne(
            fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST
            }
    )
    @JoinTable(
            name = "w_company_employees",
            joinColumns = @JoinColumn(name = "employee_id"),
            inverseJoinColumns = @JoinColumn(name = "company_id")
    )
    private CompanyEntity company;

}
