package com.moovex.moovexserver2.entity.user;

public enum  UserType {
    USER,
    AGENT,
    ADMIN,
    EX_ADMIN,
    DRIVER
}
