package com.moovex.moovexserver2.entity.user;

public enum VerificationState {
    PENDING,
    APPROVED,
    INCORRECT,
    NO_ANSWER,
    DISABLED,
    REJECTED,
    NO_WHATS_APP,
    DUPLICATE
}
