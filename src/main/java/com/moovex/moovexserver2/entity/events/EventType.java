package com.moovex.moovexserver2.entity.events;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum EventType {


    //Tracking service group
    TRACKING_STARTED(Constants.priority5),
    TRACKING_STARTED_NO_PERMISSIONS(Constants.priority1),
    TRACKING_STOPPED(Constants.priority6),

    //Location hardware group
    LOCATION_AVAILABLE(Constants.priority7),
    LOCATION_NOT_AVAILABLE(Constants.priority2),

    //Internet connection hardware group
    INTERNET_CONNECTION_AVAILABLE(Constants.priority8),
    INTERNET_CONNECTION_AVAILABLE_NO_INTERNET(Constants.priority3),
    INTERNET_CONNECTION_NOT_AVAILABLE(Constants.priority4);

    @Getter
    private final Integer priority;

    //Smallest value = highest priority
    private static class Constants {
        public static final int priority1 = 1;
        public static final int priority2 = 2;
        public static final int priority3 = 3;
        public static final int priority4 = 4;
        public static final int priority5 = 5;
        public static final int priority6 = 6;
        public static final int priority7 = 7;
        public static final int priority8 = 8;
    }
}
