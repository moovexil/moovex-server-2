package com.moovex.moovexserver2.entity.events;

import com.moovex.moovexserver2.entity.tracking.TrackingEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity(name = "w_events")
public class EventEntity {

    @Id
    @GeneratedValue
    private UUID id;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "TEXT", updatable = false)
    private EventType type;

    private Long eventTimestamp;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "trackingKey_id")
    private TrackingEntity trackingKey;
}
