package com.moovex.moovexserver2.entity.order;

public enum OrderStatus {
    PENDING,
    APPROVED,
    DELAYED,
    REJECTED,
    COMPLETED,
    DISPUTE
}
