package com.moovex.moovexserver2.entity.order;

public enum OrderShipmentStatus {
    PENDING,
    IN_TRANSIT,
    DELIVERED
}
