package com.moovex.moovexserver2.entity.order;

import com.moovex.moovexserver2.entity.Auditable;
import com.moovex.moovexserver2.entity.address.AddressEntity;
import com.moovex.moovexserver2.entity.company.CompanyType;
import com.moovex.moovexserver2.entity.phone_number.PhoneNumberEntity;
import com.moovex.moovexserver2.entity.user.UserType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "w_contacts")
public class ContactEntity extends Auditable<String> {

    @Id
    @GeneratedValue
    private UUID id;

    private boolean handled;

    private String adminComment;

    private String companyName;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "TEXT")
    private CompanyType companyType;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "TEXT", nullable = false)
    private UserType userType;

    @Column(columnDefinition = "TEXT", nullable = false)
    private String contactName;

    private String email;

    @OneToMany(
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            }
    )
    @JoinTable(
            name = "w_contact_addresses",
            joinColumns = @JoinColumn(name = "contact_id"),
            inverseJoinColumns = @JoinColumn(name = "address_id")
    )
    private Set<AddressEntity> addresses;

    @OneToMany(
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            }
    )
    @JoinTable(
            name = "w_contact_phone_numbers",
            joinColumns = @JoinColumn(name = "contact_id"),
            inverseJoinColumns = @JoinColumn(name = "phone_number_id")
    )
    private Set<PhoneNumberEntity> phoneNumbers;

}
