package com.moovex.moovexserver2.entity.order;

import com.moovex.moovexserver2.entity.Auditable;
import com.moovex.moovexserver2.entity.CurrencyType;
import com.moovex.moovexserver2.entity.address.AddressEntity;
import com.moovex.moovexserver2.entity.address.LocationStatisticsEntity;
import com.moovex.moovexserver2.entity.company.CompanyEntity;
import com.moovex.moovexserver2.entity.tag.TagEntity;
import com.moovex.moovexserver2.entity.tenant.TenantEntity;
import com.moovex.moovexserver2.entity.user.UserEntity;
import com.moovex.moovexserver2.entity.vehicle.TractorHeadEntity;
import com.moovex.moovexserver2.entity.vehicle.TrailerEntity;
import com.moovex.moovexserver2.entity.vehicle.TruckEntity;
import com.moovex.moovexserver2.model.vechile.VehicleType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "w_orders")
public class OrderEntity extends Auditable<String> {

    @Id
    @GeneratedValue
    private UUID id;

    private boolean publicity;

    private String goodsDescription;

    private Integer price;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "TEXT")
    private VehicleType requestedVehicleType;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "TEXT", nullable = false)
    private OrderStatus orderStatus;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "TEXT", nullable = false)
    private OrderShipmentStatus shipmentStatus;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "TEXT")
    private CurrencyType currency;

    @Temporal(TemporalType.DATE)
    protected Date pickupDate;

    @Temporal(TemporalType.DATE)
    protected Date deliveryDate;


    @OneToOne(
            cascade = CascadeType.PERSIST
    )
    @JoinColumn(name = "tenant_id")
    private TenantEntity tenant;

    private String notes;

    @ManyToMany(
            cascade = CascadeType.PERSIST
    )
    @JoinTable(
            name = "w_order_sharing_tags",
            joinColumns = {@JoinColumn(name = "order_id")},
            inverseJoinColumns = {@JoinColumn(name = "tag_owner_company_id")}
    )
    private Set<TagEntity> tags;

    @OneToMany(
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            }
    )
    @JoinTable(
            name = "w_order_cargo_location",
            joinColumns = {@JoinColumn(name = "order_id")},
            inverseJoinColumns = {@JoinColumn(name = "location_point_id")}
    )
    private Set<LocationStatisticsEntity> cargoLocation;

    @OneToMany(
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            }
    )
    @JoinTable(
            name = "w_order_contacts",
            joinColumns = {@JoinColumn(name = "order_id")},
            inverseJoinColumns = {@JoinColumn(name = "contact_id")}
    )
    private Set<ContactEntity> contacts;

    @ElementCollection
    @Column(columnDefinition = "TEXT", nullable = false)
    @CollectionTable(name = "w_order_goods_pictures_urls")
    private Set<String> goodsPicturesUrls;


    @OneToOne(
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            }
    )
    @JoinTable(
            name = "w_order_addresses_pickup",
            joinColumns = {@JoinColumn(name = "order_id")},
            inverseJoinColumns = {@JoinColumn(name = "pickup_address_id")}
    )
    private AddressEntity pickupAddress;

    @OneToOne(
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(
            name = "w_order_addresses_destination",
            joinColumns = {@JoinColumn(name = "order_id")},
            inverseJoinColumns = {@JoinColumn(name = "contact_id")}
    )
    private AddressEntity destinationAddress;

    @OneToOne(
            cascade = CascadeType.PERSIST
    )
    @JoinColumn(name = "shipper_id")
    private CompanyEntity shipper;

    @OneToOne(
            cascade = CascadeType.PERSIST
    )
    @JoinColumn(name = "carrier_id")
    private CompanyEntity carrier;

    @OneToOne(
            cascade = CascadeType.PERSIST
    )
    @JoinColumn(name = "order_manager_id")
    private UserEntity orderManager;

    @OneToOne(
            cascade = CascadeType.PERSIST
    )
    @JoinColumn(name = "driver_id")
    private UserEntity driver;

    @OneToOne(
            cascade = CascadeType.PERSIST
    )
    @JoinColumn(name = "truck_id")
    private TruckEntity truck;

    @OneToOne(
            cascade = CascadeType.PERSIST
    )
    @JoinColumn(name = "trailer_id")
    private TrailerEntity trailer;

    @OneToOne(
            cascade = CascadeType.PERSIST
    )
    @JoinColumn(name = "tractor_id")
    private TractorHeadEntity tractor;

}
